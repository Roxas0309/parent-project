package id.co.oob.lib.common.merchant.onboarding.encryptor;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.web.util.UriUtils;

import com.google.gson.Gson;

import id.co.oob.lib.common.merchant.onboarding.dto.user.CifDecryptor;
import id.co.oob.lib.common.merchant.onboarding.dto.user.LinkChangePassDto;
public class EncryptDataFiller {

	
	public static String encodeUrl(String word) {
		return UriUtils.encode(word, StandardCharsets.UTF_8);  
	} 
	
	  private static SecretKeySpec secretKey;
	    private static byte[] key;
	 
	    public static void setKey(String myKey) 
	    {
	        MessageDigest sha = null;
	        try {
	            key = myKey.getBytes("UTF-8");
	            sha = MessageDigest.getInstance("SHA-1");
	            key = sha.digest(key);
	            key = Arrays.copyOf(key, 16); 
	            secretKey = new SecretKeySpec(key, "AES");
	        } 
	        catch (NoSuchAlgorithmException e) {
	            e.printStackTrace();
	        } 
	        catch (UnsupportedEncodingException e) {
	            e.printStackTrace();
	        }
	    }
	 
	    public static String encrypt(String strToEncrypt, String secret) 
	    {
	        try
	        {
	            setKey(secret);
	            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
	            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
	        } 
	        catch (Exception e) 
	        {
	            System.out.println("Error while encrypting: " + e.toString());
	        }
	        return null;
	    }
	 
	    public static String decrypt(String strToDecrypt, String secret) 
	    {
	        try
	        {
	            setKey(secret);
	            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
	            cipher.init(Cipher.DECRYPT_MODE, secretKey);
	            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
	        } 
	        catch (Exception e) 
	        {
	            System.out.println("Error while decrypting: " + e.toString());
	        }
	        return null;
	    }
	    
	    public static void main(String[] args) {
//	    	LinkChangePassDto linkChangePassDto = new LinkChangePassDto();
//			linkChangePassDto.setId(0L);
//			linkChangePassDto.setDate(new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date()));
//			String secretKey = "1lzr0t5qygmakgu1p0b9";
//			String encResult = EncryptDataFiller.encrypt(new Gson().toJson(linkChangePassDto), 
//					            secretKey);
//			System.out.println(encResult);
			
			System.out.println(decrypt("/RRBRbcMeRLDfEmYct15WCUqHXuqkVCBxZI+/RdhGC6vS7uNpMI0VJw/aM7Eu99y", "EAGXGHTF9TB10IEJASCJ"));
	    }
}

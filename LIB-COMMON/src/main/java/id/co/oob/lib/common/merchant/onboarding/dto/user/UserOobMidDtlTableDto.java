package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.Date;

public class UserOobMidDtlTableDto {

    private Long idOob;
	private String midNumber;
	private String nmidNumber;
	private String akunOobMid; //Only For standard And Upgrade
	private String akunOobMidDesc;
	private String applicationNumber;
	private String pathQrNumber;
	private Date createDate;
	public Long getIdOob() {
		return idOob;
	}
	public void setIdOob(Long idOob) {
		this.idOob = idOob;
	}
	public String getMidNumber() {
		return midNumber;
	}
	public void setMidNumber(String midNumber) {
		this.midNumber = midNumber;
	}
	public String getNmidNumber() {
		return nmidNumber;
	}
	public void setNmidNumber(String nmidNumber) {
		this.nmidNumber = nmidNumber;
	}
	public String getAkunOobMid() {
		return akunOobMid;
	}
	public void setAkunOobMid(String akunOobMid) {
		this.akunOobMid = akunOobMid;
	}
	public String getAkunOobMidDesc() {
		return akunOobMidDesc;
	}
	public void setAkunOobMidDesc(String akunOobMidDesc) {
		this.akunOobMidDesc = akunOobMidDesc;
	}
	public String getApplicationNumber() {
		return applicationNumber;
	}
	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}
	public String getPathQrNumber() {
		return pathQrNumber;
	}
	public void setPathQrNumber(String pathQrNumber) {
		this.pathQrNumber = pathQrNumber;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	

}

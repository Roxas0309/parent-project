package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.Date;


public class UserAuthTokenTableDto {
	private String idToken;
	private Long idOob;
	private Date loginDate;
	private Date refreshDate;
	public String getIdToken() {
		return idToken;
	}
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	public Long getIdOob() {
		return idOob;
	}
	public void setIdOob(Long idOob) {
		this.idOob = idOob;
	}
	public Date getLoginDate() {
		return loginDate;
	}
	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}
	public Date getRefreshDate() {
		return refreshDate;
	}
	public void setRefreshDate(Date refreshDate) {
		this.refreshDate = refreshDate;
	}
	
	
}

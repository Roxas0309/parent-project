package id.co.oob.lib.common.merchant.onboarding.convert;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;

public class MakingCalendar {

	public static int day(int month, int day, int year) {
        int y = year - (14 - month) / 12;
        int x = y + y/4 - y/100 + y/400;
        int m = month + 12 * ((14 - month) / 12) - 2;
        int d = (day + x + (31*m)/12) % 7;
        return d;
    }

    // return true if the given year is a leap year
    public static boolean isLeapYear(int year) {
        if  ((year % 4 == 0) && (year % 100 != 0)) return true;
        if  (year % 400 == 0) return true;
        return false;
    }

    private static String[] DAY_DATE = {"Min","Sen", "Sel", "Rab", "Kam", "Jum", "Sab"};
    
    public static CalendarDto makingCalendar(int month, int year) {
    	   // months[i] = name of month i
        String[] months = {
            "",                               // leave empty so that months[1] = "January"
            "Januari", "Februari", "Maret",
            "April", "Mei", "Juni",
            "Juli", "Agustus", "September",
            "Oktober", "November", "Desember"
        };

        // days[i] = number of days in month i
        int[] days = {
            0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
        };

        // check for leap year
        if (month == 2 && isLeapYear(year)) days[month] = 29;

        int d = day(month, 1, year);
        
        CalendarDto calendarDto = new CalendarDto();
        
        calendarDto.setBulan(month);
        calendarDto.setBulanDesc(months[month]);
        calendarDto.setTahun(year);
        
//        for (int i = 0; i < d; i++)
//        	System.out.print("   ");
//        for (int i = 1; i <= days[month]; i++) {
//        	System.out.print(i + " ");
//            if (((i + d) % 7 == 0) || (i == days[month])) System.out.println();
//        }
        
        List<CalendarDayDateDto> calendarDayDateDtos = new ArrayList<CalendarDayDateDto>();
        Integer seq = 0;
        for (int i = 1; i <= days[month]; i++) {
        	CalendarDayDateDto calendarDayDateDto = new CalendarDayDateDto();
        	calendarDayDateDto.setTanggal(i);
        	calendarDayDateDto.setHari(DAY_DATE[(d)%7]);
        	calendarDayDateDtos.add(calendarDayDateDto);
  //      	System.out.println("tanggal "+ i + " hari " + DAY_DATE[(d)%7]);
        	seq++;
        	d = d + 1;
        }

        calendarDto.setCalendarDayDateDtos(calendarDayDateDtos);
        return calendarDto;
    }
    
    public static void main(String[] args) {
        int month = 2;    // month (Jan = 1, Dec = 12)
        int year = 2020;   // year
        	
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        CalendarDto calendarDto = makingCalendar(calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR));
        System.out.println(new Gson().toJson(calendarDto));
    }

}

package id.co.oob.lib.common.merchant.onboarding.dto.otp;

public class OtpNotificationRequest {


	private String noPhone;
	private String message;
	
	
	public String getNoPhone() {
		return noPhone;
	}
	public void setNoPhone(String noPhone) {
		this.noPhone = noPhone;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}

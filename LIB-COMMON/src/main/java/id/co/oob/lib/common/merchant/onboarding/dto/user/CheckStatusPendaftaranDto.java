package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.Date;

public class CheckStatusPendaftaranDto {

	private Date statusDate;
	private String statusInDate;
	private String statusInTime;
	private String headerStatus;
	private String bodyStatus;
	private Boolean isOnOpen = false;
	private Boolean isOnTolak = false;
	private String percentage;
	
	
	
	
	
	public Boolean getIsOnTolak() {
		return isOnTolak;
	}
	public void setIsOnTolak(Boolean isOnTolak) {
		this.isOnTolak = isOnTolak;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getStatusInDate() {
		return statusInDate;
	}
	public void setStatusInDate(String statusInDate) {
		this.statusInDate = statusInDate;
	}
	public String getStatusInTime() {
		return statusInTime;
	}
	public void setStatusInTime(String statusInTime) {
		this.statusInTime = statusInTime;
	}
	public Boolean getIsOnOpen() {
		return isOnOpen;
	}
	public void setIsOnOpen(Boolean isOnOpen) {
		this.isOnOpen = isOnOpen;
	}
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	public String getHeaderStatus() {
		return headerStatus;
	}
	public void setHeaderStatus(String headerStatus) {
		this.headerStatus = headerStatus;
	}
	public String getBodyStatus() {
		return bodyStatus;
	}
	public void setBodyStatus(String bodyStatus) {
		this.bodyStatus = bodyStatus;
	}
	
	
	
}

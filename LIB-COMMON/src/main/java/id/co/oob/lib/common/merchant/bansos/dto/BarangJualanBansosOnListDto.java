package id.co.oob.lib.common.merchant.bansos.dto;

import java.math.BigDecimal;
import java.util.List;


public class BarangJualanBansosOnListDto {

	
	private List<BarangJualanBansosDto> jualanBansosDtos;
	private String sessionLogin;

	

	public List<BarangJualanBansosDto> getJualanBansosDtos() {
		return jualanBansosDtos;
	}

	public void setJualanBansosDtos(List<BarangJualanBansosDto> jualanBansosDtos) {
		this.jualanBansosDtos = jualanBansosDtos;
	}

	public String getSessionLogin() {
		return sessionLogin;
	}

	public void setSessionLogin(String sessionLogin) {
		this.sessionLogin = sessionLogin;
	}
	
}

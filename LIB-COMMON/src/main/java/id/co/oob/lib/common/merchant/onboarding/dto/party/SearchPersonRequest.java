package id.co.oob.lib.common.merchant.onboarding.dto.party;

public class SearchPersonRequest {
	
		private String cifNo;

		public String getCifNo() {
			return cifNo;
		}

		public void setCifNo(String cifNo) {
			this.cifNo = cifNo;
		}
		
}

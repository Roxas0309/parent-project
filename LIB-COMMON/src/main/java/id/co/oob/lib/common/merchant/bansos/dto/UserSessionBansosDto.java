package id.co.oob.lib.common.merchant.bansos.dto;

public class UserSessionBansosDto {

	private String session;

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}
	
	
	
}

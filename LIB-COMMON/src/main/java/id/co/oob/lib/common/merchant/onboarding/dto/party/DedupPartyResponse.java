package id.co.oob.lib.common.merchant.onboarding.dto.party;

public class DedupPartyResponse {

	        private String NomorCIF;
			private String ResponseCode;
			private String ResponseMessage;
			
			public String getNomorCIF() {
				return NomorCIF;
			}
			public void setNomorCIF(String nomorCIF) {
				NomorCIF = nomorCIF;
			}
			public String getResponseCode() {
				return ResponseCode;
			}
			public void setResponseCode(String responseCode) {
				ResponseCode = responseCode;
			}
			public String getResponseMessage() {
				return ResponseMessage;
			}
			public void setResponseMessage(String responseMessage) {
				ResponseMessage = responseMessage;
			}
			
			
}

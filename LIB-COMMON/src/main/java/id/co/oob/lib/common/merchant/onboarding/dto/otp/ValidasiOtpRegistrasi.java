package id.co.oob.lib.common.merchant.onboarding.dto.otp;

public class ValidasiOtpRegistrasi {

	public String encrpyptValidationOtp;
	public String otpValue;
	public String getEncrpyptValidationOtp() {
		return encrpyptValidationOtp;
	}
	public void setEncrpyptValidationOtp(String encrpyptValidationOtp) {
		this.encrpyptValidationOtp = encrpyptValidationOtp;
	}
	public String getOtpValue() {
		return otpValue;
	}
	public void setOtpValue(String otpValue) {
		this.otpValue = otpValue;
	}
	
	
}

package id.co.oob.lib.common.merchant.onboarding.dto.user;

public class UserOobValidasiResponse {

	private String cifInEncrypt;
	private String address;
	private String noTelp;
	private String namaPemilikUsaha;
	private String noRekeningValid;
	
	

	public String getNoRekeningValid() {
		return noRekeningValid;
	}

	public String getNamaPemilikUsaha() {
		return namaPemilikUsaha;
	}

	public void setNamaPemilikUsaha(String namaPemilikUsaha) {
		this.namaPemilikUsaha = namaPemilikUsaha;
	}

	public String getCifInEncrypt() {
		return cifInEncrypt;
	}

	public void setCifInEncrypt(String cifInEncrypt) {
		this.cifInEncrypt = cifInEncrypt;
	}
	

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	

	public String getNoTelp() {
		return noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public UserOobValidasiResponse(String cifInEncrypt, String address) {
		super();
		this.cifInEncrypt = cifInEncrypt;
		this.address = address;
	}

	public UserOobValidasiResponse(String cifInEncrypt, String address, String noTelp, 
			String namaPemilikUsaha, String noRekeningValid) {
		super();
		this.namaPemilikUsaha = namaPemilikUsaha;
		this.cifInEncrypt = cifInEncrypt;
		this.address = address;
		this.noTelp = noTelp;
		this.noRekeningValid = noRekeningValid;
	}
	
	
	
	
	
}

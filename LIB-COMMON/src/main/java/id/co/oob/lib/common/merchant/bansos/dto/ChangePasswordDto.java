package id.co.oob.lib.common.merchant.bansos.dto;

public class ChangePasswordDto {

    private String userName;
    private String currPass;
    private String newPass;
    private String sessionUser;

    public String getSessionUser() {
        return sessionUser;
    }

    public void setSessionUser(String sessionUser) {
        this.sessionUser = sessionUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCurrPass() {
        return currPass;
    }

    public void setCurrPass(String currPass) {
        this.currPass = currPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }
}

package id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat;

import java.math.BigDecimal;
import java.util.List;

public class RiwayatPencairanHdrDto {
	private String pencairanDate;
	private String pencairanDateWithoutYear;
	private BigDecimal totalPencairan;
	private String namaHari;
	private String totalPencairanInString;
	private List<RiwayatPencairanDtlDto> allRiwayats;
	
	
	
	public String getNamaHari() {
		return namaHari;
	}
	public void setNamaHari(String namaHari) {
		this.namaHari = namaHari;
	}
	public BigDecimal getTotalPencairan() {
		return totalPencairan;
	}
	public void setTotalPencairan(BigDecimal totalPencairan) {
		this.totalPencairan = totalPencairan;
	}
	public String getTotalPencairanInString() {
		return totalPencairanInString;
	}
	public void setTotalPencairanInString(String totalPencairanInString) {
		this.totalPencairanInString = totalPencairanInString;
	}
	public String getPencairanDateWithoutYear() {
		return pencairanDateWithoutYear;
	}
	public void setPencairanDateWithoutYear(String pencairanDateWithoutYear) {
		this.pencairanDateWithoutYear = pencairanDateWithoutYear;
	}
	public String getPencairanDate() {
		return pencairanDate;
	}
	public void setPencairanDate(String pencairanDate) {
		this.pencairanDate = pencairanDate;
	}
	public List<RiwayatPencairanDtlDto> getAllRiwayats() {
		return allRiwayats;
	}
	public void setAllRiwayats(List<RiwayatPencairanDtlDto> allRiwayats) {
		this.allRiwayats = allRiwayats;
	}
	
}

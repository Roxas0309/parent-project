package id.co.oob.lib.common.merchant.onboarding.dto.mandiri;

public class ValidateOtpDto {
		private String externalId;
		private String timestamp;
		private String responseCode;
		private String authenticationMethod;
		private String counter;
		private String otpKey1;
		private String otpKey2;
		private String otpKey3;
		private String handphone;
		private String responseMessage;
		public String getExternalId() {
			return externalId;
		}
		public void setExternalId(String externalId) {
			this.externalId = externalId;
		}
		public String getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}
		public String getResponseCode() {
			return responseCode;
		}
		public void setResponseCode(String responseCode) {
			this.responseCode = responseCode;
		}
		public String getAuthenticationMethod() {
			return authenticationMethod;
		}
		public void setAuthenticationMethod(String authenticationMethod) {
			this.authenticationMethod = authenticationMethod;
		}
		public String getCounter() {
			return counter;
		}
		public void setCounter(String counter) {
			this.counter = counter;
		}
		public String getOtpKey1() {
			return otpKey1;
		}
		public void setOtpKey1(String otpKey1) {
			this.otpKey1 = otpKey1;
		}
		public String getOtpKey2() {
			return otpKey2;
		}
		public void setOtpKey2(String otpKey2) {
			this.otpKey2 = otpKey2;
		}
		public String getOtpKey3() {
			return otpKey3;
		}
		public void setOtpKey3(String otpKey3) {
			this.otpKey3 = otpKey3;
		}
		public String getHandphone() {
			return handphone;
		}
		public void setHandphone(String handphone) {
			this.handphone = handphone;
		}
		public String getResponseMessage() {
			return responseMessage;
		}
		public void setResponseMessage(String responseMessage) {
			this.responseMessage = responseMessage;
		}
		
		
		
}

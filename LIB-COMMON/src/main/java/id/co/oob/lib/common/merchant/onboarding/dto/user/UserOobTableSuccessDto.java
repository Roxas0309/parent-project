package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.Date;

public class UserOobTableSuccessDto {
	
    private Long id;
	
	private String noHandphone;

	private String cifNumber;
	
	private String namaPemilikUsaha;
	
	private String dob;
	
	private String nomorKtp;
	
	private String namaIbuKandung;
	
	private String nomorRekening;
	
	private String alamatPemilikUsaha;
	
	private String emailPemilikUsaha;

	private String pathKtp;
		
	private String pathWajahKtp;
		
	private String pathNpwp;
    
    private String noNpwp;
    
    private String namaUsaha;
    
    private String nomorTelp;
    
    private String jenisUsaha;
    private String jenisUsahaDesc;
    
    private String omzet;
    private String omzetDesc;
    
   	private String pathFotoTempatUsaha;
    
   	private String pathFotoBarangAtauJasa;
    
    private String pathFotoPemilikTempatUsaha;
    
    private String kategoriLokasiUsaha;
    private String kategoriLokasiUsahaDesc;
    
    private String jenisLokasiUsaha;
    private String jenisLokasiUsahaDesc;
    
    private Long idKelurahan;
    private String detailKelurahanDesc;
    
    private String alamatLokasiUsahaSaatIni;

    private Date createDate;
    
    /*  Put Only 1,2,3,4,5
     *	1 == When User already registered
     *  2 == Ketika User data berhasil dikirimkan ke MAAS
     *  3 == Ketika User Data Berhasil dikirimkan ke BI untuk validasi.
     *	4 == Ketika User Data Berhasil menerima QR number dengan filenya dari petent
     *	5 == Ketika User Data Berhasil create password, dikirim pesan dan email
     */ 
    private Integer idStatusProses;  
    private String idStatusProsesDesc;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNoHandphone() {
		return noHandphone;
	}
	public void setNoHandphone(String noHandphone) {
		this.noHandphone = noHandphone;
	}
	public String getCifNumber() {
		return cifNumber;
	}
	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}
	public String getNamaPemilikUsaha() {
		return namaPemilikUsaha;
	}
	public void setNamaPemilikUsaha(String namaPemilikUsaha) {
		this.namaPemilikUsaha = namaPemilikUsaha;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getNomorKtp() {
		return nomorKtp;
	}
	public void setNomorKtp(String nomorKtp) {
		this.nomorKtp = nomorKtp;
	}
	public String getNamaIbuKandung() {
		return namaIbuKandung;
	}
	public void setNamaIbuKandung(String namaIbuKandung) {
		this.namaIbuKandung = namaIbuKandung;
	}
	public String getNomorRekening() {
		return nomorRekening;
	}
	public void setNomorRekening(String nomorRekening) {
		this.nomorRekening = nomorRekening;
	}
	public String getAlamatPemilikUsaha() {
		return alamatPemilikUsaha;
	}
	public void setAlamatPemilikUsaha(String alamatPemilikUsaha) {
		this.alamatPemilikUsaha = alamatPemilikUsaha;
	}
	public String getEmailPemilikUsaha() {
		return emailPemilikUsaha;
	}
	public void setEmailPemilikUsaha(String emailPemilikUsaha) {
		this.emailPemilikUsaha = emailPemilikUsaha;
	}
	public String getPathKtp() {
		return pathKtp;
	}
	public void setPathKtp(String pathKtp) {
		this.pathKtp = pathKtp;
	}
	public String getPathWajahKtp() {
		return pathWajahKtp;
	}
	public void setPathWajahKtp(String pathWajahKtp) {
		this.pathWajahKtp = pathWajahKtp;
	}
	public String getPathNpwp() {
		return pathNpwp;
	}
	public void setPathNpwp(String pathNpwp) {
		this.pathNpwp = pathNpwp;
	}
	public String getNoNpwp() {
		return noNpwp;
	}
	public void setNoNpwp(String noNpwp) {
		this.noNpwp = noNpwp;
	}
	public String getNamaUsaha() {
		return namaUsaha;
	}
	public void setNamaUsaha(String namaUsaha) {
		this.namaUsaha = namaUsaha;
	}
	public String getNomorTelp() {
		return nomorTelp;
	}
	public void setNomorTelp(String nomorTelp) {
		this.nomorTelp = nomorTelp;
	}
	public String getJenisUsaha() {
		return jenisUsaha;
	}
	public void setJenisUsaha(String jenisUsaha) {
		this.jenisUsaha = jenisUsaha;
	}
	public String getJenisUsahaDesc() {
		return jenisUsahaDesc;
	}
	public void setJenisUsahaDesc(String jenisUsahaDesc) {
		this.jenisUsahaDesc = jenisUsahaDesc;
	}
	public String getOmzet() {
		return omzet;
	}
	public void setOmzet(String omzet) {
		this.omzet = omzet;
	}
	public String getOmzetDesc() {
		return omzetDesc;
	}
	public void setOmzetDesc(String omzetDesc) {
		this.omzetDesc = omzetDesc;
	}
	public String getPathFotoTempatUsaha() {
		return pathFotoTempatUsaha;
	}
	public void setPathFotoTempatUsaha(String pathFotoTempatUsaha) {
		this.pathFotoTempatUsaha = pathFotoTempatUsaha;
	}
	public String getPathFotoBarangAtauJasa() {
		return pathFotoBarangAtauJasa;
	}
	public void setPathFotoBarangAtauJasa(String pathFotoBarangAtauJasa) {
		this.pathFotoBarangAtauJasa = pathFotoBarangAtauJasa;
	}
	public String getPathFotoPemilikTempatUsaha() {
		return pathFotoPemilikTempatUsaha;
	}
	public void setPathFotoPemilikTempatUsaha(String pathFotoPemilikTempatUsaha) {
		this.pathFotoPemilikTempatUsaha = pathFotoPemilikTempatUsaha;
	}
	public String getKategoriLokasiUsaha() {
		return kategoriLokasiUsaha;
	}
	public void setKategoriLokasiUsaha(String kategoriLokasiUsaha) {
		this.kategoriLokasiUsaha = kategoriLokasiUsaha;
	}
	public String getKategoriLokasiUsahaDesc() {
		return kategoriLokasiUsahaDesc;
	}
	public void setKategoriLokasiUsahaDesc(String kategoriLokasiUsahaDesc) {
		this.kategoriLokasiUsahaDesc = kategoriLokasiUsahaDesc;
	}
	public String getJenisLokasiUsaha() {
		return jenisLokasiUsaha;
	}
	public void setJenisLokasiUsaha(String jenisLokasiUsaha) {
		this.jenisLokasiUsaha = jenisLokasiUsaha;
	}
	public String getJenisLokasiUsahaDesc() {
		return jenisLokasiUsahaDesc;
	}
	public void setJenisLokasiUsahaDesc(String jenisLokasiUsahaDesc) {
		this.jenisLokasiUsahaDesc = jenisLokasiUsahaDesc;
	}
	public Long getIdKelurahan() {
		return idKelurahan;
	}
	public void setIdKelurahan(Long idKelurahan) {
		this.idKelurahan = idKelurahan;
	}
	
	public String getDetailKelurahanDesc() {
		return detailKelurahanDesc;
	}
	public void setDetailKelurahanDesc(String detailKelurahanDesc) {
		this.detailKelurahanDesc = detailKelurahanDesc;
	}
	public String getAlamatLokasiUsahaSaatIni() {
		return alamatLokasiUsahaSaatIni;
	}
	public void setAlamatLokasiUsahaSaatIni(String alamatLokasiUsahaSaatIni) {
		this.alamatLokasiUsahaSaatIni = alamatLokasiUsahaSaatIni;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Integer getIdStatusProses() {
		return idStatusProses;
	}
	public void setIdStatusProses(Integer idStatusProses) {
		this.idStatusProses = idStatusProses;
	}
	public String getIdStatusProsesDesc() {
		return idStatusProsesDesc;
	}
	public void setIdStatusProsesDesc(String idStatusProsesDesc) {
		this.idStatusProsesDesc = idStatusProsesDesc;
	}  
    
    
}

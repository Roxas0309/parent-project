package id.co.oob.lib.common.merchant.onboarding.throwable;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.LOCKED)
public class LockedDataException extends RuntimeException{
	private static final long serialVersionUID = 5628869454492978167L;


	public LockedDataException(String message) {
		super(message);
	}

	
}

package id.co.oob.lib.common.merchant.onboarding.dto.mandiri;

import java.util.List;

public class CustomerAccountInquiryByCifDto {
	private String responseCode;
	private String responseMessage;
	private String name;
	private String customerAddress1;
	private String customerAddress2;
	private String customerAddress3;
	private String customerAddress4;
	private String customerAddress5;
	private String noRekeningYangValid;
	private List<AccountInfoDetailDto> accountInfoDetails;
	private List<PhoneInfoDetailDto> phoneInfoDetails;
	
	
	
	public String getNoRekeningYangValid() {
		return noRekeningYangValid;
	}
	public void setNoRekeningYangValid(String noRekeningYangValid) {
		this.noRekeningYangValid = noRekeningYangValid;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCustomerAddress1() {
		return customerAddress1;
	}
	public void setCustomerAddress1(String customerAddress1) {
		this.customerAddress1 = customerAddress1;
	}
	public String getCustomerAddress2() {
		return customerAddress2;
	}
	public void setCustomerAddress2(String customerAddress2) {
		this.customerAddress2 = customerAddress2;
	}
	public String getCustomerAddress3() {
		return customerAddress3;
	}
	public void setCustomerAddress3(String customerAddress3) {
		this.customerAddress3 = customerAddress3;
	}
	public String getCustomerAddress4() {
		return customerAddress4;
	}
	public void setCustomerAddress4(String customerAddress4) {
		this.customerAddress4 = customerAddress4;
	}
	public String getCustomerAddress5() {
		return customerAddress5;
	}
	public void setCustomerAddress5(String customerAddress5) {
		this.customerAddress5 = customerAddress5;
	}
	public List<AccountInfoDetailDto> getAccountInfoDetails() {
		return accountInfoDetails;
	}
	public void setAccountInfoDetails(List<AccountInfoDetailDto> accountInfoDetails) {
		this.accountInfoDetails = accountInfoDetails;
	}
	public List<PhoneInfoDetailDto> getPhoneInfoDetails() {
		return phoneInfoDetails;
	}
	public void setPhoneInfoDetails(List<PhoneInfoDetailDto> phoneInfoDetails) {
		this.phoneInfoDetails = phoneInfoDetails;
	}
	
}

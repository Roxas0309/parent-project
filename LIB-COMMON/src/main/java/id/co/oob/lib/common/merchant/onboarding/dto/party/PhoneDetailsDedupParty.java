package id.co.oob.lib.common.merchant.onboarding.dto.party;

public class PhoneDetailsDedupParty {
	private String phoneNumber;
	private String phoneSourceSystem;
	private String phoneNoType;
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneSourceSystem() {
		return phoneSourceSystem;
	}
	public void setPhoneSourceSystem(String phoneSourceSystem) {
		this.phoneSourceSystem = phoneSourceSystem;
	}
	public String getPhoneNoType() {
		return phoneNoType;
	}
	public void setPhoneNoType(String phoneNoType) {
		this.phoneNoType = phoneNoType;
	}
	
	
}

package id.co.oob.lib.common.merchant.bansos.dto;

import java.util.List;

public class BansosHapusDataDto {
	
	private List<Long> idDataHapus;
	private String sessionUser;
	public List<Long> getIdDataHapus() {
		return idDataHapus;
	}
	public void setIdDataHapus(List<Long> idDataHapus) {
		this.idDataHapus = idDataHapus;
	}
	public String getSessionUser() {
		return sessionUser;
	}
	public void setSessionUser(String sessionUser) {
		this.sessionUser = sessionUser;
	}
	
	
	
}

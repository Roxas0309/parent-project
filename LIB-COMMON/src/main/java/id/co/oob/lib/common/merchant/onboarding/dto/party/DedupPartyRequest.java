package id.co.oob.lib.common.merchant.onboarding.dto.party;

public class DedupPartyRequest {
				
	private String NamaPemilikUsaha;
	private String NamaIbuKandung;
	private String NomorKTP;
	private String DateOfBirth;  //remind me this dob Format : ‘yyyy-MM-dd’;
	
	
	public String getNamaPemilikUsaha() {
		return NamaPemilikUsaha;
	}
	public void setNamaPemilikUsaha(String namaPemilikUsaha) {
		NamaPemilikUsaha = namaPemilikUsaha;
	}
	public String getNamaIbuKandung() {
		return NamaIbuKandung;
	}
	public void setNamaIbuKandung(String namaIbuKandung) {
		NamaIbuKandung = namaIbuKandung;
	}
	public String getNomorKTP() {
		return NomorKTP;
	}
	public void setNomorKTP(String nomorKTP) {
		NomorKTP = nomorKTP;
	}
	public String getDateOfBirth() {
		return DateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}
	
	
}

package id.co.oob.lib.common.merchant.onboarding.dto.masterUsaha;

public class MasterUsahaPropertiesTableDto {
	private String idName; 
	private String nama;
	private String tipe;
	
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public String getIdName() {
		return idName;
	}
	public void setIdName(String idName) {
		this.idName = idName;
	}
	
	
	
	
}

package id.co.oob.lib.common.merchant.onboarding.dto.party;

public class SearchPersonResponse {
	 	private String responseCode;
	 	private String responseMessage;
		private String nama;
		private String customerAddress1;
		private String customerAddress2;
		private String customerAddress3;
		private String customerAddress4;
		private String customerAddress5;
		private PhoneDetailsDedupParty phoneInfoDetails;
		private AccountInfoDetailsDedupParty accountInfoDetails;
		public String getResponseCode() {
			return responseCode;
		}
		public void setResponseCode(String responseCode) {
			this.responseCode = responseCode;
		}
		public String getResponseMessage() {
			return responseMessage;
		}
		public void setResponseMessage(String responseMessage) {
			this.responseMessage = responseMessage;
		}
		public String getNama() {
			return nama;
		}
		public void setNama(String nama) {
			this.nama = nama;
		}
		public String getCustomerAddress1() {
			return customerAddress1;
		}
		public void setCustomerAddress1(String customerAddress1) {
			this.customerAddress1 = customerAddress1;
		}
		public String getCustomerAddress2() {
			return customerAddress2;
		}
		public void setCustomerAddress2(String customerAddress2) {
			this.customerAddress2 = customerAddress2;
		}
		public String getCustomerAddress3() {
			return customerAddress3;
		}
		public void setCustomerAddress3(String customerAddress3) {
			this.customerAddress3 = customerAddress3;
		}
		public String getCustomerAddress4() {
			return customerAddress4;
		}
		public void setCustomerAddress4(String customerAddress4) {
			this.customerAddress4 = customerAddress4;
		}
		public String getCustomerAddress5() {
			return customerAddress5;
		}
		public void setCustomerAddress5(String customerAddress5) {
			this.customerAddress5 = customerAddress5;
		}
		public PhoneDetailsDedupParty getPhoneInfoDetails() {
			return phoneInfoDetails;
		}
		public void setPhoneInfoDetails(PhoneDetailsDedupParty phoneInfoDetails) {
			this.phoneInfoDetails = phoneInfoDetails;
		}
		public AccountInfoDetailsDedupParty getAccountInfoDetails() {
			return accountInfoDetails;
		}
		public void setAccountInfoDetails(AccountInfoDetailsDedupParty accountInfoDetails) {
			this.accountInfoDetails = accountInfoDetails;
		}
	
}

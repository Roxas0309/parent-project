package id.co.oob.lib.common.merchant.onboarding.dto.alamat;

public class ProvinsiTableDto {
	private Long id; 

	private String nama;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
}

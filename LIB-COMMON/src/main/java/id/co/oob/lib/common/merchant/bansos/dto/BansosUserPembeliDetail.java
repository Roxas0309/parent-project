package id.co.oob.lib.common.merchant.bansos.dto;

public class BansosUserPembeliDetail {
	
	private String noEktpPembeli;
	private String namaLengkapPembeli;
	private String fotoEktpBase64;
	private String fotoWajahdenganEktp;
	private String fotoSuratKeterangan;
	private String fotoPembeliDanBarang;
	
	
	
	public String getFotoEktpBase64() {
		return fotoEktpBase64;
	}
	public void setFotoEktpBase64(String fotoEktpBase64) {
		this.fotoEktpBase64 = fotoEktpBase64;
	}
	public String getFotoWajahdenganEktp() {
		return fotoWajahdenganEktp;
	}
	public void setFotoWajahdenganEktp(String fotoWajahdenganEktp) {
		this.fotoWajahdenganEktp = fotoWajahdenganEktp;
	}
	public String getFotoSuratKeterangan() {
		return fotoSuratKeterangan;
	}
	public void setFotoSuratKeterangan(String fotoSuratKeterangan) {
		this.fotoSuratKeterangan = fotoSuratKeterangan;
	}
	public String getFotoPembeliDanBarang() {
		return fotoPembeliDanBarang;
	}
	public void setFotoPembeliDanBarang(String fotoPembeliDanBarang) {
		this.fotoPembeliDanBarang = fotoPembeliDanBarang;
	}
	public String getNoEktpPembeli() {
		return noEktpPembeli;
	}
	public void setNoEktpPembeli(String noEktpPembeli) {
		this.noEktpPembeli = noEktpPembeli;
	}
	public String getNamaLengkapPembeli() {
		return namaLengkapPembeli;
	}
	public void setNamaLengkapPembeli(String namaLengkapPembeli) {
		this.namaLengkapPembeli = namaLengkapPembeli;
	}
	
	
	
	
}

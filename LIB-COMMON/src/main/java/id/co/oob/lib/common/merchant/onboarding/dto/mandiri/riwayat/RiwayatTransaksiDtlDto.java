package id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat;

import java.math.BigDecimal;

public class RiwayatTransaksiDtlDto {

	private String pmtDate;
	private String authSeqNo;
	private String issuerPayment;
	private String cpanNumber;
	private String cpanNumberInMask;
	private String time;
	private String date;
	private String number;
	private String feeAmount;
	private BigDecimal feeAmountNumber;
	private String authAmount;
	private BigDecimal authAmountNumber;
	private Boolean isTransferToRek = false;
	private Boolean isHold = false;
	private String pmtHoldTime;
	private String pmtHoldDate;
	private String releaseHoldDate;
	private String releaseHoldTime;
	private RiwayatTransaksiDtlRincianDto detail;
	private String postingClcd;
	private String timePembayaran;
	
	
	
	
	
	public String getPmtHoldTime() {
		return pmtHoldTime;
	}
	public void setPmtHoldTime(String pmtHoldTime) {
		this.pmtHoldTime = pmtHoldTime;
	}
	public String getReleaseHoldTime() {
		return releaseHoldTime;
	}
	public void setReleaseHoldTime(String releaseHoldTime) {
		this.releaseHoldTime = releaseHoldTime;
	}
	public String getReleaseHoldDate() {
		return releaseHoldDate;
	}
	public void setReleaseHoldDate(String releaseHoldDate) {
		this.releaseHoldDate = releaseHoldDate;
	}
	public Boolean getIsHold() {
		return isHold;
	}
	public void setIsHold(Boolean isHold) {
		this.isHold = isHold;
	}
	public String getPmtHoldDate() {
		return pmtHoldDate;
	}
	public void setPmtHoldDate(String pmtHoldDate) {
		this.pmtHoldDate = pmtHoldDate;
	}
	public String getPmtDate() {
		return pmtDate;
	}
	public void setPmtDate(String pmtDate) {
		this.pmtDate = pmtDate;
	}
	public String getTimePembayaran() {
		return timePembayaran;
	}
	public void setTimePembayaran(String timePembayaran) {
		this.timePembayaran = timePembayaran;
	}
	public String getPostingClcd() {
		return postingClcd;
	}
	public void setPostingClcd(String postingClcd) {
		this.postingClcd = postingClcd;
	}
	public BigDecimal getAuthAmountNumber() {
		return authAmountNumber;
	}
	public void setAuthAmountNumber(BigDecimal authAmountNumber) {
		this.authAmountNumber = authAmountNumber;
	}
	public BigDecimal getFeeAmountNumber() {
		return feeAmountNumber;
	}
	public void setFeeAmountNumber(BigDecimal feeAmountNumber) {
		this.feeAmountNumber = feeAmountNumber;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getAuthSeqNo() {
		return authSeqNo;
	}
	public void setAuthSeqNo(String authSeqNo) {
		this.authSeqNo = authSeqNo;
	}
	public String getIssuerPayment() {
		return issuerPayment;
	}
	public void setIssuerPayment(String issuerPayment) {
		this.issuerPayment = issuerPayment;
	}
	public String getCpanNumber() {
		return cpanNumber;
	}
	public void setCpanNumber(String cpanNumber) {
		this.cpanNumber = cpanNumber;
	}
	public String getCpanNumberInMask() {
		return cpanNumberInMask;
	}
	public void setCpanNumberInMask(String cpanNumberInMask) {
		this.cpanNumberInMask = cpanNumberInMask;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(String feeAmount) {
		this.feeAmount = feeAmount;
	}
	public String getAuthAmount() {
		return authAmount;
	}
	public void setAuthAmount(String authAmount) {
		this.authAmount = authAmount;
	}
	public Boolean getIsTransferToRek() {
		return isTransferToRek;
	}
	public void setIsTransferToRek(Boolean isTransferToRek) {
		this.isTransferToRek = isTransferToRek;
	}
	public RiwayatTransaksiDtlRincianDto getDetail() {
		return detail;
	}
	public void setDetail(RiwayatTransaksiDtlRincianDto detail) {
		this.detail = detail;
	}

	
}

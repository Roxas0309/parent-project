package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.Date;

public class CifDecryptor {

	private String cifNumber;
	private String dateEncryptor;
	private String purposeTo;
	
	
	
	public String getPurposeTo() {
		return purposeTo;
	}
	public void setPurposeTo(String purposeTo) {
		this.purposeTo = purposeTo;
	}
	public String getCifNumber() {
		return cifNumber;
	}
	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}
	public String getDateEncryptor() {
		return dateEncryptor;
	}
	public void setDateEncryptor(String dateEncryptor) {
		this.dateEncryptor = dateEncryptor;
	}
	
	
	
	
}

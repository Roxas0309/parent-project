package id.co.oob.lib.common.merchant.onboarding.dto.alamat;

public class KecamatanTableDto {
	private Long id; 
	private Long id_kota; 
	private String nama;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId_kota() {
		return id_kota;
	}
	public void setId_kota(Long id_kota) {
		this.id_kota = id_kota;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
}

package id.co.oob.lib.common.merchant.onboarding.dto.otp;

public class OtpGenerateTokenResponse {
		private String externalId;
		private String timestamp;
		private String responseCode;
		private String responseMessage;
		private String encrpyptValidationOtp;
		private String phoneNumber;
		private Boolean otpStatusError;
		
		
		
		
		public Boolean getOtpStatusError() {
			return otpStatusError;
		}
		public void setOtpStatusError(Boolean otpStatusError) {
			this.otpStatusError = otpStatusError;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public String getExternalId() {
			return externalId;
		}
		public void setExternalId(String externalId) {
			this.externalId = externalId;
		}
		public String getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}
		public String getResponseCode() {
			return responseCode;
		}
		public void setResponseCode(String responseCode) {
			this.responseCode = responseCode;
		}
		public String getResponseMessage() {
			return responseMessage;
		}
		public void setResponseMessage(String responseMessage) {
			this.responseMessage = responseMessage;
		}
		public String getEncrpyptValidationOtp() {
			return encrpyptValidationOtp;
		}
		public void setEncrpyptValidationOtp(String encrpyptValidationOtp) {
			this.encrpyptValidationOtp = encrpyptValidationOtp;
		}
		
		
}

package id.co.oob.lib.common.merchant.onboarding.dto;

public class TBACMERPMTCTNT_MSTDto {
	private String mid;
	private Integer pmtAmt;
	private String tkNo;
	private Integer saleCnt;
	private String pmtDate;
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public Integer getPmtAmt() {
		return pmtAmt;
	}
	public void setPmtAmt(Integer pmtAmt) {
		this.pmtAmt = pmtAmt;
	}
	public String getTkNo() {
		return tkNo;
	}
	public void setTkNo(String tkNo) {
		this.tkNo = tkNo;
	}
	public Integer getSaleCnt() {
		return saleCnt;
	}
	public void setSaleCnt(Integer saleCnt) {
		this.saleCnt = saleCnt;
	}
	public String getPmtDate() {
		return pmtDate;
	}
	public void setPmtDate(String pmtDate) {
		this.pmtDate = pmtDate;
	}
	
	
}

package id.co.oob.lib.common.merchant.onboarding.dto;

public class ResponseErrorMessage {

	private String message;
	private Boolean status;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public ResponseErrorMessage(String message, Boolean status) {
		super();
		this.message = message;
		this.status = status;
	}
	
	
}

package id.co.oob.lib.common.merchant.onboarding.ws;

public class Greeting {
	 private String content;

	    public Greeting() {}

	    public Greeting(String content) {
	        this.content = content;
	    }

	    public String getContent() {
	        return content;
	    }
}

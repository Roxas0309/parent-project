package id.co.oob.lib.common.merchant.onboarding.encryptor;

import java.util.Base64;

import com.google.gson.Gson;

import id.co.oob.lib.common.merchant.onboarding.BaseCommon;

public class Base64EncodeCustom {

	public static String resultEncodeStringSmsBlast(String username, String password) {
		System.out.println("Informasi authorization sms-blast : Username : " + username + ", password : " + password);
		
		String originalInput = username+":"+password;
		return Base64.getEncoder().encodeToString(originalInput.getBytes());
	}
	
	public static String resultDecodeStringSmsBlast(String encodeString) {
		byte[] decodedBytes = Base64.getDecoder().decode(encodeString);
		String decodedString = new String(decodedBytes);
		return decodedString;
	}
	
	public static String encryptPassword(String password) {
		String encrypt = "";
		encrypt = EncryptDataFiller.encrypt(new Gson().toJson(password), "PASSWORD_ME");
		return encrypt;
	}
	
	public static String decryptPassword(String decPassword) {
		String dec = "";
		 dec = EncryptDataFiller.decrypt(decPassword, "PASSWORD_ME");
		 String value = "";
		try {
			value = BaseCommon.mapperJsonToSingleDto(dec, String.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}
	
	public static void main(String[] args) {
		String value = "Roxas0309.";
		String enc = (encryptPassword(value));
		System.out.println(enc);
		String denc = decryptPassword(enc);
		System.out.println(denc.equals(value));
	}
}

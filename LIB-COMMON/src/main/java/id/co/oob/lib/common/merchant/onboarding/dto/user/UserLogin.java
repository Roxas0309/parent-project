package id.co.oob.lib.common.merchant.onboarding.dto.user;

public class UserLogin {

	private String noPhone;
	private String password;
	private String tokenLocal;
	private String sessionLocal;
	
	
	
	public String getSessionLocal() {
		return sessionLocal;
	}
	public void setSessionLocal(String sessionLocal) {
		this.sessionLocal = sessionLocal;
	}
	public String getTokenLocal() {
		return tokenLocal;
	}
	public void setTokenLocal(String tokenLocal) {
		this.tokenLocal = tokenLocal;
	}
	public String getNoPhone() {
		return noPhone;
	}
	public void setNoPhone(String noPhone) {
		this.noPhone = noPhone;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}

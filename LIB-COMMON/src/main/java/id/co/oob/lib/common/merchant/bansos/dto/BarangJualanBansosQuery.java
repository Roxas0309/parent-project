package id.co.oob.lib.common.merchant.bansos.dto;

import java.math.BigDecimal;


public class BarangJualanBansosQuery {

	private String searchData;
	private String sessionToken;
	private Long addNewData;
	
	
	
	
	public Long getAddNewData() {
		return addNewData;
	}
	public void setAddNewData(Long addNewData) {
		this.addNewData = addNewData;
	}
	public String getSearchData() {
		return searchData;
	}
	public void setSearchData(String searchData) {
		this.searchData = searchData;
	}
	public String getSessionToken() {
		return sessionToken;
	}
	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}
	
	

	
}

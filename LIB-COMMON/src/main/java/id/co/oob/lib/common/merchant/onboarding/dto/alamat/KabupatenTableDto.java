package id.co.oob.lib.common.merchant.onboarding.dto.alamat;




public class KabupatenTableDto {

	private Long id; 
	
	private Long id_provinsi; 
	
	private String nama;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_provinsi() {
		return id_provinsi;
	}

	public void setId_provinsi(Long id_provinsi) {
		this.id_provinsi = id_provinsi;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
}

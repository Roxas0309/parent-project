package id.co.oob.lib.common.merchant.onboarding;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import id.co.oob.lib.common.merchant.bansos.dto.UserBansosDecryption;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.CustomerAccountInquiryByCifDto;
import id.co.oob.lib.common.merchant.onboarding.dto.party.PhoneDetailsDedupParty;
import id.co.oob.lib.common.merchant.onboarding.dto.party.SearchPersonResponse;
import id.co.oob.lib.common.merchant.onboarding.dto.user.CifDecryptor;
import id.co.oob.lib.common.merchant.onboarding.encryptor.EncryptDataFiller;
import id.co.oob.lib.common.merchant.onboarding.throwable.AnauthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public class BaseCommon {
	protected static String MENU_REGISTRASI = "MENU_REGISTRASI";
	protected static String BANSOS_LOGIN = "BANSOS_LOGIN";
	protected static String CODE_HTTP = "codeHttp";
	protected static String CODE_ENGINE = "codeEngine";
	protected static String USER_OOB_DATE_FORMAT = "ddMMyyyy";
	protected static String USER_OOB_DATE_FORMAT_MANDIRI = "yyyy-MM-dd";
	protected MapperFacade mapperFacade = new DefaultMapperFactory.Builder().build().getMapperFacade();

	
	protected String changeDateToBansosFormatDate (Date date) {
		return new SimpleDateFormat("yyyyMMdd").format(date);
	}
	
	protected String changeDateToBansosFormatTime (Date date) {
		return new SimpleDateFormat("HHmmss").format(date);
	}
	
	protected String createIdWithTimeStamp(Date date) {
		return changeDateToBansosFormatDate(date)+changeDateToBansosFormatTime(date);
	}
	
	protected String encryptUserName(Long idSeq,String userName, String password, String secretKey) {
		UserBansosDecryption decryptor = new UserBansosDecryption();
		decryptor.setIdSeq(idSeq);
		decryptor.setUserName(userName);
		decryptor.setPassword(password);
		Date date = new Date();
		String sdf = new SimpleDateFormat("ddMMyyyyHHmmss").format(date);
		decryptor.setDateNow(sdf);
		
		String encResult = EncryptDataFiller.encrypt(new Gson().toJson(decryptor), secretKey);
		return encResult;
	}
	
	protected UserBansosDecryption getUserBansosDecryption(String encryptWord, String secretKey) {
		UserBansosDecryption decryptor = new UserBansosDecryption();
		String decrypt = EncryptDataFiller.decrypt(encryptWord, secretKey);
		System.out.println("result decrypt : " + decrypt);
		if(decrypt==null) {
			System.out.println("getUserBansosDecryption "+encryptWord+" "
					+ " Cannot Be Encrypt With Validate Your Secret Key "+secretKey);
			throw new AnauthorizedException("Cif Encryp "+encryptWord+" "
					+ " Cannot Be Encrypt With Validate Your Secret Key "+secretKey);
		}
		
		try {
			decryptor = mapperJsonToSingleDto(decrypt, UserBansosDecryption.class);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Cannot Mapping Decrypt to CifDecryptor.class");
			throw new InternalServerErrorException("Cannot Mapping Decrypt to CifDecryptor.class");
		}
		
		return decryptor;
	}

	
	protected SearchPersonResponse getResponsePersoneByCif(String cifNumber) {
		Map<String, Object> mapBody = new HashMap<String, Object>();
		mapBody.put("nomorCIF", cifNumber);
		PhoneDetailsDedupParty phoneDetailsDedupParty = new PhoneDetailsDedupParty();
		phoneDetailsDedupParty.setPhoneNumber("081310741625");
		System.out.println("map body : " + new Gson().toJson(mapBody));
		SearchPersonResponse personResponse = new SearchPersonResponse();
		personResponse.setPhoneInfoDetails(phoneDetailsDedupParty);
		personResponse.setCustomerAddress1("Jl Suka Maju No 5");
		personResponse.setCustomerAddress2("Komplek The Garden");
		personResponse.setCustomerAddress3("Kec Ciracas");
		personResponse.setCustomerAddress4("Jakarta Timur");
		personResponse.setCustomerAddress5("12345");
		return personResponse;
	}

	
	
	protected String getSaltString(Integer length) {

		String[] saltResult = new String[length];
		String resultSalt = "";
		for (int i = 0; i < saltResult.length; i++) {
			String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
			StringBuilder salt = new StringBuilder();
			Random rnd = new Random();
			while (salt.length() < 5) { // length of the random string.
				int index = (int) (rnd.nextFloat() * SALTCHARS.length());
				salt.append(SALTCHARS.charAt(index));
			}
			resultSalt += salt.toString();

			if (i != saltResult.length - 1) {
				resultSalt += "-";
			}
		}

		return resultSalt;

	}

	protected String randomPassword() {

		String resultSaltHurufBesar = "";
		String resultSaltHurufKecil = "";
		String resultSaltHurufNumeric = "";
			String SALTCHARSHURUFBESAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			String SALTCHARSHURUFKECIL = "abcdefghijklmnopqrstuvwxyz";
			String SALTCHARSHURUFNUMERIC = "1234567890";
			StringBuilder saltB = new StringBuilder();
			Random rndB = new Random();
			while (saltB.length() < 4) { // length of the random string.
				int index = (int) (rndB.nextFloat() * SALTCHARSHURUFBESAR.length());
				saltB.append(SALTCHARSHURUFBESAR.charAt(index));
			}
			resultSaltHurufBesar += saltB.toString();
			
			StringBuilder saltK = new StringBuilder();
			Random rndK = new Random();
			while (saltK.length() < 3) { // length of the random string.
				int index = (int) (rndK.nextFloat() * SALTCHARSHURUFKECIL.length());
				saltK.append(SALTCHARSHURUFKECIL.charAt(index));
			}
			
			resultSaltHurufKecil += saltK.toString();
			
			StringBuilder saltN = new StringBuilder();
			Random rndN = new Random();
			while (saltN.length() < 1) { // length of the random string.
				int index = (int) (rndN.nextFloat() * SALTCHARSHURUFNUMERIC.length());
				saltN.append(SALTCHARSHURUFNUMERIC.charAt(index));
			}
			
			resultSaltHurufNumeric += saltN.toString();

		

		String h = resultSaltHurufBesar+resultSaltHurufKecil+resultSaltHurufNumeric;
		return h;
	}

	public byte[] getBase64File(String base64) {
		byte[] fileData = DatatypeConverter.parseBase64Binary(base64.substring(base64.indexOf(",") + 1));
		return fileData;
	}

	public static Map<String, Object> mapperJsonToHashMap(String result) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Map<String, Object> finalMap = new HashMap<>();
		try {
			finalMap = mapper.readValue(result, new TypeReference<HashMap<String, Object>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
			finalMap.put("error_method", e.getMessage());
		}
		return finalMap;
	}

	public static <T> T mapperJsonToSingleDto(String json, Class<T> clazz) throws Exception {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		om.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		return om.readValue(json, clazz);
	}

	public static <T> T mapperHashmapToSingleDto(Map<String, Object> json, Class<T> clazz) throws Exception {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		om.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		return om.convertValue(json, clazz);
	}

	public static <T> List<T> mapperJsonToListDto(String json, Class<T> clazz) throws Exception {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		om.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		TypeFactory t = TypeFactory.defaultInstance();
		List<T> list = om.readValue(json, t.constructCollectionType(ArrayList.class, clazz));
		return list;
	}

	public static CloseableHttpClient createAcceptSelfSignedCertificateClientII() {
		CloseableHttpClient httpClient = null;
		try {
			httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
					.setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
						public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
							return true;
						}
					}).build()).build();
		} catch (KeyManagementException e) {
			// LOGGER.error("KeyManagementException in creating http client instance", e);
		} catch (NoSuchAlgorithmException e) {
			// LOGGER.error("NoSuchAlgorithmException in creating http client instance", e);
		} catch (KeyStoreException e) {
			// LOGGER.error("KeyStoreException in creating http client instance", e);
		}
		return httpClient;
	}

	@SuppressWarnings("rawtypes")
	public static ResponseEntity<String> wsBodySmsBlast(String url, MultiValueMap<String, String> map, HttpMethod method,
			Map<String, String> headerMap) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		if (headerMap != null) {
			for (Entry<String, String> hm : headerMap.entrySet()) {
				headers.add(hm.getKey(), hm.getValue());
			}

		}

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(createAcceptSelfSignedCertificateClientII());

		@SuppressWarnings("unchecked")
		HttpEntity<String> httpEntity = new HttpEntity(map, headers);

		System.err.println("body : " + new GsonBuilder().setPrettyPrinting().create().toJson(httpEntity.getBody()));
		System.err.println("header : " + new GsonBuilder().setPrettyPrinting().create().toJson(httpEntity.getHeaders()));
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		System.err.println("url yang diberikan : " + url);

		ResponseEntity<String> response = new ResponseEntity<String>(HttpStatus.OK);

		try {
			response = restTemplate.exchange(url, method, httpEntity, String.class);
		} catch (HttpClientErrorException exp) {
			if (Strings.isEmpty(exp.getResponseBodyAsString())) {
				response = new ResponseEntity<String>(exp.getMessage(), exp.getStatusCode());
			} else {
				response = new ResponseEntity<String>(exp.getResponseBodyAsString(), exp.getStatusCode());
			}
		} catch (HttpServerErrorException exp) {
			if (Strings.isEmpty(exp.getResponseBodyAsString())) {
				response = new ResponseEntity<String>(exp.getMessage(), exp.getStatusCode());
			} else {
				response = new ResponseEntity<String>(exp.getResponseBodyAsString(), exp.getStatusCode());
			}
		}
		System.err.println("hasil responsenya adalah : " + new GsonBuilder().setPrettyPrinting().create().toJson(response.getBody()));
		return response;
	}
	
	@SuppressWarnings("rawtypes")
	public static ResponseEntity<String> wsBody(String url, Object body, HttpMethod method,
			Map<String, String> headerMap) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		if (headerMap != null) {
			for (Entry<String, String> hm : headerMap.entrySet()) {
				headers.add(hm.getKey(), hm.getValue());
			}
		}

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(createAcceptSelfSignedCertificateClientII());

		@SuppressWarnings("unchecked")
		HttpEntity<String> httpEntity = new HttpEntity(body, headers);

		System.err.println("body : " + new Gson().toJson(httpEntity.getBody()));
		System.err.println("header : " + new Gson().toJson(httpEntity.getHeaders()));
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		System.err.println("url yang diberikan : " + url);

		ResponseEntity<String> response = new ResponseEntity<String>(HttpStatus.OK);

		try {
			response = restTemplate.exchange(url, method, httpEntity, String.class);
		} catch (HttpClientErrorException exp) {
			if (Strings.isEmpty(exp.getResponseBodyAsString())) {
				response = new ResponseEntity<String>(exp.getMessage(), exp.getStatusCode());
			} else {
				response = new ResponseEntity<String>(exp.getResponseBodyAsString(), exp.getStatusCode());
			}
		} catch (HttpServerErrorException exp) {
			if (Strings.isEmpty(exp.getResponseBodyAsString())) {
				response = new ResponseEntity<String>(exp.getMessage(), exp.getStatusCode());
			} else {
				response = new ResponseEntity<String>(exp.getResponseBodyAsString(), exp.getStatusCode());
			}
		}
		
		if(!url.contains("MaasQueryTrxCtl/queryMe")) {
			System.err.println("hasil responsenya adalah : " + new Gson().toJson(response));
		}
		
		return response;
	}

	protected Map<String, Object> successResponseCheck(Object result, String message, Integer code) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", result);
		map.put("message", message);
		map.put("code", code);
		return map;
	}

	protected Map<String, Object> validationWordingCheck(String reason, String message, Integer code) {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("error", reason);
		map.put("error_description", message);
		map.put("code", code);
		return map;
	}

}

package id.co.oob.lib.common.merchant.bansos.dto;

public class PencairanTransaksiDto {

	private String mid;
	private String pmtDate;
	private String pmtAmt;
	private String authDate;
	private String pencairanStatusWording;
	private Boolean isCair;
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getPmtDate() {
		return pmtDate;
	}
	public void setPmtDate(String pmtDate) {
		this.pmtDate = pmtDate;
	}
	public String getPmtAmt() {
		return pmtAmt;
	}
	public void setPmtAmt(String pmtAmt) {
		this.pmtAmt = pmtAmt;
	}
	public String getAuthDate() {
		return authDate;
	}
	public void setAuthDate(String authDate) {
		this.authDate = authDate;
	}
	public String getPencairanStatusWording() {
		return pencairanStatusWording;
	}
	public void setPencairanStatusWording(String pencairanStatusWording) {
		this.pencairanStatusWording = pencairanStatusWording;
	}
	public Boolean getIsCair() {
		return isCair;
	}
	public void setIsCair(Boolean isCair) {
		this.isCair = isCair;
	}
	
}

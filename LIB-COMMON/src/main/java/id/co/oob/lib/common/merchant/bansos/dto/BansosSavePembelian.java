package id.co.oob.lib.common.merchant.bansos.dto;

import java.util.List;
import java.util.Map;

public class BansosSavePembelian {

	
	private List<BansosTransaksiPembelianDto> bansosTransaksiPembelianDtos;
	private BansosUserPembeliDetail bansosUserPembeliDetail;
//	private Map<String, Object> bansosUserPembeliDetail;
	private String QrCapture;
	private String userSession;
	
	
	public String getQrCapture() {
		return QrCapture;
	}
	public void setQrCapture(String qrCapture) {
		QrCapture = qrCapture;
	}
	public String getUserSession() {
		return userSession;
	}
	public void setUserSession(String userSession) {
		this.userSession = userSession;
	}
	public List<BansosTransaksiPembelianDto> getBansosTransaksiPembelianDtos() {
		return bansosTransaksiPembelianDtos;
	}
	public void setBansosTransaksiPembelianDtos(List<BansosTransaksiPembelianDto> bansosTransaksiPembelianDtos) {
		this.bansosTransaksiPembelianDtos = bansosTransaksiPembelianDtos;
	}
	public BansosUserPembeliDetail getBansosUserPembeliDetail() {
		return bansosUserPembeliDetail;
	}
	public void setBansosUserPembeliDetail(BansosUserPembeliDetail bansosUserPembeliDetail) {
		this.bansosUserPembeliDetail = bansosUserPembeliDetail;
	}
	
	
}

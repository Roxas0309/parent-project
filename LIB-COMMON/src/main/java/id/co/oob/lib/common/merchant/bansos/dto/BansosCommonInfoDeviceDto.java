package id.co.oob.lib.common.merchant.bansos.dto;

public class BansosCommonInfoDeviceDto {
	private String dimensionDevice; //"w x h"
	private String deviceOperatingSystem;
	private String expoPushNotificationToken;
	private String fcmPushNotificationToken;
	private Integer seqDuplicate;
	private String userName;
	
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getDimensionDevice() {
		return dimensionDevice;
	}
	public void setDimensionDevice(String dimensionDevice) {
		this.dimensionDevice = dimensionDevice;
	}
	public String getDeviceOperatingSystem() {
		return deviceOperatingSystem;
	}
	public void setDeviceOperatingSystem(String deviceOperatingSystem) {
		this.deviceOperatingSystem = deviceOperatingSystem;
	}
	public String getExpoPushNotificationToken() {
		return expoPushNotificationToken;
	}
	public void setExpoPushNotificationToken(String expoPushNotificationToken) {
		this.expoPushNotificationToken = expoPushNotificationToken;
	}
	public String getFcmPushNotificationToken() {
		return fcmPushNotificationToken;
	}
	public void setFcmPushNotificationToken(String fcmPushNotificationToken) {
		this.fcmPushNotificationToken = fcmPushNotificationToken;
	}
	public Integer getSeqDuplicate() {
		return seqDuplicate;
	}
	public void setSeqDuplicate(Integer seqDuplicate) {
		this.seqDuplicate = seqDuplicate;
	}
	
}

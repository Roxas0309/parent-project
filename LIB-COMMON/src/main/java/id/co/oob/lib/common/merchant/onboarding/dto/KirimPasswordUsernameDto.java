package id.co.oob.lib.common.merchant.onboarding.dto;

public class KirimPasswordUsernameDto {

	private String email;
	private String phoneNumber;
	private Long idPendaftaran;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Long getIdPendaftaran() {
		return idPendaftaran;
	}
	public void setIdPendaftaran(Long idPendaftaran) {
		this.idPendaftaran = idPendaftaran;
	}
	
	
	
}

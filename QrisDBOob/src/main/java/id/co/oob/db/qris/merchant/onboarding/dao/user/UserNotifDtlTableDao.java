package id.co.oob.db.qris.merchant.onboarding.dao.user;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.user.UserNotifDtlTable;

@Repository
public interface UserNotifDtlTableDao extends JpaRepository<UserNotifDtlTable, Long>{

	@Query("select a from UserNotifDtlTable a where a.uniqueTick = ?1 ")
	public List<UserNotifDtlTable> getNotificationByUniqueTickData(String uniqueTickData);
	
	@Query("select a from UserNotifDtlTable a where a.idOob = ?1 order by createDate desc")
	public List<UserNotifDtlTable> getNotificationById(Long idOob);
	
	@Query("select a from UserNotifDtlTable a where a.idOob = ?1 and a.isRead = 0")
	public List<UserNotifDtlTable> getAllIsRead(Long idOob);
		
	@Query("select a from UserNotifDtlTable a where a.idOob = ?1 "
			+ " and a.createDate >= ?2 "
			+ " and a.createDate <= ?3 "
			+ " and a.notification = ?4 "
			+ " order by createDate desc ")
	public List<UserNotifDtlTable> getNotificationByIdAndFilterBetweenStartDateEndDate
	(Long idOob, Date startDate, Date endDate, String notification);
	
	@Query("select a from UserNotifDtlTable a where "
			+ " a.createDate <= ?1 "
			+ " and a.notification = ?2 "
			+ " order by createDate desc ")
	public List<UserNotifDtlTable> getNotificationAndFilterBetweenStartDateEndDate
	(Date startDate , String notification);
	
}

package id.co.oob.db.qris.merchant.onboarding.service.notif;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.oob.db.qris.merchant.onboarding.dao.user.UserNotifDtlTableDao;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserNotifDtlTable;
import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;

@Service
public class UserNotifDtlTableSvc extends BaseSvc{

	@Autowired
	private UserNotifDtlTableDao userNotifDtlTableDao;
	
	public Boolean isThereIsDataAccessNotif(Long id) {
		 List<UserNotifDtlTable> userNotifDtlTables = userNotifDtlTableDao.getAllIsRead(id);
		 if(userNotifDtlTables==null||userNotifDtlTables.size()==0) {
			 return false;
		 }
		 else {
			 return true;
		 }
	}
	
	public List<UserNotifDtlTable> getllListTableNotifById(Long id, Date startDate, Date endDate, String notif){
		List<UserNotifDtlTable> userNotifDtlTables = userNotifDtlTableDao.getNotificationByIdAndFilterBetweenStartDateEndDate
				(id, startDate, endDate, notif);
		return userNotifDtlTables;
	}
	
	public List<UserNotifDtlTable> getNotificationAndFilterBetweenStartDateEndDate(Date startDate, String notif){
		List<UserNotifDtlTable> userNotifDtlTables = userNotifDtlTableDao.getNotificationAndFilterBetweenStartDateEndDate
				(startDate,  notif);
		return userNotifDtlTables;
	}
	
	public void upgradeToRead(UserNotifDtlTable notifDtlTable) {
		notifDtlTable.setIsRead(true);
		userNotifDtlTableDao.save(notifDtlTable);
	}
}

package id.co.oob.db.qris.merchant.onboarding.dao.alamat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.alamat.ProvinsiTable;
import id.co.oob.db.qris.merchant.onboarding.repository.alamat.TblKodepos;

@Repository
public interface TblKodeposDao extends JpaRepository<TblKodepos, Long>{

	@Query("select distinct a.provinsi from TblKodepos a")
	public List<String> getAllProvinsi();
	
	@Query("select distinct a.kabupaten from TblKodepos a where a.provinsi = ?1")
	public List<String> getAllListKabupaten(String provinsi);
	
	@Query("select distinct a.kecamatan from TblKodepos a where a.kabupaten = ?1")
	public List<String> getAllListKecamatan(String kabupaten);
	
	@Query("select a from TblKodepos a where a.kecamatan = ?1")
	public List<TblKodepos> getAllListKelurahan(String kecamatan);
	
}

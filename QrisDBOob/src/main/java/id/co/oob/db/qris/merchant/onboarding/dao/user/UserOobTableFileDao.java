package id.co.oob.db.qris.merchant.onboarding.dao.user;

import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserOobTableFileDao extends JpaRepository<UserOobTableFile,Long> {
}

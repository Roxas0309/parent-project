package id.co.oob.db.qris.merchant.onboarding.repository.apivendor;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "vendor")
public class VendorModel {
	
	@Id
	@Column(name="id")
//  @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	@Size(max=500)
	@NotBlank(message="note is mandatory")
	private String note;
	
	@Size(max=100)
	@NotBlank(message="wo no is mandatory")
	private String woNo;

	@NotNull(message="tanggal wo is mandatory")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date tglWo;

	@NotNull(message="tanggal case id is mandatory")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date tglCaseId;

	@NotNull(message="tanggal visit is mandatory")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date tglVisit;
	
	@NotNull(message="status aktivitas is mandatory")
	private int statusAktivitas;
	
	@NotBlank(message="case id is mandatory")
	@Size(max=100)
	private String caseId;
	
	@NotBlank(message="mid is mandatory")
	@Size(max=50)
	private String mid;
	
	@Size(max=50)
	@NotBlank(message="tid is mandatory")
	private String tid;
	
	@NotBlank(message="merchant name is mandatory")
	@Size(max=100)
	private String merchantName;
	
	@Size(max=100)
	@NotBlank(message="merchant kota is mandatory")
	private String merchantKota;
	
	@NotBlank(message="type edc is mandatory")
	@Size(max=100)
	private String typeEdc;
	
	@NotBlank(message="sn is mandatory")
	@Size(max=100)
	private String sn;
	
	@NotBlank(message="sim is mandatory")
	@Size(max=100)
	private String sim;
	
	@NotBlank(message="sam is mandatory")
	@Size(max=100)
	private String sam;
	
	@NotBlank(message="provider is mandatory")
	@Size(max=50)
	private String provider;
	
	@NotBlank(message="remarks is mandatory")
	@Size(max=500)
	private String remarks;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getWoNo() {
		return woNo;
	}

	public void setWoNo(String woNo) {
		this.woNo = woNo;
	}

	public Date getTglWo() {
		return tglWo;
	}

	public void setTglWo(Date tglWo) {
		this.tglWo = tglWo;
	}
	
	public Date getTglCaseId() {
		return tglCaseId;
	}

	public void setTglCaseId(Date tglCaseId) {
		this.tglCaseId = tglCaseId;
	}

	public Date getTglVisit() {
		return tglVisit;
	}

	public void setTglVisit(Date tglVisit) {
		this.tglVisit = tglVisit;
	}

	public int getStatusAktivitas() {
		return statusAktivitas;
	}

	public void setStatusAktivitas(int statusAktivitas) {
		this.statusAktivitas = statusAktivitas;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantKota() {
		return merchantKota;
	}

	public void setMerchantKota(String merchantKota) {
		this.merchantKota = merchantKota;
	}

	public String getTypeEdc() {
		return typeEdc;
	}

	public void setTypeEdc(String typeEdc) {
		this.typeEdc = typeEdc;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getSim() {
		return sim;
	}

	public void setSim(String sim) {
		this.sim = sim;
	}

	public String getSam() {
		return sam;
	}

	public void setSam(String sam) {
		this.sam = sam;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}

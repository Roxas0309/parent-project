package id.co.oob.db.qris.merchant.onboarding.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.oob.db.qris.merchant.onboarding.repository.log.WebServiceLog;

public interface WebServiceLogDao extends JpaRepository<WebServiceLog, Long>{

}

package id.co.oob.db.qris.merchant.onboarding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QrisDbOobApplication {

	public static void main(String[] args) {
		SpringApplication.run(QrisDbOobApplication.class, args);
	}

}

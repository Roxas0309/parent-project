package id.co.oob.db.qris.merchant.onboarding.dao.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.user.UserAuthTokenTable;

@Repository
public interface UserAuthTokenTableDao extends JpaRepository<UserAuthTokenTable, String>{
	
	@Query("select a from UserAuthTokenTable a where a.idToken = ?1")
	public UserAuthTokenTable getAuthTokenAble(String idToken); 
	
	@Query("select a,b,c from UserOobTable a, UserOobTableAuthDtl b, UserAuthTokenTable c  "
		 + " where "
		 + " a.id = b.idUserOob "
		 + " and "
		 + " c.idOob = a.id "
		 + " and "	
		 + " c.idToken = ?1 ")
	public List<Object[]> getDetailUserOobTable(String idToken);
	
	@Query("select c from UserOobTable a, UserOobTableAuthDtl b, UserAuthTokenTable c  "
		 + " where "
		 + " a.id = b.idUserOob "
		 + " and "
		 + " c.idOob = a.id "
		 + " and "	
		 + " a.noHandphone = ?1 and b.password = ?2 ")
	public UserAuthTokenTable getDetailUserOobTableExistingToken(String noHp, String password);
	
	@Query("select c from UserOobTable a, UserOobTableAuthDtl b, UserAuthTokenTable c  "
			 + " where "
			 + " a.id = b.idUserOob "
			 + " and "
			 + " c.idOob = a.id "
			 + " and "	
			 + " a.noHandphone = ?1 and b.password = ?2 ")
	public List<UserAuthTokenTable> getDetailUserOobTableExistingTokenNew(String noHp, String password);

	@Modifying
	@Query("delete from UserAuthTokenTable")
	public Integer refreshAllToken();
	
	@Modifying
	@Query("delete from UserAuthTokenTable where idToken = ?1")
	public Integer deleteTokenById(String idToken); 
	
}

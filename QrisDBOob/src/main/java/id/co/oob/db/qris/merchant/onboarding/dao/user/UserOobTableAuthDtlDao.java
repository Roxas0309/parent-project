package id.co.oob.db.qris.merchant.onboarding.dao.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableAuthDtl;

@Repository
public interface UserOobTableAuthDtlDao extends JpaRepository<UserOobTableAuthDtl, Long>{
		
	@Query("select a from UserOobTableAuthDtl a where a.idUserOob = ?1 ")
	public UserOobTableAuthDtl getUserOobTableAuthDtl(Long idUserOob);
	
	@Query("select a from UserOobTableAuthDtl a where a.isFirstLogin = 1")
	public List<UserOobTableAuthDtl> getAllUSerThatIsNotFirstLogin();
	
	@Query("select b from UserOobTable a, UserOobTableAuthDtl b "
			+ " where a.id = b.idUserOob "
			+ " and a.noHandphone = ?1 "
			+ " and b.password = ?2 ")
	public List<UserOobTableAuthDtl> getExistingDataWithPassword(String noPhone, String password);
}

package id.co.oob.db.qris.merchant.onboarding.service.authentication;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import id.co.oob.db.qris.merchant.onboarding.dao.authws.TableAuthWsDao;
import id.co.oob.db.qris.merchant.onboarding.dao.user.UserAuthTokenTableDao;
import id.co.oob.db.qris.merchant.onboarding.dao.user.UserOobTableAuthDtlDao;
import id.co.oob.db.qris.merchant.onboarding.dao.user.UserOobTableDao;
import id.co.oob.db.qris.merchant.onboarding.repository.authws.TableAuthWs;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserAuthTokenTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableAuthDtl;
import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;
import id.co.oob.db.qris.merchant.onboarding.service.session.SessionServiceMaster;
import id.co.oob.db.qris.merchant.onboarding.service.user.UserOobLogicSvc;
import id.co.oob.lib.common.merchant.onboarding.convert.ConvertPhoneNumber;
import id.co.oob.lib.common.merchant.onboarding.dto.authentication.TblAuthWsDto;
import id.co.oob.lib.common.merchant.onboarding.dto.token.UserAuthTokenTableDto;
import id.co.oob.lib.common.merchant.onboarding.encryptor.EncryptDataFiller;
import id.co.oob.lib.common.merchant.onboarding.throwable.AnauthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.ConflictDataException;
import id.co.oob.lib.common.merchant.onboarding.throwable.ForbiddenAuthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;
import id.co.oob.lib.common.merchant.onboarding.validation.DateValidation;

@Service("allTheAuthenticationCircuitSvc")
@Transactional
public class AllTheAuthenticationCircuitSvc extends BaseSvc {

	@Autowired
	private TableAuthWsDao tableAuthWsDao;

	@Autowired
	private UserAuthTokenTableDao userAuthTokenTableDao;

	@Autowired
	private UserOobTableAuthDtlDao userOobTableAuthDtlDao;

	@Autowired
	private UserOobTableDao userOobTableDao;

	@Autowired
	private UserOobLogicSvc userOobLogicSvc;

	@Autowired
	private SessionServiceMaster sessionServiceMaster;

	public List<UserOobTable> getAllUserNeedDeactivate(){
		List<UserOobTable> userOobTables = userOobTableDao.getAllUSerOobOnStatusCekPten();
		List<UserOobTable> oobTablesDeactivate = new ArrayList<UserOobTable>();
		for (UserOobTable userOobTable : userOobTables) {
			if(DateValidation.getDifferentDays(userOobTable.getCreateDate())>7) {
				System.out.println("semua yang mengalami penonaktifan : " + userOobTable.getNamaPemilikUsaha() + ", pendafataran : " + userOobTable.getId());
				oobTablesDeactivate.add(userOobTable);
				userOobTable.setIdStatusProses(-1);
				userOobTableDao.save(userOobTable);
			}
		}
		return oobTablesDeactivate;
	}
	
	public List<UserOobTableAuthDtl> updateAllUserThatAlreadyExpired() {
		List<UserOobTableAuthDtl> userOobTableAuthDtls = userOobTableAuthDtlDao.getAllUSerThatIsNotFirstLogin();
		List<UserOobTableAuthDtl> userOobTableAuthDtlsAlreadyExp = new ArrayList<UserOobTableAuthDtl>();

		for (UserOobTableAuthDtl userOobTableAuthDtl : userOobTableAuthDtls) {

			Date lastPasswordCreated = userOobTableAuthDtl.getCreateDate();

			System.out.println("my last login in my first day is " + lastPasswordCreated + " with different "
					+ DateValidation.getDifferentDays(lastPasswordCreated));

			if (DateValidation.getDifferentDays(lastPasswordCreated) >= 2) {
				if (userOobTableAuthDtl.getWrongPasswordCount() <= 4) {
					UserOobTable userOobTable = userOobTableDao
							.getDetailUserByItsIdOobActive(userOobTableAuthDtl.getIdUserOob());
					if (userOobTable != null) {
						userOobLogicSvc.updateToFirstRegistWhenForgotPassword(userOobTable.getNoHandphone(),
								userOobTableAuthDtl.getPassword());
						userOobTableAuthDtlsAlreadyExp.add(userOobTableAuthDtl);
					}
				} else {
					UserOobTable userOobTable = userOobTableDao.getDetailUserByItsIdOob(userOobTableAuthDtl.getIdUserOob());
					if(userOobTable!=null) {
						userOobTable.setIdStatusProses(-1);
						userOobTableDao.save(userOobTable);
					}
				}
			}

		}

		return userOobTableAuthDtlsAlreadyExp;
	}

	public void deleteTokenByIdToken(String idToken) {
		userAuthTokenTableDao.deleteTokenById(idToken);
	}

	public UserOobTableAuthDtl changePassword(String secretId, Long idUserOob, String newPassword, String noPhone) {
		if (idUserOob == null) {
			throw new InternalServerErrorException("Change Password error in id user oob");
		}
		UserOobTableAuthDtl userAuthTokenTable = userOobTableAuthDtlDao.getOne(idUserOob);

		List<UserOobTableAuthDtl> getAllListAuth = userOobTableAuthDtlDao.getExistingDataWithPassword(noPhone,
				newPassword);

		if (getAllListAuth != null && getAllListAuth.size() > 0) {
			throw new ConflictDataException(
					"Password Telah Digunakan. Demi keamanan mohon gunakan password yang lain.");
		}

		userAuthTokenTable.setPassword(newPassword);
		userAuthTokenTable.setLastUpdateDate(new Date());
		userAuthTokenTable.setLastUpdateBy(secretId);
		userAuthTokenTable.setIsFirstLogin(false);
		userOobTableAuthDtlDao.save(userAuthTokenTable);
		return userAuthTokenTable;
	}

	public UserOobTableAuthDtl changePasswordForgotPassword(Long idUserOob, String newPassword) {
		if (idUserOob == null) {
			throw new InternalServerErrorException("Change Password error in id user oob");
		}
		UserOobTableAuthDtl userAuthTokenTable = userOobTableAuthDtlDao.getOne(idUserOob);
		UserOobTable userOobTable = userOobTableDao.getOne(idUserOob);

		List<UserOobTableAuthDtl> getAllListAuth = userOobTableAuthDtlDao
				.getExistingDataWithPassword(userOobTable.getNoHandphone(), newPassword);

		if (getAllListAuth != null && getAllListAuth.size() > 0) {
			throw new ConflictDataException(
					"Password Telah Digunakan. Demi keamanan mohon gunakan password yang lain.");
		}

		userAuthTokenTable.setLastLogin(new Date());
		userAuthTokenTable.setPassword(newPassword);
		userAuthTokenTable.setLastUpdateDate(new Date());
		userAuthTokenTable.setLastUpdateBy(idUserOob + "");
		userAuthTokenTable.setIsFirstLogin(false);
		userOobTableAuthDtlDao.save(userAuthTokenTable);
		return userAuthTokenTable;
	}

	public TblAuthWsDto getMyAccessWsAuthInfo(String secretId, String secretKey) {
		TableAuthWs tableAuthWs = tableAuthWsDao.getUserAccessorIdInfo(secretKey, secretId);
		return mapperFacade.map(tableAuthWs, TblAuthWsDto.class);
	}

	public Integer refreshAllToken() {
		Integer countRefresh = userAuthTokenTableDao.refreshAllToken();
		return countRefresh;
	}

	public String checkAndRefreshToken(String token, String secretKey, String sessionItem) {
		System.out.println("check refresh token : :4");
		String valueDecrypt = EncryptDataFiller.decrypt(token, secretKey);
		UserAuthTokenTableDto userAuthTokenTableDto = new UserAuthTokenTableDto();
		try {
			userAuthTokenTableDto = mapperJsonToSingleDto(valueDecrypt, UserAuthTokenTableDto.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Pastikan Token Dapat Digenerate dengan key khusus yang diberikan");
		}
		List<Object[]> authws = userAuthTokenTableDao.getDetailUserOobTable(userAuthTokenTableDto.getIdToken());

		if (authws == null || authws.size() == 0) {
			System.out.println(
					"check refresh token : Untuk keamanan, Anda baru saja log out otomatis dari aplikasi MORIS. Login kembali untuk melanjutkan aktivitas.");
			throw new ForbiddenAuthorizedException(
					"Untuk keamanan, Anda baru saja log out otomatis dari aplikasi MORIS. Login kembali untuk melanjutkan aktivitas.");
		}
		System.out.println("trying to save data in checkAndRefreshToken");
		UserAuthTokenTable userAuthTokenTable = new UserAuthTokenTable();
		UserOobTable userOobTable = (UserOobTable) authws.get(0)[0];
		UserOobTableAuthDtl userOobTableAuthDtl = (UserOobTableAuthDtl) authws.get(0)[1];
		userAuthTokenTable = (UserAuthTokenTable) authws.get(0)[2];
		Date loginDate = userAuthTokenTable.getLoginDate();

		System.out.println("trying to delete data in checkAndRefreshToken");
		userAuthTokenTableDao.delete(userAuthTokenTable);

		UserAuthTokenTable userAuthTokenTableNew = new UserAuthTokenTable();
		userAuthTokenTableNew.setIdOob(userOobTable.getId());
		userAuthTokenTableNew.setIdToken(generateToken());
		userAuthTokenTableNew.setLoginDate(loginDate);
		userAuthTokenTableNew.setRefreshDate(new Date());
		userAuthTokenTableNew.setSessionPengenal(sessionItem);

		System.out.println("user auth token table " + new Gson().toJson(userAuthTokenTableNew));

		System.out.println("trying to save data in checkAndRefreshToken");
		userAuthTokenTableDao.save(userAuthTokenTableNew);

		token = EncryptDataFiller.encrypt(new Gson().toJson(userAuthTokenTableNew), secretKey);
		return token;
	}

	public void removeTokenLogin(String token, String secretKey) {
		String valueDecrypt = EncryptDataFiller.decrypt(token, secretKey);
		UserAuthTokenTableDto userAuthTokenTableDto = new UserAuthTokenTableDto();
		try {
			userAuthTokenTableDto = mapperJsonToSingleDto(valueDecrypt, UserAuthTokenTableDto.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Pastikan Token Dapat Digenerate dengan key khusus yang diberikan");
		}
		List<Object[]> authws = userAuthTokenTableDao.getDetailUserOobTable(userAuthTokenTableDto.getIdToken());
		UserAuthTokenTable userAuthTokenTable = new UserAuthTokenTable();
		userAuthTokenTable = (UserAuthTokenTable) authws.get(0)[2];
		userAuthTokenTableDao.delete(userAuthTokenTable);
	}

	public Map<String, Object> getTokenLogin(String user, String password, String userType, String secretKey,
			String tokenLocal, String sessionLocal) {

		if (Strings.isNullOrEmpty(user)) {
			throw new AnauthorizedException("Mohon masukkan no handphone Anda yang terdaftar.");
		}

		if (Strings.isNullOrEmpty(password)) {
			throw new AnauthorizedException("Mohon masukkan password user login Anda.");
		}

		String[] allNumberPhone = ConvertPhoneNumber.getAllNumberInAllFormat(user,
				"Mohon Gunakan Format Nomor Handphone Yang Benar");

		List<UserOobTable> userOobTables = null;

		for (String phone : allNumberPhone) {
			System.out.println("test number phone : " + phone);
			userOobTables = userOobTableDao.getDetailUserOobByPhoneNumber(phone);
			System.out.println(" phone " + phone + " is size : " + userOobTables.size());
			if (userOobTables != null && userOobTables.size() > 0) {
				break;
			}
		}

		System.out.println("final result userOobTables : " + new Gson().toJson(userOobTables));

		if (userOobTables == null || userOobTables.size() == 0) {
			if (Strings.isNullOrEmpty(password)) {
				sessionServiceMaster.countBlockNeeded(sessionLocal, "/loginCtl/auth", 5, 10);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("isInvalid", true);
				map.put("condition", "NO_HP_TERDAFTAR");
				return map;
			} else {
				sessionServiceMaster.countBlockNeeded(sessionLocal, "/loginCtl/auth", 5, 10);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("isInvalid", true);
				map.put("condition", "BELUM_TERDAFTAR");
				return map;
			}
		}

		List<UserOobTableAuthDtl> userOobTableAuthDtls = userOobTableDao
				.getUserOobDetailForLoginWithoutDtlPassword(userOobTables.get(0).getNoHandphone());

		boolean isFirstLogin = false;
		if (userOobTableAuthDtls == null || userOobTableAuthDtls.size() == 0) {
			throw new AnauthorizedException(
					" Anda belum terdaftar. Silakan melakukan pendaftaran akun terlebih dahulu.");
		} else {
			isFirstLogin = userOobTableAuthDtls.get(0).getIsFirstLogin();
		}

		List<Object[]> getUserDetail = userOobTableDao.getUserOobDetailForLogin(userOobTables.get(0).getNoHandphone(),
				password);

		System.out.println("user detail setelah insert password dan no hp : " + new Gson().toJson(getUserDetail));
		if (getUserDetail == null || getUserDetail.size() == 0) {
			if (isFirstLogin) {
				sessionServiceMaster.countBlockNeeded(sessionLocal, "/loginCtl/auth", 5, 10);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("isInvalid", true);
				map.put("condition", "PASSWORD_EMAIL_NOT_SAME");
				return map;
			} else
				System.out.println("testing blocked counter");
			sessionServiceMaster.countBlockNeeded(sessionLocal, "/loginCtl/auth", 5, 10);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("isInvalid", true);
			map.put("condition", "PASSWORD_PHONE_NOT_SAME");
			return map;
		}

		UserOobTable userOobTable = (UserOobTable) getUserDetail.get(0)[0];
		UserOobTableAuthDtl userOobTableAuthDtl = (UserOobTableAuthDtl) getUserDetail.get(0)[1];
		isFirstLogin = userOobTableAuthDtl.getIsFirstLogin();
		Date lastPasswordCreated = userOobTableAuthDtl.getCreateDate();

		System.out.println("my last login in my first day is " + lastPasswordCreated + " with different "
				+ DateValidation.getDifferentDays(lastPasswordCreated));

		if (DateValidation.getDifferentDays(lastPasswordCreated) >= 2 && isFirstLogin) {
			userOobLogicSvc.updateToFirstRegistWhenForgotPassword(userOobTable.getNoHandphone(), password);
			Map<String, Object> map = new HashMap<String, Object>();
			sessionServiceMaster.countBlockNeeded(sessionLocal, "/loginCtl/auth", 5, 10);
			map.put("isInvalid", true);
			map.put("condition", "EXPIRED");
			return map;
		}
		System.err.println("cek first login coy + " + isFirstLogin);
		if (!isFirstLogin) {
			Date lastLogin = userOobTableAuthDtl.getLastLogin();
			System.out.println("my last login is " + lastPasswordCreated + " with different "
					+ DateValidation.getDifferentDays(lastLogin));

			if (DateValidation.getDifferentDays(lastLogin) >= 90) {
				sessionServiceMaster.countBlockNeeded(sessionLocal, "/loginCtl/auth", 5, 10);
				userOobTableAuthDtl.setIsLocked(1);
				userOobTable.setIdStatusProses(-1);
				userOobTableDao.save(userOobTable);
				userOobTableAuthDtlDao.save(userOobTableAuthDtl);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("isInvalid", true);
				map.put("condition", "INACTIVE");
				return map;
			}
		}

		// cek validity token local
		if (!Strings.isNullOrEmpty(tokenLocal)) {

			String valueDecrypt = EncryptDataFiller.decrypt(tokenLocal, secretKey);
			UserAuthTokenTableDto userAuthTokenTableDto = new UserAuthTokenTableDto();
			System.err.println("============= start checking token local is valid or not =========");
			try {
				userAuthTokenTableDto = mapperJsonToSingleDto(valueDecrypt, UserAuthTokenTableDto.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new InternalServerErrorException(
						"Pastikan Token Dapat Digenerate dengan key khusus yang diberikan");
			}

			UserAuthTokenTable userAuthTokenTable = userAuthTokenTableDao
					.getAuthTokenAble(userAuthTokenTableDto.getIdToken());
			System.err.println(
					"check token auth login with userAuthTokenTable merchant " + new Gson().toJson(userAuthTokenTable));
			if (userAuthTokenTable != null) {

				List<Object[]> objs = userOobTableDao.getUserOobDetailForLoginWithIdOob(userOobTable.getNoHandphone(),
						password, userAuthTokenTable.getIdOob());
				System.out.println("objs structur datanya adalah " + new Gson().toJson(objs));
				// ini untuk check session hp nya bukan punya dia
				if (objs != null && objs.size() > 0) {
					String token = checkAndRefreshToken(tokenLocal, secretKey, sessionLocal);
					Map<String, Object> tokenMap = new HashMap<String, Object>();
					tokenMap.put("token", token);
					tokenMap.put("isFirstLogin", userOobTableAuthDtl.getIsFirstLogin());
					return tokenMap;
				} else {
					userAuthTokenTableDao.deleteById(userAuthTokenTableDto.getIdToken());
				}
			}
		}

		List<UserAuthTokenTable> authTokenTables = userAuthTokenTableDao.getDetailUserOobTableExistingTokenNew(user,
				password);

		UserAuthTokenTable authTokenTable = null;

		if (authTokenTables != null && authTokenTables.size() > 1) {
			for (UserAuthTokenTable userAuthTokenTable : authTokenTables) {
				userAuthTokenTableDao.deleteById(userAuthTokenTable.getIdToken());
			}
		} else if (authTokenTables != null && authTokenTables.size() == 1) {
			authTokenTable = authTokenTables.get(0);
		}

		if (authTokenTable != null) {
			System.out.println(
					"get difference hour : " + DateValidation.getDifferentMinuteNew(authTokenTable.getRefreshDate()));
			if (DateValidation.getDifferentMinuteNew(authTokenTable.getRefreshDate()) <= 60
					&& !sessionLocal.equals(authTokenTable.getSessionPengenal())) {
				System.out.println("Pastikan Anda Hanya Login di satu perangkat " + "untuk mengakses akun ini");
				sessionServiceMaster.countBlockNeeded(sessionLocal, "/loginCtl/auth", 5, 10);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("isInvalid", true);
				map.put("condition", "ONLY_ONE_DEVICE");
				return map;
			} else {
				userAuthTokenTableDao.delete(authTokenTable);
			}
		}

		// just edit this if you want to 1 session
		UserAuthTokenTable userAuthTokenTable = new UserAuthTokenTable();
		userAuthTokenTable.setIdOob(userOobTable.getId());
		userAuthTokenTable.setIdToken(generateToken());
		userAuthTokenTable.setLoginDate(new Date());
		userAuthTokenTable.setRefreshDate(new Date());
		userAuthTokenTable.setSessionPengenal(sessionLocal);
		userOobTableAuthDtl.setLastLogin(new Date());
		userOobTableAuthDtlDao.save(userOobTableAuthDtl);
		userAuthTokenTableDao.save(userAuthTokenTable);

		String token = EncryptDataFiller.encrypt(new Gson().toJson(userAuthTokenTable), secretKey);

		sessionServiceMaster.removeAllSessionHdr(sessionLocal, "/loginCtl/auth");
		Map<String, Object> tokenMap = new HashMap<String, Object>();
		tokenMap.put("isInvalid", false);
		tokenMap.put("token", token);
		tokenMap.put("isFirstLogin", userOobTableAuthDtl.getIsFirstLogin());
		return tokenMap;
	}

	public String generateToken() {
		String token = getSaltString(6);
		do {
			token = getSaltString(6);
		} while (userAuthTokenTableDao.existsById(token));
		return token;
	}
}

package id.co.oob.db.qris.merchant.onboarding.service.otp;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import id.co.oob.db.qris.merchant.onboarding.dao.otp.OtpHandlerTableDao;
import id.co.oob.db.qris.merchant.onboarding.repository.otp.OtpHandlerTable;
import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;
import id.co.oob.lib.common.merchant.onboarding.dto.ResponseErrorMessage;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.OtpHandlerTableDto;
import id.co.oob.lib.common.merchant.onboarding.throwable.AnauthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;

@Service
@Transactional
public class OtpHandlerTableSvc extends BaseSvc {

	@Autowired
	private OtpHandlerTableDao otpHandlerTableDao;

	@Autowired
	private OtpBlockHandlerSvc otpBlockHandlerSvc;

	public void isYourOtpAlreadyValidToSave(String userPurposed, String secretKey, String otpOnMenu) {
		OtpHandlerTable otpHandlerTable = otpHandlerTableDao.getInfoOtpOnWhenWillSuccessRegistByUser(userPurposed,
				otpOnMenu, secretKey);
		if (otpHandlerTable == null) {
			throw new AnauthorizedException("Pastikan Bahwa Anda Sudah Melakukan Validasi OTP");
		}

		else {
			otpHandlerTable.setStatusOtp(OtpHandlerTableDao.EXPIRED);
		}

	}

	public OtpHandlerTable getValueOtpHandlerForOtpOnly(String userPurposed, String otpOnMenu, String secretKey) {
		OtpHandlerTable otpHandlerTable = otpHandlerTableDao.getInfoOtpOnValidationByUser(userPurposed, otpOnMenu,
				secretKey);
		if (otpHandlerTable == null) {
			throw new AnauthorizedException("Pastikan Bahwa Anda Telah Meminta OTP");
		} else {
			if (Strings.isNullOrEmpty(otpOnMenu)) {
				throw new NotFoundDataException("Mohon Isi OTP Terlebih Dahulu");
			} else {
				return otpHandlerTable;
			}
		}
	}

	public ResponseErrorMessage isYourOtpValid(String userPurposed, String secretKey, String otpOnMenu, String otpValue,
			String phoneNumber, String statusOtp) {
		OtpHandlerTable otpHandlerTable = getValueOtpHandlerForOtpOnly(userPurposed, otpOnMenu, secretKey);

		if (!statusOtp.equals("2000000")) {
			if (otpHandlerTable.getWrongCounter() >= 3) {
				otpHandlerTable.setStatusOtp(OtpHandlerTableDao.WRONG_AND_INVALID);
				System.out.println("yang akan disimpan : " + new Gson().toJson(otpHandlerTable));
				otpHandlerTableDao.save(otpHandlerTable);
				otpBlockHandlerSvc.saveCausingWrongOtpInput(phoneNumber);
				return new ResponseErrorMessage("No Anda Sudah Diblokir Sampai waktu 2 hari ke depan. Karena, sudah salah input verifikasi OTP sebanyak 3 kali.", false);
			}
			otpHandlerTable.setWrongCounter(otpHandlerTable.getWrongCounter() + 1);
			System.out.println("yang akan disimpan : " + new Gson().toJson(otpHandlerTable));
			otpHandlerTableDao.save(otpHandlerTable);
			return new ResponseErrorMessage(
					"Anda telah memasukkan kode otp yang salah sebanyak " + otpHandlerTable.getWrongCounter() + " kali",
					false);

		} else {
			otpHandlerTableDao.deleteAllClearedDataByUserProposedAndSecretKey(userPurposed, otpOnMenu, secretKey);
			otpHandlerTable.setStatusOtp(OtpHandlerTableDao.CLEARED);
			otpHandlerTableDao.save(otpHandlerTable);
			return new ResponseErrorMessage("", true);
		}

	}

	public OtpHandlerTableDto saveOtpHandler(String userPurposed, String secretKey, String otpOnMenu, String otpValue) {
		List<OtpHandlerTable> otpHandlerTables = otpHandlerTableDao.getInfoOtpsOnValidationByUser(userPurposed,
				otpOnMenu, secretKey);
		if (otpHandlerTables != null) {
			for (OtpHandlerTable otpHandlerTable : otpHandlerTables) {
				otpHandlerTable.setStatusOtp(OtpHandlerTableDao.EXPIRED);
				otpHandlerTableDao.save(otpHandlerTable);
			}
		}
		OtpHandlerTable otpHandlerTable = new OtpHandlerTable();
		otpHandlerTable.setCreateDate(new Date());
		otpHandlerTable.setOtpValue(otpValue);
		otpHandlerTable.setId(generateIdFromExisting());
		otpHandlerTable.setSecretKey(secretKey);
		otpHandlerTable.setStatusOtp(OtpHandlerTableDao.ON_VALIDATION);
		otpHandlerTable.setUserPurposed(userPurposed);
		otpHandlerTable.setWrongCounter(0);
		otpHandlerTable.setOtpOnMenu(otpOnMenu);
		otpHandlerTableDao.save(otpHandlerTable);
		return mapperFacade.map(otpHandlerTable, OtpHandlerTableDto.class);
	}

	public Long generateIdFromExisting() {
		Long id = 0L;
		while (otpHandlerTableDao.existsById(id)) {
			id++;
		}
		return id;
	}

}

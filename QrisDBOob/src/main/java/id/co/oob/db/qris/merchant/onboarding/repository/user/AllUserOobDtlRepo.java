package id.co.oob.db.qris.merchant.onboarding.repository.user;

import id.co.oob.lib.common.merchant.onboarding.dto.user.UserAuthTokenTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobMidDtlTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableAuthDtlDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableSuccessDto;

public class AllUserOobDtlRepo {
	private UserAuthTokenTableDto userAuthTokenTableDto;
	private UserOobMidDtlTableDto userOobMidDtlTableDto;
	private UserOobTableSuccessDto userOobTableSuccessDto;
	private UserOobTableAuthDtlDto userOobTableAuthDtlDto;
	public UserAuthTokenTableDto getUserAuthTokenTableDto() {
		return userAuthTokenTableDto;
	}
	public void setUserAuthTokenTableDto(UserAuthTokenTableDto userAuthTokenTableDto) {
		this.userAuthTokenTableDto = userAuthTokenTableDto;
	}
	public UserOobMidDtlTableDto getUserOobMidDtlTableDto() {
		return userOobMidDtlTableDto;
	}
	public void setUserOobMidDtlTableDto(UserOobMidDtlTableDto userOobMidDtlTableDto) {
		this.userOobMidDtlTableDto = userOobMidDtlTableDto;
	}
	public UserOobTableSuccessDto getUserOobTableSuccessDto() {
		return userOobTableSuccessDto;
	}
	public void setUserOobTableSuccessDto(UserOobTableSuccessDto userOobTableSuccessDto) {
		this.userOobTableSuccessDto = userOobTableSuccessDto;
	}
	public UserOobTableAuthDtlDto getUserOobTableAuthDtlDto() {
		return userOobTableAuthDtlDto;
	}
	public void setUserOobTableAuthDtlDto(UserOobTableAuthDtlDto userOobTableAuthDtlDto) {
		this.userOobTableAuthDtlDto = userOobTableAuthDtlDto;
	}
	
	
	
	
}

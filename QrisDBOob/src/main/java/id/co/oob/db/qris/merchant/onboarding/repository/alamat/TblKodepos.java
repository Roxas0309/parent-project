package id.co.oob.db.qris.merchant.onboarding.repository.alamat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_kodepos")
public class TblKodepos {

	@Id
	@Column(name="id")
	private Long id;
	
	@Column(name="kelurahan")
	private String kelurahan;
	
	@Column(name="kecamatan")
	private String kecamatan;
	
	
	@Column(name="kabupaten")
	private String kabupaten;
	
	
	@Column(name="provinsi")
	private String provinsi;
	
	@Column(name="kodepos")
	private String kodepos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKabupaten() {
		return kabupaten;
	}

	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}

	public String getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}

	public String getKodepos() {
		return kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}
	
	
	
}

package id.co.oob.db.qris.merchant.onboarding.service.otp;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.oob.db.qris.merchant.onboarding.dao.otp.OtpBlockHandlerDao;
import id.co.oob.db.qris.merchant.onboarding.dao.otp.OtpHandlerTableDao;
import id.co.oob.db.qris.merchant.onboarding.repository.otp.OtpBlockHandlerTable;
import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.OtpGenerateTokenResponse;
import id.co.oob.lib.common.merchant.onboarding.dto.success.SuccessValidation;
import id.co.oob.lib.common.merchant.onboarding.encryptor.MaskingNumber;
import id.co.oob.lib.common.merchant.onboarding.validation.DateValidation;

@Service
@Transactional
public class OtpBlockHandlerSvc extends BaseSvc{

	@Autowired
	private OtpBlockHandlerDao otpBlockHandlerDao;
	
	public OtpBlockHandlerTable saveCausingWrongOtpInput(String phoneNumber) {
		OtpBlockHandlerTable blockHandlerTable = new OtpBlockHandlerTable();
		blockHandlerTable.setCreateDate(new Date());
		blockHandlerTable.setPhoneNumber(phoneNumber);
		blockHandlerTable.setReason(OtpBlockHandlerDao.WRONG_OTP_INPUT);
		blockHandlerTable.setId(generateIdFromExisting());
		otpBlockHandlerDao.save(blockHandlerTable);
		return blockHandlerTable;
	}
	
	public void deleteAllOtpRecounter() {
	    otpBlockHandlerDao.deleteAll();
	}
	
	public ResponseEntity<Object> validateBlockOtp(String phoneNumber) {
		List<OtpBlockHandlerTable> blockHandlerTable = otpBlockHandlerDao.getInfoOtpBlockHandlerByPhoneNumber(phoneNumber);
		for (OtpBlockHandlerTable otpBlockHandlerTable : blockHandlerTable) {
			Long dayDiff = DateValidation.getDifferentDays(otpBlockHandlerTable.getCreateDate());
			if(dayDiff<3) {
			OtpGenerateTokenResponse otpGenerateTokenResponse = new OtpGenerateTokenResponse();
			otpGenerateTokenResponse.setExternalId("XXXXXXX");
			otpGenerateTokenResponse.setTimestamp(new Date().toString());
			otpGenerateTokenResponse.setResponseCode("200000000");
			otpGenerateTokenResponse.setResponseMessage
			("No Anda sudah diblokir sampai waktu 1-2 hari ke depan. Karena, sudah salah input verifikasi OTP sebanyak 3 kali.");
			otpGenerateTokenResponse.setPhoneNumber(MaskingNumber.maskingPhoneNumber(phoneNumber));
			otpGenerateTokenResponse.setOtpStatusError(new Boolean(true));
			return new ResponseEntity<Object>(new SuccessValidation
					(otpGenerateTokenResponse.getResponseMessage(),
					otpGenerateTokenResponse), HttpStatus.OK);
			}
		}
	
		return null;
	}
	
	
	public Long generateIdFromExisting() {
		Long id = 0L;
		while(otpBlockHandlerDao.existsById(id)) {
			id++;
		}
		return id;
	}
}

package id.co.oob.db.qris.merchant.onboarding.repository.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Session_Login_Hdr_Dtl")
public class SessionLoginHdrDtl {
	
	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_dtl")
	private Long idDtl;
	
	@Column(name = "session_token")
	private String sessionToken;
	
	@Column(name = "url_blocked")
	private String urlBlocked;
	
	@Column(name="blocked_Date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date blockedDate;
	
	@Column(name="blocked_counter", nullable = true)
	private Integer blockedCounter;
	
	@Column(name="blockedOpen_Date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date blockedOpenDate;

	@Column(name="is_blocked", nullable = true)
	private Boolean isBlocked;
	
	
	
	public Boolean getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public Long getIdDtl() {
		return idDtl;
	}

	public void setIdDtl(Long idDtl) {
		this.idDtl = idDtl;
	}

	public Integer getBlockedCounter() {
		return blockedCounter;
	}

	public void setBlockedCounter(Integer blockedCounter) {
		this.blockedCounter = blockedCounter;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getUrlBlocked() {
		return urlBlocked;
	}

	public void setUrlBlocked(String urlBlocked) {
		this.urlBlocked = urlBlocked;
	}

	public Date getBlockedDate() {
		return blockedDate;
	}

	public void setBlockedDate(Date blockedDate) {
		this.blockedDate = blockedDate;
	}

	public Date getBlockedOpenDate() {
		return blockedOpenDate;
	}

	public void setBlockedOpenDate(Date blockedOpenDate) {
		this.blockedOpenDate = blockedOpenDate;
	}
	
	
}

package id.co.oob.db.qris.merchant.onboarding.dao.alamat;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.alamat.ProvinsiTable;

@Repository
public interface ProvinsiTableDao extends JpaRepository<ProvinsiTable, Long>{

	@Query("select a from ProvinsiTable a order by nama asc")
	public List<ProvinsiTable> findAllProvinsi();
	
	
}

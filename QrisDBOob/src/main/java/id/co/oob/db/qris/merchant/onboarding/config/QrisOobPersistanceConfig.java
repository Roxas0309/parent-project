//package id.co.oob.db.qris.merchant.onboarding.config;
//
//
//import javax.persistence.PersistenceContext;
//import javax.sql.DataSource;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//import org.springframework.orm.jpa.JpaVendorAdapter;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//import java.util.Properties;
//
//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(
//		entityManagerFactoryRef = "entityManager", 
//        transactionManagerRef = "transactionManager", basePackages = {
//		"id.co.oob.db.qris.merchant.onboarding.dao" })
//public class QrisOobPersistanceConfig {
//
//	@Value("${datasource.qris.username}")
//	protected  String DATASOURCE_QRIS_USERNAME;
//	
//	@Value("${datasource.qris.url}")
//	protected  String DATASOURCE_QRIS_URL;
//	
//	@Value("${datasource.qris.password}")
//	protected  String DATASOURCE_QRIS_PASSWORD;
//	
//	@Value("${hibernate.hbm2ddl.auto}")
//	protected  String HIBERNATE_HBM2DDL_AUTO;
//	
//	@Value("${hibernate.dialect}")
//	protected  String HIBERNATE_DIALECT;
//	
//	@Value("${hibernate.show_sql}")
//	protected  String HIBERNATE_SHOW_SQL;
//	
//	@Value("${hibernate.format_sql}")
//	protected  String HIBERNATE_FORMAT_SQL;
//	
//	@Value("${hibernate.default_schema}")
//	protected  String HIBERNATE_DEFAULT_SCHEMA;
//	
//	@Value("${hibernate.connection.driver_class}")
//	protected String HIBERNATE_CONNECTION_DRIVER_CLASS;
//	
//	@Primary
//	@Bean(name = "dataSource")
//	public DataSource dataSource() {
//		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		dataSource.setUrl(DATASOURCE_QRIS_URL);
//		dataSource.setUsername(DATASOURCE_QRIS_USERNAME);
//		dataSource.setPassword(DATASOURCE_QRIS_PASSWORD);
//		dataSource.setDriverClassName(HIBERNATE_CONNECTION_DRIVER_CLASS);
//		return dataSource;
//	}
//	
//	@PersistenceContext(unitName = "primary")
//	@Bean(name = "entityManager")
//	@Primary
//	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//	LocalContainerEntityManagerFactoryBean em 
//        = new LocalContainerEntityManagerFactoryBean();
//      em.setDataSource(dataSource());
//      em.setPackagesToScan(new String[] {"id.co.oob.db.qris.merchant.onboarding.repository"});
//      JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//      em.setJpaVendorAdapter(vendorAdapter);
//      em.setJpaProperties(additionalProperties());
//      return em;
//	}
//
//	Properties additionalProperties() {
//	    Properties properties = new Properties();
//	    properties.setProperty("hibernate.hbm2ddl.auto", HIBERNATE_HBM2DDL_AUTO);
//	    properties.setProperty("hibernate.dialect", HIBERNATE_DIALECT);
//	    properties.setProperty("hibernate.show_sql", HIBERNATE_SHOW_SQL);  
//	    properties.setProperty("hibernate.format_sql", HIBERNATE_FORMAT_SQL); 
//	    properties.setProperty("hibernate.default_schema", HIBERNATE_DEFAULT_SCHEMA);
//	    properties.setProperty("hibernate.jdbc.lob.non_contextual_creation", "true");
//	    return properties;
//	}
//	
//}

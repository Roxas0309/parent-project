package id.co.oob.db.qris.merchant.onboarding.repository.test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


//@Entity
//@Table(name="test_table")
public class TestTable {

	@Id
	@Column(name="column1")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long column1; 
	
	@Column(name="test_column")
	private String testColumn;

	public Long getColumn1() {
		return column1;
	}

	public void setColumn1(Long column1) {
		this.column1 = column1;
	}

	public String getTestColumn() {
		return testColumn;
	}

	public void setTestColumn(String testColumn) {
		this.testColumn = testColumn;
	}
	
	
	
	
	
}

package id.co.oob.db.qris.merchant.onboarding.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.log.SmsLogTable;

@Repository
public interface SmsLogTableDao extends JpaRepository<SmsLogTable, Long>{

}

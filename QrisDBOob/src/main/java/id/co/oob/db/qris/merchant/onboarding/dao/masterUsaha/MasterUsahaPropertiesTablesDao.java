package id.co.oob.db.qris.merchant.onboarding.dao.masterUsaha;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.masterUsaha.MasterUsahaPropertiesTable;

@Repository
public interface MasterUsahaPropertiesTablesDao extends JpaRepository<MasterUsahaPropertiesTable, String>{

	public static final String JENIS_USAHA = "JENIS_USAHA";
	public static final String OMSET_USAHA = "OMSET_USAHA";
	public static final String LOKASI_PERMANEN_USAHA = "LOKASI_PERMANEN_USAHA";
	public static final String LOKASI_NON_PERMANEN_USAHA = "LOKASI_NON_PERMANEN_USAHA";
	public static final String AKUN_OOB_QR = "AKUN_OOB_QR";
	
	@Query("select a from MasterUsahaPropertiesTable a where a.tipe = ?1")
	List<MasterUsahaPropertiesTable> getAllMasterUsahaPropertiesBaseTipeData(String tipe);
	
	@Query("select a from MasterUsahaPropertiesTable a where a.idName = ?1 and a.tipe = '"+JENIS_USAHA+"'")
	MasterUsahaPropertiesTable checkIdJenisUsaha(String idJenisUsaha);
	
	@Query("select a from MasterUsahaPropertiesTable a where a.idName = ?1 and a.tipe = '"+OMSET_USAHA+"'")
	MasterUsahaPropertiesTable checkIdOmsetUsaha(String idOmsetUsaha);
	
	@Query("select a from MasterUsahaPropertiesTable a where a.idName = ?1 and a.tipe = '"+LOKASI_PERMANEN_USAHA+"'")
	MasterUsahaPropertiesTable checkIdLokasiPermanenUsaha(String idLokasiPermanenUsaha);
	
	@Query("select a from MasterUsahaPropertiesTable a where a.idName = ?1 and a.tipe = '"+LOKASI_NON_PERMANEN_USAHA+"'")
	MasterUsahaPropertiesTable checkIdLokasiNonPermanenUsaha(String idLokasiNonPermanenUsaha);
	
	@Query("select a from MasterUsahaPropertiesTable a where a.idName = ?1 and a.tipe = '"+AKUN_OOB_QR+"'")
	MasterUsahaPropertiesTable checkIdAkunOobQr(String idLokasiNonPermanenUsaha);
	
}

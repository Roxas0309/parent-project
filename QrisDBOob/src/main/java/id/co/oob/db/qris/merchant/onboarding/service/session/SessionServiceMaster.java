package id.co.oob.db.qris.merchant.onboarding.service.session;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import id.co.oob.db.qris.merchant.onboarding.dao.session.SessionLoginHdrDao;
import id.co.oob.db.qris.merchant.onboarding.dao.session.SessionLoginHdrDtlDao;
import id.co.oob.db.qris.merchant.onboarding.repository.user.SessionLoginHdr;
import id.co.oob.db.qris.merchant.onboarding.repository.user.SessionLoginHdrDtl;
import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;
import id.co.oob.lib.common.merchant.onboarding.validation.DateValidation;

@Service
@Transactional
public class SessionServiceMaster extends BaseSvc{

	@Autowired
	private SessionLoginHdrDao sessionLoginHdrDao;
	
	@Autowired
	private SessionLoginHdrDtlDao sessionLoginHdrDtlDao;
	
	
	
	public void saveSessionMe(String sessionId) {
		SessionLoginHdr sessionLoginHdr = new SessionLoginHdr();
		sessionLoginHdr.setAccessFrom("Testing");
		sessionLoginHdr.setCreatedDate(new Date());
		sessionLoginHdr.setRefreshDate(new Date());
		sessionLoginHdr.setSessionToken(sessionId);
		sessionLoginHdrDao.save(sessionLoginHdr);
	}
	
	public void removeAllSessionHdr(String session, String url) {
		List<SessionLoginHdrDtl> sessionHdrs = sessionLoginHdrDtlDao.SessionLoginHdrDtl(session, url);
	 for (SessionLoginHdrDtl dtl : sessionHdrs) {
		sessionLoginHdrDtlDao.delete(dtl);
	}   
	}
	
	public SessionLoginHdrDtl getSessionLoginDtl(String session, String url) {
		List<SessionLoginHdrDtl> sessionHdrs = sessionLoginHdrDtlDao.SessionLoginHdrDtl(session, url);
		System.out.println("session hdrs di getSessionLoginDtl " + new Gson().toJson(sessionHdrs));
		if(sessionHdrs==null||sessionHdrs.size()==0) {
			return null;
		}
		else {
			if(sessionHdrs.size()==1) {
				SessionLoginHdrDtl sessionLoginHdrDtl = sessionHdrs.get(0);
				if(sessionLoginHdrDtl.getBlockedOpenDate()!=null) {
				if(new Date().after(sessionLoginHdrDtl.getBlockedOpenDate())) {
					sessionLoginHdrDtlDao.delete(sessionLoginHdrDtl);
					return null;
				}
				else {
				return sessionLoginHdrDtl;
				}
				}
				else{
					return null;
				}
			}
			else {
				for (SessionLoginHdrDtl sessionLoginHdrDtl : sessionHdrs) {
					sessionLoginHdrDtlDao.delete(sessionLoginHdrDtl);
				}
				return null;
			}
		}
	}
	
	public Integer countBlockNeeded(String session, String url, Integer counterToBlocked,
			Integer timeOpenBlockedInMinute) {
		List<SessionLoginHdrDtl> sessionHdrs = sessionLoginHdrDtlDao.SessionLoginHdrDtl(session, url);
		Integer counter = 1;
	System.err.println("countBlockNeeded untuk sessionHdrs. " + new Gson().toJson(sessionHdrs));
		if(sessionHdrs==null||sessionHdrs.size()==0) {
			SessionLoginHdrDtl sessionLoginHdrDtl = new SessionLoginHdrDtl();
			sessionLoginHdrDtl.setBlockedCounter(counter);
			sessionLoginHdrDtl.setBlockedDate(null);
			sessionLoginHdrDtl.setBlockedOpenDate(null);
			sessionLoginHdrDtl.setIdDtl(generateIdFromExisting());
			sessionLoginHdrDtl.setIsBlocked(false);
			sessionLoginHdrDtl.setSessionToken(session);
			sessionLoginHdrDtl.setUrlBlocked(url);
			sessionLoginHdrDtlDao.save(sessionLoginHdrDtl);
			System.out.println("siap untuk save");
		}
		else {
			if(sessionHdrs.size()==1) {
				SessionLoginHdrDtl sessionLoginHdrDtl = sessionHdrs.get(0);
				counter = sessionLoginHdrDtl.getBlockedCounter()+1;
				
				if(counter>=counterToBlocked) {
					sessionLoginHdrDtl.setIsBlocked(true);
					sessionLoginHdrDtl.setBlockedDate(new Date());
					sessionLoginHdrDtl.setBlockedOpenDate(DateValidation.addDate(60000L*timeOpenBlockedInMinute));
				}else {
					sessionLoginHdrDtl.setBlockedCounter(counter);
				}
				sessionLoginHdrDtlDao.save(sessionLoginHdrDtl);
			}
			else {
				for (SessionLoginHdrDtl sessionLoginHdrDtl : sessionHdrs) {
					sessionLoginHdrDtlDao.delete(sessionLoginHdrDtl);
				}
				SessionLoginHdrDtl sessionLoginHdrDtl = new SessionLoginHdrDtl();
				sessionLoginHdrDtl.setBlockedCounter(counter);
				sessionLoginHdrDtl.setBlockedDate(null);
				sessionLoginHdrDtl.setBlockedOpenDate(null);
				sessionLoginHdrDtl.setIdDtl(generateIdFromExisting());
				sessionLoginHdrDtl.setIsBlocked(false);
				sessionLoginHdrDtl.setSessionToken(session);
				sessionLoginHdrDtl.setUrlBlocked(url);
				sessionLoginHdrDtlDao.save(sessionLoginHdrDtl);
			}
		}
		
		return counter;
	}
	
	public Long generateIdFromExisting() {
		Long id = 1L;
		while(sessionLoginHdrDtlDao.existsById(id)) {
			id++;
		}
		return id;
	}
	
	public void refreshSessionMe(String sessionMe) {
		SessionLoginHdr sessionLoginHdr = sessionLoginHdrDao.getSessionLoginHdr(sessionMe);
		sessionLoginHdr.setRefreshDate(new Date());
		
		sessionLoginHdrDao.save(sessionLoginHdr);
	}
}

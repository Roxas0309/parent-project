package id.co.oob.db.qris.merchant.onboarding.dao.authws;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.authws.TableAuthWs;

@Repository
public interface TableAuthWsDao extends JpaRepository<TableAuthWs, Long>{
	
	@Query("select a from TableAuthWs a where a.secretKey = ?1 and a.secretId = ?2")
	public TableAuthWs getUserAccessorIdInfo(String secretKey, String secretId);

}

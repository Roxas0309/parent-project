package id.co.oob.db.qris.merchant.onboarding.dao.otp;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.otp.OtpBlockHandlerTable;
import id.co.oob.db.qris.merchant.onboarding.repository.otp.OtpHandlerTable;

@Repository
public interface OtpBlockHandlerDao extends JpaRepository<OtpBlockHandlerTable, Long>{
	public final static String WRONG_OTP_INPUT = "WRONG_OTP_INPUT";
	
	@Query("select a from OtpBlockHandlerTable a where a.phoneNumber = ?1")
	public List<OtpBlockHandlerTable> getInfoOtpBlockHandlerByPhoneNumber(String phoneNumber);
	
}

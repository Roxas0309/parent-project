package id.co.oob.db.qris.merchant.onboarding.service.vendor;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import id.co.oob.db.qris.merchant.onboarding.dao.vendor.VendorRepository;
import id.co.oob.db.qris.merchant.onboarding.repository.apivendor.VendorModel;
import org.springframework.web.bind.annotation.RequestHeader;

@Service
public class VendorService extends BaseSvc {
	
	@Autowired
	private VendorRepository vendorRepository;

	public <T>ResponseEntity<T> save(
			@RequestBody @Valid VendorModel vendorModel){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			vendorRepository.save(vendorModel);
			result.put("status", HttpStatus.OK.value());
			result.put("message", "ok");
			return new ResponseEntity<T>((T) result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("status", HttpStatus.BAD_REQUEST.value());
			result.put("message", e.getMessage());
			return new ResponseEntity<T>((T) result, HttpStatus.BAD_REQUEST);
		}
	}
}

package id.co.oob.db.qris.merchant.onboarding.repository.alamat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="kabupaten_table")
public class KabupatenTable {

	
	@Id
	@Column(name="id")
	private Long id; 
	
	@Column(name="id_provinsi")
	private Long id_provinsi; 
	
	@Column(name="nama")
	private String nama;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_provinsi() {
		return id_provinsi;
	}

	public void setId_provinsi(Long id_provinsi) {
		this.id_provinsi = id_provinsi;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
	
}

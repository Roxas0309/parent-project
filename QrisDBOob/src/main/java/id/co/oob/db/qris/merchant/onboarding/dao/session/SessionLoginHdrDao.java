package id.co.oob.db.qris.merchant.onboarding.dao.session;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.user.SessionLoginHdr;

@Repository
public interface SessionLoginHdrDao extends JpaRepository<SessionLoginHdr, String> {
	
	@Query("select a from SessionLoginHdr a where a.sessionToken = ?1")
	public SessionLoginHdr getSessionLoginHdr(String sessionId);
	
	
	
}

package id.co.oob.db.qris.merchant.onboarding.dao.alamat;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.alamat.KecamatanTable;

@Repository
public interface KecamatanTableDao extends JpaRepository<KecamatanTable, Long>{
	
	@Query("select a from KecamatanTable a order by a.nama asc")
	public List<KecamatanTable> findAllKecamatanTable();
	
	@Query("select a from KecamatanTable a where a.id not in "
			+ " (select kt.id_kecamatan from KelurahanTable kt ) order by a.nama asc")
	public List<KecamatanTable> findAllKecamatanTableFilter();
	
	@Query("select a from KecamatanTable a where a.id_kota = ?1 order by a.nama asc")
	public List<KecamatanTable> findAllKecamatanTableByIdKota(Long idKota);
}

package id.co.oob.db.qris.merchant.onboarding.repository.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Session_Login_Hdr")
public class SessionLoginHdr {

	
	@Id
	@Column(name = "session_token")
	private String sessionToken;
	
	@Column(name = "access_from")
	private String accessFrom;
	
	@Column(name="created_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name="refresh_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date refreshDate;

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getAccessFrom() {
		return accessFrom;
	}

	public void setAccessFrom(String accessFrom) {
		this.accessFrom = accessFrom;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getRefreshDate() {
		return refreshDate;
	}

	public void setRefreshDate(Date refreshDate) {
		this.refreshDate = refreshDate;
	}
	
	
}

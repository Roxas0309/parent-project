package id.co.oob.db.qris.merchant.onboarding.repository.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "User_Oob_Notification_Table")
public class UserOobNotificationTable {

	@Id
	@Column(name="token_page")
	private String tokenPage;
	
	@Column(name="id_Oob")
    //@GeneratedValue(strategy = GenerationType.AUTO)
    private Long idOob;
	
	@Column(name="create_date", nullable = false,updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	@Column(name="update_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	public String getTokenPage() {
		return tokenPage;
	}

	public void setTokenPage(String tokenPage) {
		this.tokenPage = tokenPage;
	}

	public Long getIdOob() {
		return idOob;
	}

	public void setIdOob(Long idOob) {
		this.idOob = idOob;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
}

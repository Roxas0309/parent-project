package id.co.oob.db.qris.merchant.onboarding.repository.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "user_notifdtl_table")
public class UserNotifDtlTable {

	@Id
	@Column(name="id_Notification")
	private Long idNotification;
	
	@Column(name="id_Oob")
    private Long idOob;
	
	@Column(name="header")
	private String header;
	
	@Column(name="notification")
	private String notification;  //untuk saat ini Transaksi dan Informasi
	
	@Column(name="unique_tick")
	private String uniqueTick;
	
	@Column(name="body")
	private String body;
	
	@Column(name="is_Read")
	private Boolean isRead;
	
	@Column(name="is_Sent")
	private Boolean isSent;
	
	@Column(name="create_date", nullable = false,updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public String getUniqueTick() {
		return uniqueTick;
	}

	public void setUniqueTick(String uniqueTick) {
		this.uniqueTick = uniqueTick;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Long getIdNotification() {
		return idNotification;
	}

	public void setIdNotification(Long idNotification) {
		this.idNotification = idNotification;
	}

	public Boolean getIsSent() {
		return isSent;
	}

	public void setIsSent(Boolean isSent) {
		this.isSent = isSent;
	}

	public Long getIdOob() {
		return idOob;
	}

	public void setIdOob(Long idOob) {
		this.idOob = idOob;
	}


	public Boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	
}

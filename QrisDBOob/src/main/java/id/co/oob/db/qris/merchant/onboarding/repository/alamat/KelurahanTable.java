package id.co.oob.db.qris.merchant.onboarding.repository.alamat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="kelurahan_table")
public class KelurahanTable {
	@Id
	@Column(name="id")
	private Long id; 
	
	@Column(name="id_kecamatan")
	private Long id_kecamatan; 
	
	@Column(name="nama" )
	private String nama;
	
	@Column(name="kode_Pos" )
	private String kodePos;

	
	
	public String getKodePos() {
		return kodePos;
	}

	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_kecamatan() {
		return id_kecamatan;
	}

	public void setId_kecamatan(Long id_kecamatan) {
		this.id_kecamatan = id_kecamatan;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
}

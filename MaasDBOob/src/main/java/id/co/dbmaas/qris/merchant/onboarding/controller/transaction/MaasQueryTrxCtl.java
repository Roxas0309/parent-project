package id.co.dbmaas.qris.merchant.onboarding.controller.transaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.dbmaas.qris.merchant.onboarding.service.MaasCustomNativeSvc;

@RestController
@RequestMapping("/MaasQueryTrxCtl")
public class MaasQueryTrxCtl {

	@Autowired
	private MaasCustomNativeSvc maasCustomNativeSvc;
	
	@PostMapping("/queryMe")
	public Object queryMeAll(@RequestBody String query){
		return maasCustomNativeSvc.resultQuery(query);
	}

	
}

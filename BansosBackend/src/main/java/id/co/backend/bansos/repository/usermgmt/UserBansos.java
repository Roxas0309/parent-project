package id.co.backend.bansos.repository.usermgmt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "User_Bansos")
public class UserBansos {
	
	@Id
	@Column(name = "user_name")
	private String userName;
	
	private String password;
	
	@Column(name = "first_time_login", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date firstTimeLogin;
	
	private String mid;
	
	@Column(name = "mer_Nm", nullable = true)
	private String merNm;
	
	@Column(name = "no_hp", nullable = true)
	private String noHp;
	
	private String email;
	
	private String address;

	private String firstLogin;
	
	

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getMerNm() {
		return merNm;
	}

	public void setMerNm(String merNm) {
		this.merNm = merNm;
	}

	public String getNoHp() {
		return noHp;
	}

	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(String firstLogin) {
		this.firstLogin = firstLogin;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getFirstTimeLogin() {
		return firstTimeLogin;
	}

	public void setFirstTimeLogin(Date firstTimeLogin) {
		this.firstTimeLogin = firstTimeLogin;
	}
	
	
	
}

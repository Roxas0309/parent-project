package id.co.backend.bansos.repository.transaksi;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Lob;
import javax.persistence.Table;

import id.co.backend.bansos.repository.transaksi.id.PembeliBarangBansosId;

@Entity
@Table(name = "Pembeli_Barang_Bansos")
@IdClass(PembeliBarangBansosId.class)
public class PembeliBarangBansos {
	
	@Id
	private String pembeliNo;
	
	@Id
	private String transaksiOwner;
	
	private String noEktp;
	
	private String namaPembeli;
	
	private String fotoEktpBase64;
	
	private String fotoWajahdenganEktp;
	
	private String fotoSuratKeterangan;
	
	private String fotoPembeliDanBarang;

	private BigDecimal totalBelanjaan;
	
	private BigDecimal totalPembayaran;
	
	@Column(name = "created_date", nullable = false) //format yyyyMMdd
	private String createdDate;
	
	@Column(name = "created_time", nullable = false) //format HHmmSS
	private String createdTime;
	
	@Lob
	private String qrCode;
	
	

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public BigDecimal getTotalBelanjaan() {
		return totalBelanjaan;
	}

	public void setTotalBelanjaan(BigDecimal totalBelanjaan) {
		this.totalBelanjaan = totalBelanjaan;
	}

	public BigDecimal getTotalPembayaran() {
		return totalPembayaran;
	}

	public void setTotalPembayaran(BigDecimal totalPembayaran) {
		this.totalPembayaran = totalPembayaran;
	}

	public String getPembeliNo() {
		return pembeliNo;
	}

	public void setPembeliNo(String pembeliNo) {
		this.pembeliNo = pembeliNo;
	}

	public String getTransaksiOwner() {
		return transaksiOwner;
	}

	public void setTransaksiOwner(String transaksiOwner) {
		this.transaksiOwner = transaksiOwner;
	}

	public String getNoEktp() {
		return noEktp;
	}

	public void setNoEktp(String noEktp) {
		this.noEktp = noEktp;
	}

	public String getNamaPembeli() {
		return namaPembeli;
	}

	public void setNamaPembeli(String namaPembeli) {
		this.namaPembeli = namaPembeli;
	}

	public String getFotoEktpBase64() {
		return fotoEktpBase64;
	}

	public void setFotoEktpBase64(String fotoEktpBase64) {
		this.fotoEktpBase64 = fotoEktpBase64;
	}

	public String getFotoWajahdenganEktp() {
		return fotoWajahdenganEktp;
	}

	public void setFotoWajahdenganEktp(String fotoWajahdenganEktp) {
		this.fotoWajahdenganEktp = fotoWajahdenganEktp;
	}

	public String getFotoSuratKeterangan() {
		return fotoSuratKeterangan;
	}

	public void setFotoSuratKeterangan(String fotoSuratKeterangan) {
		this.fotoSuratKeterangan = fotoSuratKeterangan;
	}

	public String getFotoPembeliDanBarang() {
		return fotoPembeliDanBarang;
	}

	public void setFotoPembeliDanBarang(String fotoPembeliDanBarang) {
		this.fotoPembeliDanBarang = fotoPembeliDanBarang;
	}
	
	
}

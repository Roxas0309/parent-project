package id.co.backend.bansos.controller.device;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import id.co.backend.bansos.controller.BaseCtl;
import id.co.backend.bansos.repository.device.DeviceInfoVersion;
import id.co.oob.lib.common.merchant.bansos.dto.RemotelyPublicUpdateDto;

@RestController
@RequestMapping("/DeviceInfoCtl")
public class DeviceInfoCtl extends BaseCtl{

	@Autowired
	private id.co.backend.bansos.dao.device.DeviceInfoVersionDao deviceInfoVersionDao;
	
	
	
	@PostMapping("/pushRemotelyUpdate")
	public ResponseEntity<Object> getPushRemotelyUpdate(@RequestBody RemotelyPublicUpdateDto remotelyPublicUpdateDto){
		System.out.println("remote update " + new Gson().toJson(remotelyPublicUpdateDto));
		List<DeviceInfoVersion> getAllListFromUpdatedData = deviceInfoVersionDao.getVeryUpdatedVersion();

		String linkUpdate = "";
		Boolean isUpdate = false;
		String versionNow = remotelyPublicUpdateDto.getVersionBefore();
		for (DeviceInfoVersion deviceInfoVersion : getAllListFromUpdatedData) {
			if(deviceInfoVersion.getVersionOS().equalsIgnoreCase(remotelyPublicUpdateDto.getOperatingSystem())
					&& !deviceInfoVersion.getVersionAplication().equalsIgnoreCase(remotelyPublicUpdateDto.getVersionBefore())) {
				versionNow = deviceInfoVersion.getVersionAplication();
				linkUpdate = deviceInfoVersion.getDownloadLink();
				isUpdate = true;
				break;
			}
		}
		
		remotelyPublicUpdateDto.setIsNeedUpdate(isUpdate);
		remotelyPublicUpdateDto.setLinkUpdate(linkUpdate);
		remotelyPublicUpdateDto.setVersionAfter(versionNow);
		
		return new ResponseEntity<Object>(remotelyPublicUpdateDto, HttpStatus.OK);
	}
		
}

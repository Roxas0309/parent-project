package id.co.backend.bansos.dao.barang;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.backend.bansos.repository.barang.BarangJualanBansos;
import id.co.backend.bansos.repository.barang.id.BarangJualanBansosId;

@Repository
public interface BarangJualanBansosDao extends JpaRepository<BarangJualanBansos, BarangJualanBansosId>{

	@Query("select a from BarangJualanBansos a where a.userName = ?1")
	public List<BarangJualanBansos> findByUserName(String userName);
	
	@Query("select a.addGrouping from BarangJualanBansos a where a.userName = ?1   group by a.addGrouping")
	public List<Long> CountCapabiltyToCheckCapability(String userName);
	
	@Query("select a from BarangJualanBansos a where a.userName = ?1 and a.addGrouping = ?2")
	public List<BarangJualanBansos> findAddGroupingList(String userName, Long addGrouping);
	
	@Query("select a from BarangJualanBansos a where a.userName = ?1 and a.idSeq = ?2")
	public BarangJualanBansos findByBarangByUserNameAndId(String userName, Long idSeq);
	
	@Query("delete from BarangJualanBansos a where a.userName = ?1 and a.idSeq = ?2")
	@Modifying
	public void hapusBarangByUserNameAndId(String userName, Long idSeq);
	
	@Query("select a from BarangJualanBansos a where a.userName = ?1 and a.idSeq = ?2")
	public BarangJualanBansos findByUserNameAndSeq(String userName, Long idSeq);
	
	@Query("select a from BarangJualanBansos a where a.userName = ?1 and ("
			+ "	upper(a.jenisBarang) like upper(?2) "
			+ " or "
			+ " upper(a.merk) like upper(?2) "
			+ " ) "
			+ " order By a.createdDate desc, a.createdTime desc")
	public List<BarangJualanBansos> findByUserNameOrderByDateAndTime(String userName, String search);
	
	
}

package id.co.backend.bansos.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/testing")
public class TestingCtl extends BaseCtl{

	@GetMapping("/hello-bansos")
	public String HelloBansos() {
		return "Hello World, Ready To Code!";
	}
	
}

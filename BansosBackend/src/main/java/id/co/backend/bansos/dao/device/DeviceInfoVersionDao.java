package id.co.backend.bansos.dao.device;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.backend.bansos.repository.device.DeviceInfoVersion;

@Repository
public interface DeviceInfoVersionDao extends JpaRepository<DeviceInfoVersion, Long>{

	@Query("select a from DeviceInfoVersion a order by a.versionDateRelease desc")
	public List<DeviceInfoVersion> getVeryUpdatedVersion();
	
	@Query("select a.downloadLink from DeviceInfoVersion a where upper(a.versionOS) = upper(?1)")
	public String urlRedirect(String osVersion);
	
}

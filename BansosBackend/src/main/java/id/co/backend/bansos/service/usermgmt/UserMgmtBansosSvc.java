package id.co.backend.bansos.service.usermgmt;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import id.co.oob.lib.common.merchant.bansos.dto.UserBansosDecryption;
import id.co.oob.lib.common.merchant.onboarding.encryptor.Base64EncodeCustom;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import id.co.backend.bansos.dao.common.BansosCommonInfoDeviceDao;
import id.co.backend.bansos.dao.usermgmt.UserBansosDao;
import id.co.backend.bansos.dao.usermgmt.UserBansosLoginHistoryDao;
import id.co.backend.bansos.dao.usermgmt.UserBansosLoginSessionDao;
import id.co.backend.bansos.repository.common.BansosCommonInfoDevice;
import id.co.backend.bansos.repository.usermgmt.UserBansos;
import id.co.backend.bansos.repository.usermgmt.UserBansosLoginHistory;
import id.co.backend.bansos.repository.usermgmt.UserBansosLoginSession;
import id.co.backend.bansos.service.BaseSvc;

@Service
@Transactional
public class UserMgmtBansosSvc extends BaseSvc {

	@Autowired
	private BansosCommonInfoDeviceDao bansosCommonInfoDeviceDao;

	@Autowired
	private UserBansosDao userBansosDao;

	@Autowired
	private UserBansosLoginHistoryDao userBansosLoginHistoryDao;

	@Autowired
	private UserBansosLoginSessionDao userBansosLoginSessionDao;
	
	public UserBansos getUserBansos(String userName) {
		return userBansosDao.getUserBansosByUserName(userName);
	}
	
	public void logoutMe (String userName) {
		userBansosLoginSessionDao.logOutMe(userName);
	}

	public ResponseEntity<Object> getSessionLoginOjectEntity(String sessionLogin) {
		UserBansosLoginSession userBansosLoginSession = userBansosLoginSessionDao
				.getLoginSessionBySessionLogin(sessionLogin);
		System.out.println("userLoginBansos untuk sessionnya " + sessionLogin + " adalah : "
				+ new Gson().toJson(userBansosLoginSession));
		if (userBansosLoginSession == null) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(true, HttpStatus.OK);
		}

	}

	public ResponseEntity<Object> changePass (String userName, String newPass, String session) {

		UserBansos userBansos = userBansosDao.getUserBansosByUserName(userName);
		Map<String, Object> mapResult = new HashedMap<>();

		if (userBansos.getPassword().equals(newPass)) {
			//userBansosDao.changePass(newPass, userName);
			mapResult.put("error", "Kata Sandi Baru tidak boleh sama dengan Kata Sandi yang terdaftar");
			mapResult.put("firstLogin", userBansos.getFirstLogin());
			mapResult.put(CODE_HTTP, HttpStatus.NOT_FOUND.value());
			mapResult.put(CODE_ENGINE, ERROR_NOT_FOUND_USER);
			return new ResponseEntity<Object>(mapResult, HttpStatus.NOT_FOUND);

		} else {
			String newPassInEnc = Base64EncodeCustom.encryptPassword(newPass);
			userBansosDao.changePass(newPassInEnc, userName);
			mapResult.put("message", "Change password success");
			mapResult.put("firstLogin", userBansos.getFirstLogin());
			mapResult.put(CODE_HTTP, HttpStatus.OK.value());
			mapResult.put(CODE_ENGINE, SUCCESS_TRX);
			return new ResponseEntity<Object>(mapResult, HttpStatus.OK);
		}

	}

	public ResponseEntity<Object> saveLoginUserMgmt(String userName, String password, String expoToken) {
		UserBansosLoginSession userBansosLoginSession = userBansosLoginSessionDao.getLoginSessionByUserName(userName);
		Map<String, Object> mapResult = new HashedMap<>();
		System.out.println("usb : " + new Gson().toJson(userBansosLoginSession));
		if (userBansosLoginSession != null) {
			mapResult.put("error", "Sedang login di perangkat lain.");
			mapResult.put(CODE_HTTP, HttpStatus.CONFLICT.value());
			mapResult.put(CODE_ENGINE, ERROR_CONFLICT_USER_LOGIN);
			return new ResponseEntity<Object>(mapResult, HttpStatus.CONFLICT);
		}

		
		UserBansos userBansos = userBansosDao.getUserBansosByUserName(userName);
		
		
		if (userBansos != null) {
			String passwordInDenc = password;
			if(userBansos.getFirstLogin().equalsIgnoreCase("N")) {
				passwordInDenc = Base64EncodeCustom.decryptPassword(userBansos.getPassword());
			}
			System.out.println("passwordInDenc : " + passwordInDenc);
			if(!passwordInDenc.equals(password)) {
				mapResult.put("error", "Username atau password berbeda dengan yang terdaftar");
				mapResult.put(CODE_HTTP, HttpStatus.NOT_FOUND.value());
				mapResult.put(CODE_ENGINE, ERROR_NOT_FOUND_USER);
				return new ResponseEntity<Object>(mapResult, HttpStatus.NOT_FOUND);
			}
			
			userBansos.setFirstTimeLogin(new Date());
			userBansosDao.save(userBansos);
		}
		else {
			mapResult.put("error", "Username atau password berbeda dengan yang terdaftar");
			mapResult.put(CODE_HTTP, HttpStatus.NOT_FOUND.value());
			mapResult.put(CODE_ENGINE, ERROR_NOT_FOUND_USER);
			return new ResponseEntity<Object>(mapResult, HttpStatus.NOT_FOUND);
		}

		List<BansosCommonInfoDevice> bansosCommonInfoDevices = bansosCommonInfoDeviceDao
				.getExpoPushNotificationTokenThatExist(expoToken);
		if (bansosCommonInfoDevices != null) {
			for (BansosCommonInfoDevice bansosCommonInfoDevice : bansosCommonInfoDevices) {
				bansosCommonInfoDevice.setUserName(userName);
				bansosCommonInfoDeviceDao.save(bansosCommonInfoDevice);
			}
		}

		Long idSeq = getIdSeqByUserName(userName);
		UserBansosLoginHistory userBansosLoginHistory = new UserBansosLoginHistory();
		userBansosLoginHistory.setIdSeq(idSeq);
		userBansosLoginHistory.setLoginTime(new Date());
		userBansosLoginHistory.setLogout_time(null);
		userBansosLoginHistory.setUserName(userName);
		userBansosLoginHistoryDao.save(userBansosLoginHistory);

		String encryptData = encryptUserName(idSeq, userName, password, BANSOS_LOGIN);
		mapResult.put(CODE_HTTP, HttpStatus.OK.value());
		mapResult.put(CODE_ENGINE, SUCCESS_TRX);
		mapResult.put("encryption", encryptData);
		mapResult.put("firstLogin", userBansos.getFirstLogin());

		UserBansosLoginSession session = new UserBansosLoginSession();
		session.setSessionLogin(encryptData);
		session.setUserName(userName);
		userBansosLoginSessionDao.save(session);
		return new ResponseEntity<Object>(mapResult, HttpStatus.OK);
	}

	private Long getIdSeqByUserName(String userName) {
		List<UserBansosLoginHistory> userBansosLoginHistory = userBansosLoginHistoryDao
				.getAllHistoryUserBansos(userName);
		if (userBansosLoginHistory == null) {
			return 0L;
		}
		return Integer.toUnsignedLong(userBansosLoginHistory.size() + 1);

	}

}

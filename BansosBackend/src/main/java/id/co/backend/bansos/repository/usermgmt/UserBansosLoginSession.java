package id.co.backend.bansos.repository.usermgmt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import id.co.backend.bansos.repository.usermgmt.id.UserBansosLoginHistoryId;

@Entity
@Table(name = "User_Bansos_Login_Session")
public class UserBansosLoginSession {

	@Id
	@Column(name="user_name")
	private String userName;
	
	@Column(name="session_login")
	private String sessionLogin;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSessionLogin() {
		return sessionLogin;
	}

	public void setSessionLogin(String sessionLogin) {
		this.sessionLogin = sessionLogin;
	}
	
	
	
}

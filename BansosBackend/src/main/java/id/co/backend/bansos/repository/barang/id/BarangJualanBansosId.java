package id.co.backend.bansos.repository.barang.id;

import java.io.Serializable;

public class BarangJualanBansosId implements Serializable{

	private static final long serialVersionUID = 2381231056792951810L;

	private Long idSeq;
	
	private String userName;

	public Long getIdSeq() {
		return idSeq;
	}

	public void setIdSeq(Long idSeq) {
		this.idSeq = idSeq;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}

package id.co.backend.bansos.repository.common.id;

import java.io.Serializable;

public class BansosCommonInfoDeviceId implements Serializable{
	private static final long serialVersionUID = 3479006802223680741L;
	private String expoPushNotificationToken;
	private Integer seqDuplicate;
	public String getExpoPushNotificationToken() {
		return expoPushNotificationToken;
	}
	public void setExpoPushNotificationToken(String expoPushNotificationToken) {
		this.expoPushNotificationToken = expoPushNotificationToken;
	}
	public Integer getSeqDuplicate() {
		return seqDuplicate;
	}
	public void setSeqDuplicate(Integer seqDuplicate) {
		this.seqDuplicate = seqDuplicate;
	}	
	
	
}

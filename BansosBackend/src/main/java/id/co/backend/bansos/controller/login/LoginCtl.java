package id.co.backend.bansos.controller.login;

import java.util.Map;

import id.co.oob.lib.common.merchant.bansos.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import id.co.backend.bansos.controller.BaseCtl;
import id.co.backend.bansos.dao.common.BansosCommonInfoDeviceDao;
import id.co.backend.bansos.service.common.BansosCommonInfoDeviceSvc;
import id.co.backend.bansos.service.usermgmt.UserMgmtBansosSvc;

@RestController
@RequestMapping("/loginCtl")
public class LoginCtl extends BaseCtl{
	
	@Autowired
	private BansosCommonInfoDeviceSvc bansosCommonInfoDeviceSvc; 
	
	@PutMapping("/logout")
	public  String authLogout(@RequestBody QuerySingleTransactionBansos querySingleTransactionBansos) {
		UserBansosDecryption userBansosDecryption = getUserBansosDecryption(querySingleTransactionBansos.getSessionUser(),
				BANSOS_LOGIN);
		userMgmtBansosSvc.logoutMe(userBansosDecryption.getUserName());
		return "Logout Berhasil";
	}

	@PostMapping("/auth")
	public ResponseEntity<Object> authLogin(@RequestBody LoginBansosDto loginBansosDto) {
		System.out.println("user login bansos : " + new Gson().toJson(loginBansosDto));
		return userMgmtBansosSvc.saveLoginUserMgmt(loginBansosDto.getUserName(), loginBansosDto.getPassword(), loginBansosDto.getExpoToken());
	}
	
	@PostMapping("/checkSessionValidity")
	public ResponseEntity<Object> checkSessionValidity(@RequestBody UserSessionBansosDto userSessionBansosDto) {
		System.out.println("user login bansos : " + new Gson().toJson(userSessionBansosDto));
		return userMgmtBansosSvc.getSessionLoginOjectEntity(userSessionBansosDto.getSession());
	}

	@PutMapping("/changePass")
	public ResponseEntity<Object> changePass(@RequestBody ChangePasswordDto dto) {
		System.out.println("change pass : " + new Gson().toJson(dto));
		UserBansosDecryption userBansosDecryption = getUserBansosDecryption(dto.getSessionUser(),
				BANSOS_LOGIN);
		return userMgmtBansosSvc.changePass(userBansosDecryption.getUserName(), dto.getNewPass(), dto.getSessionUser());
	}
	
	@GetMapping("/helloWorld")
	public ResponseEntity<Object> helloWorld() {
		return new ResponseEntity<Object>("Hello World", HttpStatus.OK);
	}
	
	@PutMapping("/putDeviceInfo")
	public ResponseEntity<Object> putDeviceInfo(@RequestBody BansosCommonInfoDeviceDto bansosCommonInfoDeviceDto){
		return bansosCommonInfoDeviceSvc.saveCommonDeviceInfo(bansosCommonInfoDeviceDto);
	}
	
}

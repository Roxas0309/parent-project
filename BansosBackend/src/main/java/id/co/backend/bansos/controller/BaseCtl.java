package id.co.backend.bansos.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.aliyun.oss.OSS;
import com.google.common.base.Strings;

import id.co.backend.bansos.service.native_q.CustomNativeSvc;
import id.co.backend.bansos.service.usermgmt.UserMgmtBansosSvc;
import id.co.oob.lib.common.merchant.onboarding.BaseCommon;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.file.Base64FileDto;

@Component
public class BaseCtl extends BaseCommon{

	@Autowired
	protected OSS ossClient;
	
	@Autowired
	protected CustomNativeSvc customNativeSvc;
	
	@Value("${oss_BUCKET_NAME}")
	protected String BUCKET_NAME;
	

	@Value("${oss.folder.endpoint}")
	protected  String OSS_FOLDER_ENDPOINT;
	
	@Autowired
	protected UserMgmtBansosSvc userMgmtBansosSvc;
	
	public InputStream getInputStreamAliOss(String qrUrl) {
		InputStream is = ossClient.getObject(BUCKET_NAME,qrUrl).getObjectContent();
		//byte[] bit = is.
		return is;
	}
	
	protected String inputStreamToBase64(InputStream is) throws IOException {
		byte[] buffer = IOUtils.toByteArray(is);
		return  Base64.encodeBase64String(buffer);
	}

	public String getPathSaveNameAliOss(InputStream fileStream, String fileName, String folder) {
		String bucketName = BUCKET_NAME;
		String finalName = OSS_FOLDER_ENDPOINT+folder+DateConverter.convertDateToString(new Date(), "ddMMyyyyHHmmSS")+";"+fileName;
		//System.out.println("put object with : access key : " + springCloudAlicloudAccessKey + ", secret key : " + springCloudAlicloudSecretKey + ", in endPoint : " + springCloudAlicloudOssEndpoint);
		ossClient.putObject(bucketName,finalName, fileStream);
		return finalName;
	}
	
	public String getFileNew(Base64FileDto base64, String folder, String uuid) {
		String urlnya = null;
		if(!Strings.isNullOrEmpty(base64.getBase64())) {
			InputStream in = new ByteArrayInputStream(getBase64File(base64.getBase64()));
			String mimeType = null;
			String fileExtension = ".png";
			 try {
			        mimeType = URLConnection.guessContentTypeFromStream(in); //mimeType is something like "image/jpeg"
			        String delimiter="[/]";
			        String[] tokens = mimeType.split(delimiter);
			        fileExtension = "."+tokens[1];
			    } catch (IOException ioException){

			    }
			urlnya = getPathSaveNameAliOss(in,uuid+fileExtension,folder);
		}
		return urlnya;
	}
	
}

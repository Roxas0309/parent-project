package id.co.backend.bansos.repository.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Lob;
import javax.persistence.Table;

import id.co.backend.bansos.repository.common.id.BansosCommonInfoDeviceId;

@Entity
@Table(name = "Bansos_Common_Info_Device")
@IdClass(BansosCommonInfoDeviceId.class)
public class BansosCommonInfoDevice {
	
	@Lob
	@Column(name = "Dimension_Device")
	private String dimensionDevice; //"w x h"
	
	@Lob
	@Column(name = "Device_Operating_System")
	private String deviceOperatingSystem;
	
	@Id
	@Column(name =  "Expo_Push_Notification_Token")
	private String expoPushNotificationToken;
	
	@Lob
	@Column(name =  "Fcm_Push_Notification_Token")
	private String fcmPushNotificationToken;
	
	@Id
	@Column(name =  "Seq_Duplicate")
	private Integer seqDuplicate;
	
	@Column(name =  "user_Name", nullable = true)
	private String userName;
	
	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDimensionDevice() {
		return dimensionDevice;
	}

	public void setDimensionDevice(String dimensionDevice) {
		this.dimensionDevice = dimensionDevice;
	}

	public String getDeviceOperatingSystem() {
		return deviceOperatingSystem;
	}

	public void setDeviceOperatingSystem(String deviceOperatingSystem) {
		this.deviceOperatingSystem = deviceOperatingSystem;
	}

	public String getExpoPushNotificationToken() {
		return expoPushNotificationToken;
	}

	public void setExpoPushNotificationToken(String expoPushNotificationToken) {
		this.expoPushNotificationToken = expoPushNotificationToken;
	}

	public String getFcmPushNotificationToken() {
		return fcmPushNotificationToken;
	}

	public void setFcmPushNotificationToken(String fcmPushNotificationToken) {
		this.fcmPushNotificationToken = fcmPushNotificationToken;
	}

	public Integer getSeqDuplicate() {
		return seqDuplicate;
	}

	public void setSeqDuplicate(Integer seqDuplicate) {
		this.seqDuplicate = seqDuplicate;
	}

	
}

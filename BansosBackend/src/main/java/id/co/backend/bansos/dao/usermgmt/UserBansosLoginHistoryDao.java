package id.co.backend.bansos.dao.usermgmt;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.backend.bansos.repository.usermgmt.UserBansosLoginHistory;
import id.co.backend.bansos.repository.usermgmt.id.UserBansosLoginHistoryId;

@Repository
public interface UserBansosLoginHistoryDao extends JpaRepository<UserBansosLoginHistory, UserBansosLoginHistoryId>{

	@Query("select a from UserBansosLoginHistory a where a.userName = ?1 ")
	List<UserBansosLoginHistory> getAllHistoryUserBansos(String userName);
}

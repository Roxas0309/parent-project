package id.co.backend.bansos.controller.transaksi;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;

import id.co.backend.bansos.controller.BaseCtl;
import id.co.backend.bansos.dao.barang.BarangJualanBansosDao;
import id.co.backend.bansos.dao.transaksi.PembeliBarangBansosDao;
import id.co.backend.bansos.dao.transaksi.TransaksiBarangBansosDao;
import id.co.backend.bansos.repository.barang.BarangJualanBansos;
import id.co.backend.bansos.repository.transaksi.PembeliBarangBansos;
import id.co.backend.bansos.repository.transaksi.TransaksiBarangBansos;
import id.co.backend.bansos.repository.usermgmt.UserBansos;
import id.co.oob.lib.common.merchant.bansos.dto.BansosSavePembelian;
import id.co.oob.lib.common.merchant.bansos.dto.BansosTransaksiPembelianDto;
import id.co.oob.lib.common.merchant.bansos.dto.BansosUserPembeliDetail;
import id.co.oob.lib.common.merchant.bansos.dto.PembeliBarangBansosDto;
import id.co.oob.lib.common.merchant.bansos.dto.PencairanTransaksiDto;
import id.co.oob.lib.common.merchant.bansos.dto.QuerySingleTransactionBansos;
import id.co.oob.lib.common.merchant.bansos.dto.UserBansosDecryption;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.convert.MoneyConverter;
import id.co.oob.lib.common.merchant.onboarding.file.Base64FileDto;

@Repository
@RestController
@RequestMapping("/bansosPencairanCtl")
public class BansosPencairanBarangCtl extends BaseCtl {

	@Autowired
	private PembeliBarangBansosDao pembeliBarangBansosDao;

	@Autowired
	private TransaksiBarangBansosDao transaksiBarangBansosDao;

	@Autowired
	private BarangJualanBansosDao barangJualanBansosDao;
	

	private static final String TOTAL_BELANJAAN = "TOTAL_BELANJAAN";
	private static final String TOTAL_PEMBAYARAN = "TOTAL_PEMBAYARAN";
	
	
	
	@PutMapping("/getPencairanTrx")
	public ResponseEntity<Object> queryGetCurrentPembeliDatas(@RequestBody  QuerySingleTransactionBansos querySingleTransactionBansos){
		System.out.println("queryTransaction barangPalingDicari : " + new Gson().toJson(querySingleTransactionBansos));
		UserBansosDecryption userBansosDecryption = getUserBansosDecryption(querySingleTransactionBansos.getSessionUser(),
				BANSOS_LOGIN);
		UserBansos userBansos = userMgmtBansosSvc.getUserBansos(userBansosDecryption.getUserName());
		String link = "http://localhost:6969/sales-qr/get?mid="+userBansos.getMid()+"&pmtDate="+querySingleTransactionBansos.getStartDate()+
				"&endDate="+querySingleTransactionBansos.getEndDate();
		ResponseEntity<String> resp = wsBody(link, null, HttpMethod.GET, null);
		
		Map<String, Object> mappBoy = mapperJsonToHashMap(resp.getBody());
		Map<String, Object> mappBoyData = mapperJsonToHashMap(new Gson().toJson(mappBoy.get("data")));
		List<PencairanTransaksiDto> pencairanTransaksiDtos = new ArrayList<PencairanTransaksiDto>();
		try {
			pencairanTransaksiDtos = mapperJsonToListDto(new Gson().toJson(mappBoyData.get("listResult")), PencairanTransaksiDto.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<PencairanTransaksiDto> showList = new ArrayList<PencairanTransaksiDto>();
		BigDecimal bd = new BigDecimal(0);
		for (PencairanTransaksiDto pencairanTransaksiDto : pencairanTransaksiDtos) {
			PencairanTransaksiDto transaksiDto = pencairanTransaksiDto;
			Boolean isCair = !Strings.isNullOrEmpty(pencairanTransaksiDto.getPmtDate());
			String cairWording = "Berhasil";
			if(!isCair) {
				bd = bd.add(new BigDecimal(pencairanTransaksiDto.getPmtAmt()));
				cairWording = "Diproses"; 
			}
			if(!Strings.isNullOrEmpty(pencairanTransaksiDto.getAuthDate())) {
			transaksiDto.setAuthDate(
					DateConverter.convertDateToAnotherDateFormat
					(pencairanTransaksiDto.getAuthDate(),"yyyyMMdd" , "dd MMM yyyy"));
			}
			transaksiDto.setIsCair(isCair);
			transaksiDto.setPencairanStatusWording(cairWording);
			transaksiDto.setPmtAmt("Rp "+MoneyConverter.convert(new BigDecimal(pencairanTransaksiDto.getPmtAmt())));
			
			if(!Strings.isNullOrEmpty(pencairanTransaksiDto.getPmtDate())) {
			transaksiDto.setPmtDate(DateConverter.convertDateToAnotherDateFormat
					(pencairanTransaksiDto.getPmtDate(),"yyyyMMdd" , "dd MMM yyyy"));
			}
			showList.add(transaksiDto);
		}
		
		
		
		Map<String, Object> mapp = new HashMap<String, Object>();
		mapp.put("allRiwayats", showList);
		
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
		dt = c.getTime();
		
		mapp.put("tglNow", DateConverter.convertDateToString(dt, "dd MMM yyyy"));
		mapp.put("yangBelomDicairkan", "Rp "+MoneyConverter.convert(bd));
		
		return new ResponseEntity<Object>(mapp,HttpStatus.OK);
	}

	

}

package id.co.backend.bansos.repository.transaksi.id;

import java.io.Serializable;

public class TransaksiBarangBansosId implements Serializable{

	private static final long serialVersionUID = -4598701302894647196L;
	
	private String pembeliNo;
	private String transaksiNo;
	
	
	
	public String getPembeliNo() {
		return pembeliNo;
	}
	public void setPembeliNo(String pembeliNo) {
		this.pembeliNo = pembeliNo;
	}
	public String getTransaksiNo() {
		return transaksiNo;
	}
	public void setTransaksiNo(String transaksiNo) {
		this.transaksiNo = transaksiNo;
	}
	
	

}

package id.co.backend.bansos.repository.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Master_Properties_Table")
public class MasterPropertiesTable {
	@Id
	@Column(name="id_Name")
	private String idName; 
	
	@Column(name="nama")
	private String nama;
	
	@Column(name="tipe")
	private String tipe;

	public String getIdName() {
		return idName;
	}

	public void setIdName(String idName) {
		this.idName = idName;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}  
	
	
}

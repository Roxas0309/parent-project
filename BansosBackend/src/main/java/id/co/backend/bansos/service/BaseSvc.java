package id.co.backend.bansos.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import id.co.oob.db.qris.merchant.onboarding.repository.log.EmailLogTable;
import id.co.oob.lib.common.merchant.onboarding.BaseCommon;

@Component
public class BaseSvc extends BaseCommon{
	
	public static final Integer ERROR_CONFLICT_USER_LOGIN = 10;
	public static final Integer ERROR_NOT_FOUND_USER = 11;
	public static final Integer SUCCESS_TRX = 0;
	
}

package id.co.oob.merchant.onboarding.lib.encrypt;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


//Ini yang dipakai untuk X-SIGNATURE Type Transaction Type
public class XSignatureHmacSha512Vers2 {
	
	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final String HMAC_SHA512 = "HmacSHA512";
	 
	
	  public static String buildHmacSignature(String value, String secret) {
	        String result;
	        try {
	            Mac hmacSHA512 = Mac.getInstance("HmacSHA512");
	            SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes(),
	                    "HmacSHA512");
	            hmacSHA512.init(secretKeySpec);

	            byte[] digest = hmacSHA512.doFinal(value.getBytes());
	            BigInteger hash = new BigInteger(1, digest);
	            result = hash.toString(16);
	            if ((result.length() % 2) != 0) {
	                result = "0" + result;
	            }
	        } catch (IllegalStateException | InvalidKeyException | NoSuchAlgorithmException ex) {
	            throw new RuntimeException("Problemas calculando HMAC", ex);
	        }
	        return result;
	    }
	
	public static void main(String[] args) {
		  final String key = "40cc9870-3593-4368-9d1e-66dda33051ac";
	        final String message = 
	        		"POST:/openapi/customers/v1.0/CustomerAccountInquiryByCIF:eyJraWQiOiJzc29zIiwiYWxnIjoiUlM1MTIifQ.eyJzdWIiOiI2ZjI0YjkxNi1mOGFlLTQzYzktODc1Yi03YzhmNzlkNjY0ZTMiLCJhdWQiOlsiNmYyNGI5MTYtZjhhZS00M2M5LTg3NWItN2M4Zjc5ZDY2NGUzIiwiand0LWF1ZCJdLCJjbGllbnRJZCI6ImYzMTUwZGFhLTMwZWYtNDIwNi1hMjRkLWQzYjdmNDU5YzczMyIsImlzcyI6Imp3dC1pc3N1ZXIiLCJleHAiOjE2MDc5MjMxNDMsImlhdCI6MTYwNzkyMjI0M30.P85USlK5kGybVAZITG1FNjUthLCD5XEr9Qg0hG_HUk3fhFZgaSHN_5cIg2X6Cf6bO0N164_KX2EVvHS3D6HuDNJnlN9mOl3XgaTIDgQPSN0hQehF8JJgxRVo7LBpVMbwB9n3oxYSa58X7sTWe9GxYDEdiD085AeU_dNFjqIzhVxdM8fmOY1UjDwtyH-coizeOs1MZ-SxlaCgLc3Fk8u_I28QGgmjzRoxZearA_Up4yC5Hmupp9zw6MPkt0m8XGl7ef4kyt7cKQoDcp2g0gFaN1_h9Db1viqrFNn7f9aPx3O7VnxqWcpwV0IT1-40U8JK_fL7AeXqInuhHDJA9G4xHA:{\"cifNumber\":\"23000317493\"}:2020-12-14T11:24:00.000T+0700";
		String result = buildHmacSignature(message, key);
		System.err.println(result);
	}
	 
}

package id.co.oob.merchant.onboarding.controller.base64;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import id.co.oob.db.qris.merchant.onboarding.dao.user.UserOobMidDtlTableDao;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobMidDtlTable;
import id.co.oob.merchant.onboarding.controller.BaseCtl;

@RestController
public class QrImageCtl extends BaseCtl{

	@Autowired
	private UserOobMidDtlTableDao userOobMidDtlTableDao; 
	
	 @GetMapping(value = "/{path_number}/QRMandiri", produces = MediaType.IMAGE_JPEG_VALUE,
			 headers="Accept=image/jpeg, image/jpg, image/png, image/gif")
	 public @ResponseBody byte[] getQRMandiri(@PathVariable("path_number") Long pathNumber, HttpServletResponse httpServletResponse) throws IllegalStateException, IOException {
		System.out.println("testing : " + pathNumber);
		 UserOobMidDtlTable userOobMidDtlTable = userOobMidDtlTableDao.getOne(pathNumber); 
		   InputStreamResource resource = new InputStreamResource(getInputStreamAliOss
		    		(userOobMidDtlTable.getPathQrNumber()));
		    InputStream is = resource.getInputStream();
		    BufferedImage  bi = ImageIO.read(is);
		    ByteArrayOutputStream bao=new ByteArrayOutputStream();
		    ImageIO.write(bi,"png",bao);
		    return bao.toByteArray();
	 }
}

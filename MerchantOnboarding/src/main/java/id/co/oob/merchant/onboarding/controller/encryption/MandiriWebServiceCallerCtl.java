package id.co.oob.merchant.onboarding.controller.encryption;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.token.AuthenticationSpiritMaker;
import id.co.oob.lib.common.merchant.onboarding.dto.token.MandiriTokenResponse;
import id.co.oob.lib.common.merchant.onboarding.encryptor.SHA256withRSAEncrypted;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;
import id.co.oob.merchant.onboarding.controller.BaseCtl;
import id.co.oob.merchant.onboarding.lib.encrypt.XSignatureHmacSha512Vers2;

@RestController
@RequestMapping("/mandiriCtl")
public class MandiriWebServiceCallerCtl extends BaseCtl{

	@PostMapping("/callMandiri")
	public ResponseEntity<String>  callMandiri(
			@RequestBody Object body,
			@RequestHeader(name = "path", required = true) String path){
		String objBody = new Gson().toJson(body);
		System.out.println("objBody " + objBody);
		objBody = objBody.replace("\\u003c", "<");
		System.out.println("objBody terbaru" + objBody);
		String url_otp_auth = MANDIRI_URL+"/openapi/auth/token";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("grant_type", "client_credentials");
		String timeStamp = DateConverter.convertDateToString(new Date(),
				"yyyy-MM-dd'T'HH:mm:ss.SSS'T'+0700");
		System.out.println("clientId " + clientId);
		System.out.println("keyPassword " + keyPassword);
		System.out.println("keyAlias " + keyAlias);
		System.out.println("keyStoreUrl " + keyStoreUrl);
		System.out.println("deployMentFrom " + deployMentFrom );
		System.out.println("timestamp " + timeStamp );
		String message = clientId+"|"+timeStamp;
		System.out.println("message " + message);
		
		
		Map<String, Object> mappo = null;
		try {
			 mappo = SHA256withRSAEncrypted.mySignatureWithInputStream
					(message, keyPassword, keyAlias, getInputStreamAliOss(urlJks));
		} catch (UnrecoverableKeyException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException
				| CertificateException | SignatureException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException(e.getMessage());
		}
		
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("X-Mandiri-Key", clientId);
		headerMap.put("X-SIGNATURE", (String) mappo.get(SHA256withRSAEncrypted.SIGNATURE));
		headerMap.put("X-TIMESTAMP", timeStamp);
		ResponseEntity<String> response = 
				wsBody(url_otp_auth, map, HttpMethod.POST, headerMap);
		System.err.println("url otp auth adalah : " + new Gson().toJson(response));
		MandiriTokenResponse mandiriTokenResponse = new MandiriTokenResponse();
		try {
			mandiriTokenResponse = mapperJsonToSingleDto(response.getBody(), MandiriTokenResponse.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Token Mandiri tidak dapat digenerate");
		}
		
		System.err.println("req payload " + objBody);
		
		String value = "POST:"+path+":"+mandiriTokenResponse.getAccessToken()+
				":"+objBody+":"+timeStamp;
		System.out.println("value : " + value);
		AuthenticationSpiritMaker authenticationSpiritMaker = new AuthenticationSpiritMaker();
		authenticationSpiritMaker.setAuthorization(mandiriTokenResponse.getTokenType()+" " + mandiriTokenResponse.getAccessToken());
		authenticationSpiritMaker.setxTimestamp(timeStamp);
		authenticationSpiritMaker.setxSignature(XSignatureHmacSha512Vers2.buildHmacSignature
				(value, clientSecret));
		Map<String, String> mapApi = new HashMap<String, String>();
		mapApi.put("Authorization", authenticationSpiritMaker.getAuthorization());
		mapApi.put("X-SIGNATURE", authenticationSpiritMaker.getxSignature());
		mapApi.put("X-TIMESTAMP", authenticationSpiritMaker.getxTimestamp());
		ResponseEntity<String> responseApi = wsBody
				(MANDIRI_URL+path, body, HttpMethod.POST, mapApi);
		return responseApi;
	}
	
}

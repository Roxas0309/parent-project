package id.co.oob.merchant.onboarding.controller.home;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.CountProjection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import antlr.MakeGrammar;
import id.co.oob.db.qris.merchant.onboarding.repository.user.AllUserOobDtlRepo;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserNotifDtlTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableAuthDtl;
import id.co.oob.db.qris.merchant.onboarding.service.authentication.AllTheAuthenticationCircuitSvc;
import id.co.oob.lib.common.merchant.onboarding.convert.CharacterRemoval;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.convert.MoneyConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.TBACMERPMTCTNT_MSTDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.MaasResponseDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat.RiwayatPencairanDtlDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat.RiwayatPencairanDtlRincianDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat.RiwayatPencairanHdrDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat.RiwayatTransaksiDtlDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat.RiwayatTransaksiDtlRincianDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat.RiwayatTransaksiHdrDto;
import id.co.oob.lib.common.merchant.onboarding.dto.success.SuccessValidation;
import id.co.oob.lib.common.merchant.onboarding.encryptor.MaskingNumber;
import id.co.oob.lib.common.merchant.onboarding.throwable.LengthValidationException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;
import id.co.oob.lib.common.merchant.onboarding.throwable.SizeDataOverCapacityException;
import id.co.oob.lib.common.merchant.onboarding.validation.DateValidation;
import id.co.oob.merchant.onboarding.controller.BaseCtl;

@RestController
@RequestMapping("/homeScreen")
public class HomeScreenRequestCtl extends BaseCtl {

	@Autowired
	private AllTheAuthenticationCircuitSvc allTheAuthenticationCircuitSvc;

	@GetMapping("/getPaidUnpaidMoney")
	public Map<String, Object> getPaidUnpaidMoney(@RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, @RequestParam("mid") String mid) {
		RiwayatTransaksiHdrDto riwayatTransaksiHdrDto = getDataTransaksiByFilter(startDate, endDate, mid, false);
		BigDecimal paid = new BigDecimal(0);
		BigDecimal unpaid = new BigDecimal(0);

		for (RiwayatTransaksiDtlDto detail : riwayatTransaksiHdrDto.getDetail()) {
			if (detail.getIsTransferToRek()) {
				paid = paid.add(detail.getDetail().getTransferAmountNumber());
			} else {
				unpaid = unpaid.add(detail.getDetail().getTransferAmountNumber());
			}
		}

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("unpaidTransaksi", unpaid);
		map.put("unpaidTransaksiInRupiah", "Rp. " + MoneyConverter.convert(unpaid));
		map.put("paidTransaksi", paid);
		map.put("paidTransaksiRupiah", "Rp. " + MoneyConverter.convert(paid));
		return map;
	}

	@GetMapping("/getPaidUnpaidMoney/auth")
	public ResponseEntity<Object> getPaidUnpaidMoneyAuth(
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate,
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken) {

		AuthorizedResponseFirst(secretKey, secretId);
		AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken(secretToken, secretKey);
		String mid = allUserOobDtlRepo.getUserOobMidDtlTableDto().getMidNumber();

		return new ResponseEntity<Object>(
				new SuccessValidation("Data Sukses Retrieve", getPaidUnpaidMoney(startDate, endDate, mid)),
				HttpStatus.OK);

	}

	@GetMapping("/getRincianPencairan/auth")
	public ResponseEntity<Object> getDataTransaksiAuth(
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate,
			@RequestParam(value = "isLimitValidated", required = true) Boolean isLimitValidated,
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken) {
		if (isLimitValidated) {
			Long getDiffDays = DateValidation.getDifferentDays(startDate, endDate);
			System.out.println("get diffDays : " + getDiffDays);
			if (getDiffDays > 90 || getDiffDays < 7) {
				throw new NotAcceptanceDataException("Difference Start Date And End Date : " + getDiffDays);
			}
		}
		AuthorizedResponseFirst(secretKey, secretId);
		AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken(secretToken, secretKey);
		String mid = allUserOobDtlRepo.getUserOobMidDtlTableDto().getMidNumber();
		List<RiwayatPencairanHdrDto> riwayatPencairanHdrDtos = getRincianPencairanByFilter(startDate, endDate, mid);

		return new ResponseEntity<Object>(new SuccessValidation("Data Sukses Retrieve", riwayatPencairanHdrDtos),
				HttpStatus.OK);
	}
	
	

	@GetMapping("/getRincianPencairan")
	public List<RiwayatPencairanHdrDto> getRincianPencairanByFilter(@RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, @RequestParam("mid") String mid) {
		Integer getDifferentDays = DateValidation.getDifferentDays(startDate, endDate).intValue();
		List<RiwayatPencairanHdrDto> riwayatPencairanHdrDtos = new ArrayList<RiwayatPencairanHdrDto>();
		RiwayatTransaksiHdrDto riwayatTransaksiHdrDto = getDataTransaksiByFilter(DateConverter.addDays(startDate, "yyyyMMdd", -30),endDate, mid, false);
//		System.out.println("riwayatTransaksiHdrDto result : " + new Gson().toJson(riwayatTransaksiHdrDto));
		for (int i = 0; i <= getDifferentDays; i++) {
			String tanggal = DateConverter.addDays(endDate, "yyyyMMdd", (0 - i));
			RiwayatPencairanHdrDto riwayatPencairanHdrDto = new RiwayatPencairanHdrDto();
			riwayatPencairanHdrDto
					.setPencairanDate(DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMM yyyy"));
			riwayatPencairanHdrDto.setPencairanDateWithoutYear(
					DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMMM"));
			riwayatPencairanHdrDto.setNamaHari(DateConverter.getdayOfDate(tanggal, "yyyyMMdd"));

			List<RiwayatPencairanDtlDto> allRiwayats = new ArrayList<RiwayatPencairanDtlDto>();
			BigDecimal totalPencairan = new BigDecimal(0);

			RiwayatPencairanDtlDto pencairanHold08 = new RiwayatPencairanDtlDto();
			RiwayatPencairanDtlDto pencairanHold16 = new RiwayatPencairanDtlDto();
			RiwayatPencairanDtlDto pencairanHold20 = new RiwayatPencairanDtlDto();
			RiwayatPencairanDtlDto pencairanAfterHold08 = new RiwayatPencairanDtlDto();
			RiwayatPencairanDtlDto pencairanAfterHold16 = new RiwayatPencairanDtlDto();
			RiwayatPencairanDtlDto pencairanAfterHold20 = new RiwayatPencairanDtlDto();
			RiwayatPencairanDtlDto pencairan08 = new RiwayatPencairanDtlDto();
			RiwayatPencairanDtlDto pencairan16 = new RiwayatPencairanDtlDto();
			RiwayatPencairanDtlDto pencairan20 = new RiwayatPencairanDtlDto();
			List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtosHold08 = new ArrayList<RiwayatPencairanDtlRincianDto>();
			List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtosHold16 = new ArrayList<RiwayatPencairanDtlRincianDto>();
			List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtosHold20 = new ArrayList<RiwayatPencairanDtlRincianDto>();
			List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtosCairAfterHold08 = new ArrayList<RiwayatPencairanDtlRincianDto>();
			List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtosCairAfterHold16 = new ArrayList<RiwayatPencairanDtlRincianDto>();
			List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtosCairAfterHold20 = new ArrayList<RiwayatPencairanDtlRincianDto>();
			List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtos08 = new ArrayList<RiwayatPencairanDtlRincianDto>();
			List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtos16 = new ArrayList<RiwayatPencairanDtlRincianDto>();
			List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtos20 = new ArrayList<RiwayatPencairanDtlRincianDto>();

			Integer countTransaksiHold08 = 0;
			Integer countTransaksiHold16 = 0;
			Integer countTransaksiHold20 = 0;
			Integer countTransaksiHoldAfterHold08 = 0;
			Integer countTransaksiHoldAfterHold16 = 0;
			Integer countTransaksiHoldAfterHold20 = 0;
			Integer countTransaksi08 = 0;
			Integer countTransaksi16 = 0;
			Integer countTransaksi20 = 0;

			BigDecimal feeAmountHold08 = new BigDecimal(0);
			BigDecimal feeAmountHold16 = new BigDecimal(0);
			BigDecimal feeAmountHold20 = new BigDecimal(0);
			BigDecimal feeAmountHoldAfterHold08 = new BigDecimal(0);
			BigDecimal feeAmountHoldAfterHold16 = new BigDecimal(0);
			BigDecimal feeAmountHoldAfterHold20 = new BigDecimal(0);
			BigDecimal feeAmount08 = new BigDecimal(0);
			BigDecimal feeAmount16 = new BigDecimal(0);
			BigDecimal feeAmount20 = new BigDecimal(0);

			BigDecimal pencairanMoneyHold08 = new BigDecimal(0);
			BigDecimal pencairanMoneyHold16 = new BigDecimal(0);
			BigDecimal pencairanMoneyHold20 = new BigDecimal(0);
			BigDecimal pencairanMoneyAfterHold08 = new BigDecimal(0);
			BigDecimal pencairanMoneyAfterHold16 = new BigDecimal(0);
			BigDecimal pencairanMoneyAfterHold20 = new BigDecimal(0);
			BigDecimal pencairanMoney08 = new BigDecimal(0);
			BigDecimal pencairanMoney16 = new BigDecimal(0);
			BigDecimal pencairanMoney20 = new BigDecimal(0);

			BigDecimal pencairanMoneyWithoutFeeHold08 = new BigDecimal(0);
			BigDecimal pencairanMoneyWithoutFeeHold16 = new BigDecimal(0);
			BigDecimal pencairanMoneyWithoutFeeHold20 = new BigDecimal(0);
			BigDecimal pencairanMoneyWithoutFeeAfterHold08 = new BigDecimal(0);
			BigDecimal pencairanMoneyWithoutFeeAfterHold16 = new BigDecimal(0);
			BigDecimal pencairanMoneyWithoutFeeAfterHold20 = new BigDecimal(0);
			BigDecimal pencairanMoneyWithoutFee08 = new BigDecimal(0);
			BigDecimal pencairanMoneyWithoutFee16 = new BigDecimal(0);
			BigDecimal pencairanMoneyWithoutFee20 = new BigDecimal(0);

			String pmtHoldDateAfterString = ""; 
			for (RiwayatTransaksiDtlDto detail : riwayatTransaksiHdrDto.getDetail()) {

				if(tanggal.equalsIgnoreCase(detail.getPmtHoldDate())) {
					if(detail.getPmtHoldTime().equals("16:00")) {
						countTransaksiHold16++;
						feeAmountHold16 = feeAmountHold16.add(detail.getFeeAmountNumber());
						pencairanMoneyHold16 = pencairanMoneyHold16.add(detail.getDetail().getTransferAmountNumber());
						pencairanMoneyWithoutFeeHold16 = pencairanMoneyWithoutFeeHold16
								.add(detail.getAuthAmountNumber());
						RiwayatPencairanDtlRincianDto dto = new RiwayatPencairanDtlRincianDto();
						dto.setAuthNo(detail.getAuthSeqNo());
						dto.setAuthNumberInRupiah(detail.getDetail().getTransferAmount());
						dto.setAuthTime(detail.getTime());
						dto.setIssuerName(detail.getIssuerPayment());
						dto.setReffNo(detail.getDetail().getReffNumber());
						riwayatPencairanDtlRincianDtosHold16.add(dto);
					}
					else if(detail.getPmtHoldTime().equals("20:00")) {
						countTransaksiHold20++;
						feeAmountHold20 = feeAmountHold20.add(detail.getFeeAmountNumber());
						pencairanMoneyHold20 = pencairanMoneyHold20.add(detail.getDetail().getTransferAmountNumber());
						pencairanMoneyWithoutFeeHold20 = pencairanMoneyWithoutFeeHold20
								.add(detail.getAuthAmountNumber());
						RiwayatPencairanDtlRincianDto dto = new RiwayatPencairanDtlRincianDto();
						dto.setAuthNo(detail.getAuthSeqNo());
						dto.setAuthNumberInRupiah(detail.getDetail().getTransferAmount());
						dto.setAuthTime(detail.getTime());
						dto.setIssuerName(detail.getIssuerPayment());
						dto.setReffNo(detail.getDetail().getReffNumber());
						riwayatPencairanDtlRincianDtosHold20.add(dto);
					}
					else if(detail.getPmtHoldTime().equals("08:00")) {
						countTransaksiHold08++;
						feeAmountHold08 = feeAmountHold08.add(detail.getFeeAmountNumber());
						pencairanMoneyHold08 = pencairanMoneyHold08.add(detail.getDetail().getTransferAmountNumber());
						pencairanMoneyWithoutFeeHold08 = pencairanMoneyWithoutFeeHold08
								.add(detail.getAuthAmountNumber());
						RiwayatPencairanDtlRincianDto dto = new RiwayatPencairanDtlRincianDto();
						dto.setAuthNo(detail.getAuthSeqNo());
						dto.setAuthNumberInRupiah(detail.getDetail().getTransferAmount());
						dto.setAuthTime(detail.getTime());
						dto.setIssuerName(detail.getIssuerPayment());
						dto.setReffNo(detail.getDetail().getReffNumber());
						riwayatPencairanDtlRincianDtosHold08.add(dto);
					}
					
				}
				
				if (tanggal.equalsIgnoreCase(detail.getPmtDate())) {

					if (detail.getTimePembayaran() != null) {

						if (detail.getTimePembayaran().equals("16:00")) {

							if (detail.getPmtHoldDate() == null) {
								countTransaksi16++;
								feeAmount16 = feeAmount16.add(detail.getFeeAmountNumber());
								pencairanMoney16 = pencairanMoney16.add(detail.getDetail().getTransferAmountNumber());
								pencairanMoneyWithoutFee16 = pencairanMoneyWithoutFee16
										.add(detail.getAuthAmountNumber());
								RiwayatPencairanDtlRincianDto dto = new RiwayatPencairanDtlRincianDto();
								dto.setAuthNo(detail.getAuthSeqNo());
								dto.setAuthNumberInRupiah(detail.getDetail().getTransferAmount());
								dto.setAuthTime(detail.getTime());
								dto.setIssuerName(detail.getIssuerPayment());
								dto.setReffNo(detail.getDetail().getReffNumber());
								riwayatPencairanDtlRincianDtos16.add(dto);
							} else {
								countTransaksiHoldAfterHold16++;
								pmtHoldDateAfterString =  	DateConverter.convertDateToAnotherDateFormat
										(detail.getPmtHoldDate(), "yyyyMMdd", "dd MMM");
								feeAmountHoldAfterHold16 = feeAmountHoldAfterHold16.add(detail.getFeeAmountNumber());
								pencairanMoneyAfterHold16 = pencairanMoneyAfterHold16.add(detail.getDetail().getTransferAmountNumber());
								pencairanMoneyWithoutFeeAfterHold16 = pencairanMoneyWithoutFeeAfterHold16
										.add(detail.getAuthAmountNumber());
								RiwayatPencairanDtlRincianDto dto = new RiwayatPencairanDtlRincianDto();
								dto.setAuthNo(detail.getAuthSeqNo());
								dto.setAuthNumberInRupiah(detail.getDetail().getTransferAmount());
								dto.setAuthTime(detail.getTime());
								dto.setIssuerName(detail.getIssuerPayment());
								dto.setReffNo(detail.getDetail().getReffNumber());
								riwayatPencairanDtlRincianDtosCairAfterHold16.add(dto);
							}
							
						} else if (detail.getTimePembayaran().equals("20:00")) {
							if (detail.getPmtHoldDate() == null) {
								countTransaksi20++;
								feeAmount20 = feeAmount20.add(detail.getFeeAmountNumber());
								pencairanMoney20 = pencairanMoney20.add(detail.getDetail().getTransferAmountNumber());
								pencairanMoneyWithoutFee20 = pencairanMoneyWithoutFee20
										.add(detail.getAuthAmountNumber());
								RiwayatPencairanDtlRincianDto dto = new RiwayatPencairanDtlRincianDto();
								dto.setAuthNo(detail.getAuthSeqNo());
								dto.setAuthNumberInRupiah(detail.getDetail().getTransferAmount());
								dto.setAuthTime(detail.getTime());
								dto.setIssuerName(detail.getIssuerPayment());
								dto.setReffNo(detail.getDetail().getReffNumber());
								riwayatPencairanDtlRincianDtos20.add(dto);
							} else {
								countTransaksiHoldAfterHold20++;
								pmtHoldDateAfterString =  	DateConverter.convertDateToAnotherDateFormat
										(detail.getPmtHoldDate(), "yyyyMMdd", "dd MMM");
								feeAmountHoldAfterHold20 = feeAmountHoldAfterHold16.add(detail.getFeeAmountNumber());
								pencairanMoneyAfterHold20 = pencairanMoneyAfterHold16.add(detail.getDetail().getTransferAmountNumber());
								pencairanMoneyWithoutFeeAfterHold20 = pencairanMoneyWithoutFeeAfterHold20
										.add(detail.getAuthAmountNumber());
								RiwayatPencairanDtlRincianDto dto = new RiwayatPencairanDtlRincianDto();
								dto.setAuthNo(detail.getAuthSeqNo());
								dto.setAuthNumberInRupiah(detail.getDetail().getTransferAmount());
								dto.setAuthTime(detail.getTime());
								dto.setIssuerName(detail.getIssuerPayment());
								dto.setReffNo(detail.getDetail().getReffNumber());
								riwayatPencairanDtlRincianDtosCairAfterHold20.add(dto);
							}

						} else if (detail.getTimePembayaran().equals("08:00")) {
							if (detail.getPmtHoldDate() == null) {
								countTransaksi08++;
								feeAmount08 = feeAmount08.add(detail.getFeeAmountNumber());
								pencairanMoney08 = pencairanMoney08.add(detail.getDetail().getTransferAmountNumber());
								pencairanMoneyWithoutFee08 = pencairanMoneyWithoutFee08
										.add(detail.getAuthAmountNumber());
								RiwayatPencairanDtlRincianDto dto = new RiwayatPencairanDtlRincianDto();
								dto.setAuthNo(detail.getAuthSeqNo());
								dto.setAuthNumberInRupiah(detail.getDetail().getTransferAmount());
								dto.setAuthTime(detail.getTime());
								dto.setIssuerName(detail.getIssuerPayment());
								dto.setReffNo(detail.getDetail().getReffNumber());
								riwayatPencairanDtlRincianDtos08.add(dto);
							} else {
								countTransaksiHoldAfterHold08++;
								pmtHoldDateAfterString =  	DateConverter.convertDateToAnotherDateFormat
										(detail.getPmtHoldDate(), "yyyyMMdd", "dd MMM");
								feeAmountHoldAfterHold08 = feeAmountHoldAfterHold16.add(detail.getFeeAmountNumber());
								pencairanMoneyAfterHold08 = pencairanMoneyAfterHold16.add(detail.getDetail().getTransferAmountNumber());
								pencairanMoneyWithoutFeeAfterHold08 = pencairanMoneyWithoutFeeAfterHold08
										.add(detail.getAuthAmountNumber());
								RiwayatPencairanDtlRincianDto dto = new RiwayatPencairanDtlRincianDto();
								dto.setAuthNo(detail.getAuthSeqNo());
								dto.setAuthNumberInRupiah(detail.getDetail().getTransferAmount());
								dto.setAuthTime(detail.getTime());
								dto.setIssuerName(detail.getIssuerPayment());
								dto.setReffNo(detail.getDetail().getReffNumber());
								riwayatPencairanDtlRincianDtosCairAfterHold08.add(dto);
							}
							
						}
					}
				}
			}

			pencairan08.setCountTransaksi(countTransaksi08);
			pencairan08.setFeeAmount(feeAmount08);
			pencairan08.setFeeAmountInString("Rp. " + MoneyConverter.convert(feeAmount08));
			pencairan08.setIsPencairanLanjutan(false);
			pencairan08.setIsPencairanSusulan(false);
			pencairan08.setNamaHari(DateConverter.getdayOfDate(tanggal, "yyyyMMdd"));
			pencairan08
					.setPencairanDate(DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMM yyyy"));
			pencairan08.setPencairanDateWithoutYear(
					DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMMM"));
			pencairan08.setPencairanMoney(pencairanMoney08);
			pencairan08.setPencairanMoneyInString("Rp. " + MoneyConverter.convert(pencairanMoney08));
			pencairan08.setPencairanMoneyWithoutFee(pencairanMoneyWithoutFee08);
			pencairan08
					.setPencairanMoneyWithoutFeeInString("Rp. " + MoneyConverter.convert(pencairanMoneyWithoutFee08));
			pencairan08.setPencairanTime("08:00");
			pencairan08.setPencairanType("Pencairan 1");
			pencairan08.setPencairanTypeCode("1");
			pencairan08.setRiwayatPencairanDtlRincianDtos(riwayatPencairanDtlRincianDtos08);

			pencairan16.setCountTransaksi(countTransaksi16);
			pencairan16.setFeeAmount(feeAmount16);
			pencairan16.setFeeAmountInString("Rp. " + MoneyConverter.convert(feeAmount16));
			pencairan16.setIsPencairanLanjutan(false);
			pencairan16.setIsPencairanSusulan(false);
			pencairan16.setNamaHari(DateConverter.getdayOfDate(tanggal, "yyyyMMdd"));
			pencairan16
					.setPencairanDate(DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMM yyyy"));
			pencairan16.setPencairanDateWithoutYear(
					DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMMM"));
			pencairan16.setPencairanMoney(pencairanMoney16);
			pencairan16.setPencairanMoneyInString("Rp. " + MoneyConverter.convert(pencairanMoney16));
			pencairan16.setPencairanMoneyWithoutFee(pencairanMoneyWithoutFee16);
			pencairan16
					.setPencairanMoneyWithoutFeeInString("Rp. " + MoneyConverter.convert(pencairanMoneyWithoutFee16));
			pencairan16.setPencairanTime("16:00");
			pencairan16.setPencairanType("Pencairan 2");
			pencairan16.setPencairanTypeCode("2");
			pencairan16.setRiwayatPencairanDtlRincianDtos(riwayatPencairanDtlRincianDtos16);

			pencairan20.setCountTransaksi(countTransaksi20);
			pencairan20.setFeeAmount(feeAmount20);
			pencairan20.setFeeAmountInString("Rp. " + MoneyConverter.convert(feeAmount20));
			pencairan20.setIsPencairanLanjutan(false);
			pencairan20.setIsPencairanSusulan(false);
			pencairan20.setNamaHari(DateConverter.getdayOfDate(tanggal, "yyyyMMdd"));
			pencairan20
					.setPencairanDate(DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMM yyyy"));
			pencairan20.setPencairanDateWithoutYear(
					DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMMM"));
			pencairan20.setPencairanMoney(pencairanMoney20);
			pencairan20.setPencairanMoneyInString("Rp. " + MoneyConverter.convert(pencairanMoney20));
			pencairan20.setPencairanMoneyWithoutFee(pencairanMoneyWithoutFee20);
			pencairan20
					.setPencairanMoneyWithoutFeeInString("Rp. " + MoneyConverter.convert(pencairanMoneyWithoutFee20));
			pencairan20.setPencairanTime("20:00");
			pencairan20.setPencairanType("Pencairan 3");
			pencairan20.setPencairanTypeCode("2");
			pencairan20.setRiwayatPencairanDtlRincianDtos(riwayatPencairanDtlRincianDtos20);

			pencairanAfterHold08.setCountTransaksi(countTransaksiHoldAfterHold08);
			pencairanAfterHold08.setFeeAmount(feeAmountHoldAfterHold08);
			pencairanAfterHold08.setFeeAmountInString("Rp. " + MoneyConverter.convert(feeAmountHoldAfterHold08));
			pencairanAfterHold08.setIsPencairanLanjutan(false); //hold payment
			pencairanAfterHold08.setIsPencairanSusulan(true); //hold payment after paid
			pencairanAfterHold08.setNamaHari(DateConverter.getdayOfDate(tanggal, "yyyyMMdd"));
			pencairanAfterHold08.setPencairanDate(DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMM yyyy"));
			pencairanAfterHold08.setPencairanDateWithoutYear(
					DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMMM"));
			pencairanAfterHold08.setPencairanMoney(pencairanMoneyAfterHold08);
			pencairanAfterHold08.setPencairanMoneyInString("Rp. " + MoneyConverter.convert(pencairanMoneyAfterHold08));
			pencairanAfterHold08.setPencairanMoneyWithoutFee(pencairanMoneyWithoutFeeAfterHold08);
			pencairanAfterHold08.setPencairanMoneyWithoutFeeInString("Rp. " + MoneyConverter.convert(pencairanMoneyWithoutFeeAfterHold08));
			pencairanAfterHold08.setPencairanTime("08:00");
			pencairanAfterHold08.setPencairanType("Pencairan 1 Susulan");
			pencairanAfterHold08.setPencairanTypeCode("-1");
			pencairanAfterHold08.setPencairanBeforeWithoutYear(pmtHoldDateAfterString);
			pencairanAfterHold08.setRiwayatPencairanDtlRincianDtos(riwayatPencairanDtlRincianDtosCairAfterHold08);
			
			pencairanAfterHold16.setCountTransaksi(countTransaksiHoldAfterHold16);
			pencairanAfterHold16.setFeeAmount(feeAmountHoldAfterHold16);
			pencairanAfterHold16.setFeeAmountInString("Rp. " + MoneyConverter.convert(feeAmountHoldAfterHold16));
			pencairanAfterHold16.setIsPencairanLanjutan(false); //hold payment
			pencairanAfterHold16.setIsPencairanSusulan(true); //hold payment after paid
			pencairanAfterHold16.setNamaHari(DateConverter.getdayOfDate(tanggal, "yyyyMMdd"));
			pencairanAfterHold16.setPencairanDate(DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMM yyyy"));
			pencairanAfterHold16.setPencairanDateWithoutYear(
					DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMMM"));
			pencairanAfterHold16.setPencairanMoney(pencairanMoneyAfterHold16);
			pencairanAfterHold16.setPencairanMoneyInString("Rp. " + MoneyConverter.convert(pencairanMoneyAfterHold16));
			pencairanAfterHold16.setPencairanMoneyWithoutFee(pencairanMoneyWithoutFeeAfterHold16);
			pencairanAfterHold16.setPencairanMoneyWithoutFeeInString("Rp. " + MoneyConverter.convert(pencairanMoneyWithoutFeeAfterHold16));
			pencairanAfterHold16.setPencairanTime("16:00");
			pencairanAfterHold16.setPencairanType("Pencairan 2 Susulan");
			pencairanAfterHold16.setPencairanTypeCode("-1");
			pencairanAfterHold16.setPencairanBeforeWithoutYear(pmtHoldDateAfterString);
			pencairanAfterHold16.setRiwayatPencairanDtlRincianDtos(riwayatPencairanDtlRincianDtosCairAfterHold16);
			
			
			pencairanAfterHold20.setCountTransaksi(countTransaksiHoldAfterHold20);
			pencairanAfterHold20.setFeeAmount(feeAmountHoldAfterHold20);
			pencairanAfterHold20.setFeeAmountInString("Rp. " + MoneyConverter.convert(feeAmountHoldAfterHold20));
			pencairanAfterHold20.setIsPencairanLanjutan(false); //hold payment
			pencairanAfterHold20.setIsPencairanSusulan(true); //hold payment after paid
			pencairanAfterHold20.setNamaHari(DateConverter.getdayOfDate(tanggal, "yyyyMMdd"));
			pencairanAfterHold20.setPencairanDate(DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMM yyyy"));
			pencairanAfterHold20.setPencairanDateWithoutYear(
					DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMMM"));
			pencairanAfterHold20.setPencairanMoney(pencairanMoneyAfterHold20);
			pencairanAfterHold20.setPencairanMoneyInString("Rp. " + MoneyConverter.convert(pencairanMoneyAfterHold20));
			pencairanAfterHold20.setPencairanMoneyWithoutFee(pencairanMoneyWithoutFeeAfterHold20);
			pencairanAfterHold20.setPencairanMoneyWithoutFeeInString("Rp. " + MoneyConverter.convert(pencairanMoneyWithoutFeeAfterHold20));
			pencairanAfterHold20.setPencairanTime("20:00");
			pencairanAfterHold20.setPencairanType("Pencairan 3 Susulan");
			pencairanAfterHold20.setPencairanTypeCode("-1");
			pencairanAfterHold20.setPencairanBeforeWithoutYear(pmtHoldDateAfterString);
			pencairanAfterHold20.setRiwayatPencairanDtlRincianDtos(riwayatPencairanDtlRincianDtosCairAfterHold20);
			
			
			pencairanHold08.setCountTransaksi(countTransaksiHold08);
			pencairanHold08.setFeeAmount(feeAmountHold08);
			pencairanHold08.setFeeAmountInString("Rp. " + MoneyConverter.convert(feeAmountHold08));
			pencairanHold08.setIsPencairanLanjutan(false); //hold payment
			pencairanHold08.setIsPencairanSusulan(true); //hold payment after paid
			pencairanHold08.setNamaHari(DateConverter.getdayOfDate(tanggal, "yyyyMMdd"));
			pencairanHold08.setPencairanDate(DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMM yyyy"));
			pencairanHold08.setPencairanDateWithoutYear(
					DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMMM"));
			pencairanHold08.setPencairanMoney(pencairanMoneyHold08);
			pencairanHold08.setPencairanMoneyInString("Rp. " + MoneyConverter.convert(pencairanMoneyHold08));
			pencairanHold08.setPencairanMoneyWithoutFee(pencairanMoneyWithoutFeeHold08);
			pencairanHold08.setPencairanMoneyWithoutFeeInString("Rp. " + MoneyConverter.convert(pencairanMoneyWithoutFeeHold08));
			pencairanHold08.setPencairanTime("08:00");
			pencairanHold08.setPencairanType("Pencairan 1 Tertahan");
			pencairanHold08.setPencairanTypeCode("-2");
			pencairanHold08.setRiwayatPencairanDtlRincianDtos(riwayatPencairanDtlRincianDtosHold08);
			
			pencairanHold16.setCountTransaksi(countTransaksiHold16);
			pencairanHold16.setFeeAmount(feeAmountHold16);
			pencairanHold16.setFeeAmountInString("Rp. " + MoneyConverter.convert(feeAmountHold16));
			pencairanHold16.setIsPencairanLanjutan(false); //hold payment
			pencairanHold16.setIsPencairanSusulan(true); //hold payment after paid
			pencairanHold16.setNamaHari(DateConverter.getdayOfDate(tanggal, "yyyyMMdd"));
			pencairanHold16.setPencairanDate(DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMM yyyy"));
			pencairanHold16.setPencairanDateWithoutYear(
					DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMMM"));
			pencairanHold16.setPencairanMoney(pencairanMoneyHold16);
			pencairanHold16.setPencairanMoneyInString("Rp. " + MoneyConverter.convert(pencairanMoneyHold16));
			pencairanHold16.setPencairanMoneyWithoutFee(pencairanMoneyWithoutFeeHold16);
			pencairanHold16.setPencairanMoneyWithoutFeeInString("Rp. " + MoneyConverter.convert(pencairanMoneyWithoutFeeHold16));
			pencairanHold16.setPencairanTime("16:00");
			pencairanHold16.setPencairanType("Pencairan 2 Tertahan");
			pencairanHold16.setPencairanTypeCode("-2");
			pencairanHold16.setRiwayatPencairanDtlRincianDtos(riwayatPencairanDtlRincianDtosHold16);
			
			pencairanHold20.setCountTransaksi(countTransaksiHold20);
			pencairanHold20.setFeeAmount(feeAmountHold20);
			pencairanHold20.setFeeAmountInString("Rp. " + MoneyConverter.convert(feeAmountHold20));
			pencairanHold20.setIsPencairanLanjutan(false); //hold payment
			pencairanHold20.setIsPencairanSusulan(true); //hold payment after paid
			pencairanHold20.setNamaHari(DateConverter.getdayOfDate(tanggal, "yyyyMMdd"));
			pencairanHold20.setPencairanDate(DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMM yyyy"));
			pencairanHold20.setPencairanDateWithoutYear(
					DateConverter.convertDateToAnotherDateFormat(tanggal, "yyyyMMdd", "dd MMMM"));
			pencairanHold20.setPencairanMoney(pencairanMoneyHold20);
			pencairanHold20.setPencairanMoneyInString("Rp. " + MoneyConverter.convert(pencairanMoneyHold20));
			pencairanHold20.setPencairanMoneyWithoutFee(pencairanMoneyWithoutFeeHold20);
			pencairanHold20.setPencairanMoneyWithoutFeeInString("Rp. " + MoneyConverter.convert(pencairanMoneyWithoutFeeHold20));
			pencairanHold20.setPencairanTime("20:00");
			pencairanHold20.setPencairanType("Pencairan 3 Tertahan");
			pencairanHold20.setPencairanTypeCode("-2");
			pencairanHold20.setRiwayatPencairanDtlRincianDtos(riwayatPencairanDtlRincianDtosHold20);
			
			if(countTransaksiHold08>0) {
			allRiwayats.add(pencairanHold08);
			 if(countTransaksi08>0) {
				 allRiwayats.add(pencairan08);
			 }
			}else {
				allRiwayats.add(pencairan08);
			}
			
			
			if(countTransaksiHoldAfterHold08>0) {
			allRiwayats.add(pencairanAfterHold08);
			}
			
			
			if(countTransaksiHold16>0) {
			  allRiwayats.add(pencairanHold16);
			  if(countTransaksi16>0) {
				  allRiwayats.add(pencairan16);
			  }
			}else {
				allRiwayats.add(pencairan16);
			}
			
			if(countTransaksiHoldAfterHold16>0) {
			allRiwayats.add(pencairanAfterHold16);
			}
			
			
			if(countTransaksiHold20>0) {
			allRiwayats.add(pencairanHold20);
			if(countTransaksi20>0) {
				allRiwayats.add(pencairan20);
			}
			}else {
				allRiwayats.add(pencairan20);
			}
			
			if(countTransaksiHoldAfterHold20>0) {
			   allRiwayats.add(pencairanAfterHold20);
			}
			
////			allRiwayats.add(pencairanHold08);
//			allRiwayats.add(pencairan08);
//			allRiwayats.add(pencairan16);
//			allRiwayats.add(pencairanHold16);
//	    	allRiwayats.add(pencairanAfterHold16);
//			allRiwayats.add(pencairan20);
////			allRiwayats.add(pencairanAfterHold20);
			
			totalPencairan = totalPencairan.add(pencairanMoney08.add
									(pencairanMoney16.add(pencairanMoney20)))
									.add(pencairanMoneyAfterHold08.add(pencairanMoneyAfterHold16
											.add(pencairanMoneyAfterHold20)));

			riwayatPencairanHdrDto.setTotalPencairan(totalPencairan);
			riwayatPencairanHdrDto.setTotalPencairanInString("Rp. " + MoneyConverter.convert(totalPencairan));
			riwayatPencairanHdrDto.setAllRiwayats(allRiwayats);
			riwayatPencairanHdrDtos.add(riwayatPencairanHdrDto);
		}

		return riwayatPencairanHdrDtos;
	}
	
	@GetMapping("/getDataTransaksi/homeScreen")
	public RiwayatTransaksiHdrDto getDataTransaksiOnlyHomeScreen(@RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, @RequestParam("mid") String mid,
			@RequestParam("isLimitValidated") Boolean isLimitValidated) {
		System.out.println("start date : " + startDate + " endDate " + endDate + " mid " + mid);
		
		String rplc = "";
		
		if(!applicationProfileName.equalsIgnoreCase("prod")) {
			rplc = "_DEV";
		}
		
		System.out.println("getAllDataMidThatUpgrade using table : " + rplc);
		
		String query = "SELECT a.AUTH_SEQ_NO ,a.ISSUER_NAME,a.AUTH_DATE ,a.AUTH_TIME , " + 
				" a.AUTH_AMT, a.DATA_INP_DTTM, a.RREF_NO, a.CUSTOMER_NAME, a.MER_PAN, a.TID, a.CUST_PAN FROM dwh.TBAUAUTHQR_MV"+rplc+" a " + 
				"WHERE a.QR_TYPE = 'S' " + 
				" AND a.CUST_PAN IS NOT NULL  " + 
				" AND a.MID = '"+mid+"' " + 
				" AND a.AUTH_DATE  \n" + 
				" BETWEEN '"+startDate+"' AND '"+endDate
				+"'  ORDER BY a.AUTH_DATE DESC, a.AUTH_TIME DESC";
				
				
				System.out.println("your query : " + query);
		List<Object[]> postMaas = getMaasQuerySvc(query);

		Integer countAmt = new Integer(0);
		Integer countAmtAfterFee = new Integer(0);
		Integer countMaas = postMaas.size();
		List<RiwayatTransaksiDtlDto> riwayatTransaksiDtlDtos = new ArrayList<RiwayatTransaksiDtlDto>();

		for (Object[] obj : postMaas) {

			RiwayatTransaksiDtlDto riwayatTransaksiDtlDto = new RiwayatTransaksiDtlDto();
			BigDecimal bd = new BigDecimal(obj[4]+"");
			Integer authAmt = bd.intValue();
			
			countAmt += authAmt;
			String numeric = CharacterRemoval.makeNumber(countMaas--);
			riwayatTransaksiDtlDto.setAuthAmount("Rp " + MoneyConverter.convert(authAmt));
			riwayatTransaksiDtlDto
					.setDate(DateConverter.convertDateToAnotherDateFormat((String) obj[2], "yyyyMMdd", "dd MMM yyyy"));


			riwayatTransaksiDtlDto.setNumber(numeric);
			riwayatTransaksiDtlDto.setIssuerPayment((String) obj[1]);
			RiwayatTransaksiDtlRincianDto detail = new RiwayatTransaksiDtlRincianDto();


		
			detail.setNumber(numeric);
			detail.setAuthAmount("Rp " + MoneyConverter.convert(authAmt));
			detail.setAuthAmountNumber(new BigDecimal(authAmt));
			detail.setReffNumber((String)obj[6]);
			detail.setFeeAmount("Rp " + MoneyConverter.convert((Integer) obj[4]));
			detail.setFeeAmountNumber(new BigDecimal((Integer) obj[4]));
			detail.setIssuerName((String) obj[1]);
			detail.setAuthDateTime(
					DateConverter.convertDateToAnotherDateFormat((String) obj[2], "yyyyMMdd", "dd MMM yyyy") + " "
							+ DateConverter.convertTimeToAnotherTime((String) obj[3], ":"));
			detail.setCustomerName((String) obj[7]);
			detail.setMpan((String) obj[8]);
			detail.setTid((String) obj[9]);
			detail.setCpan((String) obj[10]);
			Integer tm = new BigDecimal(obj[4]+"").intValue();
			detail.setTransferAmount("Rp " + MoneyConverter.convert(tm));
			countAmtAfterFee += tm.intValue();
			detail.setTransferAmountNumber(new BigDecimal(tm));
			
			riwayatTransaksiDtlDto.setDetail(detail);
			riwayatTransaksiDtlDto.setFeeAmount("Rp " + MoneyConverter.convert((Integer) obj[4]));
			riwayatTransaksiDtlDto.setFeeAmountNumber(new BigDecimal((Integer) obj[4]));
			riwayatTransaksiDtlDto.setAuthAmountNumber(new BigDecimal(authAmt));
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String dateStr = sdf.format(date);
			
			if (!dateStr.equals((String) obj[2])) {
				riwayatTransaksiDtlDto.setTime(
						DateConverter.convertDateToAnotherDateFormat((String) obj[2], "yyyyMMdd", "dd MMM yyyy") + " "
								+ DateConverter.convertTimeToAnotherTime((String) obj[3], ":"));
			} else {
				riwayatTransaksiDtlDto.setTime(DateConverter.convertTimeToAnotherTime((String) obj[3], ":"));
			}
			riwayatTransaksiDtlDto.setAuthSeqNo((String) obj[0]);
			riwayatTransaksiDtlDtos.add(riwayatTransaksiDtlDto);
		}

		RiwayatTransaksiHdrDto riwayatTransaksiHdrDto = new RiwayatTransaksiHdrDto();
		riwayatTransaksiHdrDto.setCountAllTransaksi(postMaas.size());
		riwayatTransaksiHdrDto.setDetail(riwayatTransaksiDtlDtos);
		riwayatTransaksiHdrDto
				.setEndDate(DateConverter.convertDateToAnotherDateFormat(endDate, "yyyyMMdd", "dd MMM yyyy"));
		riwayatTransaksiHdrDto
				.setEndDateWithoutYear(DateConverter.convertDateToAnotherDateFormat(endDate, "yyyyMMdd", "dd MMMM"));
		riwayatTransaksiHdrDto.setMid(mid);
		riwayatTransaksiHdrDto
				.setStartDate(DateConverter.convertDateToAnotherDateFormat(startDate, "yyyyMMdd", "dd MMM yyyy"));
		riwayatTransaksiHdrDto.setStartDateWithoutYear(
				DateConverter.convertDateToAnotherDateFormat(startDate, "yyyyMMdd", "dd MMMM"));
		riwayatTransaksiHdrDto.setSumAllTransaksi("Rp " + MoneyConverter.convert(countAmt));
		riwayatTransaksiHdrDto.setSumAllTransaksiAfterFee("Rp " + MoneyConverter.convert(countAmtAfterFee));
		return riwayatTransaksiHdrDto;
	}

	@GetMapping("/getDataTransaksi")
	public RiwayatTransaksiHdrDto getDataTransaksiByFilter(@RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, @RequestParam("mid") String mid,
			@RequestParam("isLimitValidated") Boolean isLimitValidated) {
		System.out.println("start date : " + startDate + " endDate " + endDate + " mid " + mid);
		
		String rplc = "";
		
		if(!applicationProfileName.equalsIgnoreCase("prod")) {
			rplc = "_DEV";
		}
		
		System.out.println("getAllDataMidThatUpgrade using table : " + rplc);
		
		String query = " SELECT a.*, "
				+ " CASE WHEN a.pymt_date_time <= TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') AND IS_HOLD != 1 " + // 26
				" THEN 1 " + " ELSE 0 " + " END is_cair " + " FROM ( SELECT " + " A.AUTH_SEQ_NO," + // 0
				" a.MID , " // 1
				+ " a.asnd_mcnt_name," // 2
				+ "  tm.MER_FEE ," // 3
				+ "  a.AUTH_AMT, " // 4
				+ " tm.PMT_AMT  ," + // 5
				" a.AUTH_DATE " // 6
				+ " , a.AUTH_TIME " // 7
				+ " ,a.CUST_PAN  ," // 8
				+ " a.ISSUER_NAME, " // 9
				+ " a.ISSUER_ID, " // 10
				+ "  a.QR_TYPE" + // 11
				", a.CUSTOMER_NAME, " // 12
				+ "  a.RREF_NO, " // 13
				+ "  a.TID, " // 14
				+ "  a.MER_PAN, " + // 15
				" tm.MER_FEE_RT " // 16
				+ ",  tm.POSTNG_DATE " // 17
				+ ", tm.SALE_CONF_DTTM, " + // 18
				" tm.PMT_DATE, " + // 19
//				"  CASE" + 
//				" WHEN tm.AUTH_TIME BETWEEN '060000' AND '135900' " + 
//				" THEN a.AUTH_DATE " + 
//				" WHEN tm.AUTH_TIME BETWEEN '140000' AND '175900' " + 
//				" THEN a.AUTH_DATE" + 
//				" WHEN tm.AUTH_TIME BETWEEN '180000' AND '235999' " + 
//				" THEN TO_CHAR(to_date(tm.AUTH_DATE,'YYYYMMDD')+1,'YYYYMMDD') " + 
//				" WHEN tm.AUTH_TIME BETWEEN '000000' AND '055900' " + 
//				" THEN a.AUTH_DATE " + 
//				" END pmt_date, " +  //19
//				" CASE " + 
//				" WHEN tm.AUTH_TIME BETWEEN '060000' AND '135900'" + 
//				" THEN concat(a.AUTH_DATE,'160000') " + 
//				" WHEN tm.AUTH_TIME BETWEEN '140000' AND '175900'" + 
//				" THEN concat(a.AUTH_DATE,'200000') " + 
//				" WHEN tm.AUTH_TIME BETWEEN '180000' AND '235999'" + 
//				" THEN concat(TO_CHAR(to_date(tm.AUTH_DATE,'YYYYMMDD')+1,'YYYYMMDD'),'080000' )" + 
//				" WHEN tm.AUTH_TIME BETWEEN '000000' AND '055900'" + 
//				" THEN concat(a.AUTH_DATE,'080000') " + 
//				" END pymt_date_time," + //20
				" CASE " + " WHEN tm.AUTH_TIME BETWEEN '060000' AND '135900'" + " THEN concat(tm.PMT_DATE,'160000') "
				+ " WHEN tm.AUTH_TIME BETWEEN '140000' AND '175900'" + " THEN concat(tm.PMT_DATE,'200000') "
				+ " WHEN tm.AUTH_TIME BETWEEN '180000' AND '235999'" + " THEN concat(tm.PMT_DATE,'080000' ) "
				+ " WHEN tm.AUTH_TIME BETWEEN '000000' AND '055900'" + " THEN concat(tm.PMT_DATE,'080000') "
				+ " END pymt_date_time," + // 20
				" CASE" + " WHEN tm.AUTH_TIME BETWEEN '060000' AND '135900'" + " THEN 2 "
				+ " WHEN tm.AUTH_TIME BETWEEN '140000' AND '175900'" + " THEN 3 "
				+ " WHEN tm.AUTH_TIME BETWEEN '180000' AND '235999'" + " THEN 1 "
				+ " WHEN tm.AUTH_TIME BETWEEN '000000' AND '055900'" + " THEN 1" + " END batch_ke," + // 21
				" CASE" + " WHEN tm.AUTH_TIME BETWEEN '060000' AND '135900'" + " THEN '16:00' "
				+ " WHEN tm.AUTH_TIME BETWEEN '140000' AND '175900'" + " THEN '20:00' "
				+ " WHEN tm.AUTH_TIME BETWEEN '180000' AND '235999'" + " THEN '08:00' "
				+ " WHEN tm.AUTH_TIME BETWEEN '000000' AND '055900'" + " THEN '08:00'" + " END jam_pembayaran, " + // 22
				" CASE " + " WHEN tm.PAID_STATUS = 'N' THEN 1 " + " ELSE 0 " + " END is_hold, " + // 23
				" tmd.pmt_hold_req_date, " + // 24
				" tmd.RLSE_DATE, " + // 25
				" CASE  \n" + 
				" WHEN  concat(TO_CHAR(tmd.DATA_CHNG_DTTM, 'HH24'),'0000') BETWEEN '080001' AND '160000' THEN '16:00'\n" + 
				" WHEN  concat(TO_CHAR(tmd.DATA_CHNG_DTTM, 'HH24'),'0000') BETWEEN '160001' AND '200000' THEN '20:00'\n" + 
				" WHEN  concat(TO_CHAR(tmd.DATA_CHNG_DTTM, 'HH24'),'0000') BETWEEN '200001' AND '235959' THEN '08:00'\n" + 
				" WHEN  concat(TO_CHAR(tmd.DATA_CHNG_DTTM, 'HH24'),'0000') BETWEEN '000000' AND '080000' THEN '08:00'\n" + 
				" END hold_rlse_time " + //26 
				" FROM dwh.TBAUAUTHQR_MV"+rplc+"  a" + " INNER JOIN dwh.TBACSALESQRMAIN_MV"+rplc+" tm "
				+ " ON a.AUTH_SEQ_NO = tm.AUTH_SEQ_NO " + " LEFT JOIN dwh.TBMCPMTHOLDREQCTNT_MV"+rplc+" tmd  "
				+ " ON tm.PMT_HOLD_SEQ_NO = tmd.PMT_HOLD_SEQ_NO " + " WHERE a.QR_TYPE = 'S'" +
//				" AND " + 
//				" a.MID IN (" + 
//				" SELECT mid FROM dwh.tbmcqrmain_dev" + 
//				" where" + 
//				" qr_knd_cd= '01'" + 
//				" AND qr_cond_cd = '02') " +
				" AND a.CUST_PAN IS NOT NULL " + " AND a.MID = '" + mid + "'" + " AND a.AUTH_DATE " + " BETWEEN '"
				+ startDate + "' AND '" + endDate + "' " + " ORDER BY a.AUTH_DATE DESC, a.AUTH_TIME DESC)a ";
		System.out.println(" query untuk transaksi : " + query);
		List<Object[]> postMaas = getMaasQuerySvc(query);

		Integer countAmt = new Integer(0);
		Integer countAmtAfterFee = new Integer(0);
		Integer countMaas = postMaas.size();
		List<RiwayatTransaksiDtlDto> riwayatTransaksiDtlDtos = new ArrayList<RiwayatTransaksiDtlDto>();

		for (Object[] obj : postMaas) {

			RiwayatTransaksiDtlDto riwayatTransaksiDtlDto = new RiwayatTransaksiDtlDto();
			BigDecimal bd = new BigDecimal(obj[4]+"");
			Integer authAmt = bd.intValue();
			
			countAmt += authAmt;
			String numeric = CharacterRemoval.makeNumber(countMaas--);
			riwayatTransaksiDtlDto.setAuthAmount("Rp " + MoneyConverter.convert(authAmt));
			riwayatTransaksiDtlDto
					.setDate(DateConverter.convertDateToAnotherDateFormat((String) obj[6], "yyyyMMdd", "dd MMM yyyy"));

			Boolean isTransfer = ((Integer) obj[27]) == 1 ? true : false;
			Boolean isHold = ((Integer) obj[23]) == 1 ? true : false;
			riwayatTransaksiDtlDto.setPmtHoldDate((String) obj[24]);
			if ((String) obj[24] != null) // agar bisa sama kalau hold date nya null maka timenya juga null
			{
				riwayatTransaksiDtlDto.setPmtHoldTime((String) obj[22]);
			}

			riwayatTransaksiDtlDto.setReleaseHoldDate((String) obj[25]);
//			if ((String) obj[25] != null) // agar bisa sama kalau releade date nya null maka timenya juga null
//			{
//				riwayatTransaksiDtlDto.setReleaseHoldTime("16:00");
//			}

			riwayatTransaksiDtlDto.setIsHold(isHold);

			riwayatTransaksiDtlDto.setNumber(numeric);
			RiwayatTransaksiDtlRincianDto detail = new RiwayatTransaksiDtlRincianDto();

			if (!isTransfer) {
				riwayatTransaksiDtlDto.setIsTransferToRek(false);
				detail.setIsTransferToRek(false);

			} else {
				detail.setIsTransferToRek(true);
				riwayatTransaksiDtlDto.setIsTransferToRek(true);
				
				//validasi untuk cek time pembayaran jika kenak hold
				if(obj[24]!=null) {
					String tp = (String) obj[26];
					riwayatTransaksiDtlDto.setTimePembayaran(tp);
					riwayatTransaksiDtlDto.setReleaseHoldTime(tp);
					riwayatTransaksiDtlDto.setPmtDate((String) obj[25]);
				}else {
				riwayatTransaksiDtlDto.setTimePembayaran((String) obj[22]);
				riwayatTransaksiDtlDto.setPmtDate((String) obj[19]);
				}
			}

		
			detail.setNumber(numeric);
			detail.setAuthAmount("Rp " + MoneyConverter.convert(authAmt));
			detail.setAuthAmountNumber(new BigDecimal(authAmt));
			detail.setCpan((String) obj[8]);
			detail.setCustomerName((String) obj[12]);
			detail.setFeeAmount("Rp " + MoneyConverter.convert((Integer) obj[3]));
			detail.setFeeAmountNumber(new BigDecimal((Integer) obj[3]));
			detail.setIssuerName((String) obj[9]);
			detail.setAuthDateTime(
					DateConverter.convertDateToAnotherDateFormat((String) obj[6], "yyyyMMdd", "dd MMM yyyy") + " "
							+ DateConverter.convertTimeToAnotherTime((String) obj[7], ":"));
			detail.setSettleDate((String) obj[17]);

			detail.setMpan((String) obj[15]);
			detail.setPercentageFeeAmount(obj[16] + "%");
			detail.setPercentageFeeAmountNumber(obj[16] + "");
			detail.setReffNumber((String) obj[13]);
			detail.setTid((String) obj[14]);
			detail.setTimeDataChange((String) obj[18]);
			Integer tm = new BigDecimal(obj[5]+"").intValue();
			detail.setTransferAmount("Rp " + MoneyConverter.convert(tm));
			countAmtAfterFee += tm.intValue();
			detail.setTransferAmountNumber(new BigDecimal(tm));
			riwayatTransaksiDtlDto.setDetail(detail);
			riwayatTransaksiDtlDto.setFeeAmount("Rp " + MoneyConverter.convert((Integer) obj[3]));
			riwayatTransaksiDtlDto.setFeeAmountNumber(new BigDecimal((Integer) obj[3]));
			riwayatTransaksiDtlDto.setIssuerPayment((String) obj[9]);
			riwayatTransaksiDtlDto.setCpanNumber((String) obj[8]);
			riwayatTransaksiDtlDto.setAuthAmountNumber(new BigDecimal(authAmt));
			riwayatTransaksiDtlDto.setCpanNumberInMask(CharacterRemoval.maskingValueCpan((String) obj[8]));
			if (isLimitValidated) {
				riwayatTransaksiDtlDto.setTime(
						DateConverter.convertDateToAnotherDateFormat((String) obj[6], "yyyyMMdd", "dd MMM yyyy") + ", "
								+ DateConverter.convertTimeToAnotherTime((String) obj[7], ":"));
			} else {
				riwayatTransaksiDtlDto.setTime(DateConverter.convertTimeToAnotherTime((String) obj[7], ":"));
			}
			riwayatTransaksiDtlDto.setAuthSeqNo((String) obj[0]);
			riwayatTransaksiDtlDtos.add(riwayatTransaksiDtlDto);
		}

		RiwayatTransaksiHdrDto riwayatTransaksiHdrDto = new RiwayatTransaksiHdrDto();
		riwayatTransaksiHdrDto.setCountAllTransaksi(postMaas.size());
		riwayatTransaksiHdrDto.setDetail(riwayatTransaksiDtlDtos);
		riwayatTransaksiHdrDto
				.setEndDate(DateConverter.convertDateToAnotherDateFormat(endDate, "yyyyMMdd", "dd MMM yyyy"));
		riwayatTransaksiHdrDto
				.setEndDateWithoutYear(DateConverter.convertDateToAnotherDateFormat(endDate, "yyyyMMdd", "dd MMMM"));
		riwayatTransaksiHdrDto.setMid(mid);
		riwayatTransaksiHdrDto
				.setStartDate(DateConverter.convertDateToAnotherDateFormat(startDate, "yyyyMMdd", "dd MMM yyyy"));
		riwayatTransaksiHdrDto.setStartDateWithoutYear(
				DateConverter.convertDateToAnotherDateFormat(startDate, "yyyyMMdd", "dd MMMM"));
		riwayatTransaksiHdrDto.setSumAllTransaksi("Rp " + MoneyConverter.convert(countAmt));
		riwayatTransaksiHdrDto.setSumAllTransaksiAfterFee("Rp " + MoneyConverter.convert(countAmtAfterFee));
		return riwayatTransaksiHdrDto;
	}

	public static final Integer BATCH_1 = 1;
	public static final Integer BATCH_2 = 2;
	public static final Integer BATCH_3 = 3;

	public Integer getBatchNumber(String hour) {
		if (DateValidation.checkBetweenHour(hour, "18:00", "23:59")) {
			return BATCH_1;
		} else if (DateValidation.checkBetweenHour(hour, "00:00", "05:59")) {
			return BATCH_1;
		} else if (DateValidation.checkBetweenHour(hour, "06:00", "13:59")) {
			return BATCH_2;
		} else if (DateValidation.checkBetweenHour(hour, "14:00", "17.59")) {
			return BATCH_3;
		} else {
			throw new NotAcceptanceDataException("Mohon pastikan format jam hh:mm");
		}
	}


	@GetMapping("/getDataTransaksi/auth")
	public ResponseEntity<Object> refresh(@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate,
			@RequestParam(value = "isLimitValidated", required = true) Boolean isLimitValidated,
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken) {

		if (isLimitValidated) {
			String newDateString = new SimpleDateFormat("yyyyMMdd").format(new Date());
			Long getDiffDaysBet = DateValidation.getDifferentDays(startDate, endDate);
			Long getDiffDay90DaysEnd = DateValidation.getDifferentDays(endDate, newDateString);
			Long getDiffDay90DaysStart = DateValidation.getDifferentDays(startDate, newDateString);
			System.out.println("get diffDays : " + getDiffDaysBet);
			
			if(getDiffDaysBet<0) {
				throw new LengthValidationException("Difference Start date and End date is minus");
			}
			else if(getDiffDay90DaysEnd>90 || getDiffDay90DaysStart>90) {
				throw new NotAcceptanceDataException("Difference Start Date : " + getDiffDay90DaysStart
						+ " or End Date : " + getDiffDay90DaysEnd);
			}
			else if (getDiffDaysBet> 7) {
				throw new SizeDataOverCapacityException("Difference Start Date And End Date : " + getDiffDaysBet);
			}
		}

		AuthorizedResponseFirst(secretKey, secretId);
		AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken(secretToken, secretKey);
		String mid = allUserOobDtlRepo.getUserOobMidDtlTableDto().getMidNumber();
		RiwayatTransaksiHdrDto riwayatTransaksiHdrDto = getDataTransaksiByFilter(startDate, endDate, mid,
				isLimitValidated);

		if (startDate.equalsIgnoreCase(DateConverter.convertDateToString(new Date(), "yyyyMMdd"))
				&& endDate.equalsIgnoreCase(DateConverter.convertDateToString(new Date(), "yyyyMMdd"))) {
			for (RiwayatTransaksiDtlDto riwayatPencairan : riwayatTransaksiHdrDto.getDetail()) {
				// conditional untuk add new date() --start
				UserNotifDtlTable userNotifDtlTable = new UserNotifDtlTable();
				userNotifDtlTable.setUniqueTick(allUserOobDtlRepo.getUserOobTableSuccessDto().getId()+"-"+riwayatPencairan.getAuthSeqNo() + TICK_CODE_TRX);
				userNotifDtlTable.setNotification("Transaksi");
				userNotifDtlTable.setIdOob(allUserOobDtlRepo.getUserOobMidDtlTableDto().getIdOob());
				userNotifDtlTable.setCreateDate(DateConverter
						.convertStringToDate(riwayatPencairan.getDetail().getAuthDateTime(), "dd MMM yyyy HH:mm"));
				userNotifDtlTable.setHeader("Pembayaran dari " + riwayatPencairan.getIssuerPayment());
				userNotifDtlTable.setBody("Transaksi Sebesar " + riwayatPencairan.getAuthAmount()
						+ " telah berhasil dari " + riwayatPencairan.getIssuerPayment() + ", Kode Ref. "
						+ riwayatPencairan.getDetail().getReffNumber());
				userOobLogicSvc.createNotification(userNotifDtlTable);
			}
		}

		riwayatTransaksiHdrDto.setIdOob(allUserOobDtlRepo.getUserOobMidDtlTableDto().getIdOob());
		return new ResponseEntity<Object>(new SuccessValidation("Data Sukses Retrieve", riwayatTransaksiHdrDto),
				HttpStatus.OK);
	}
	
	
	@GetMapping("/getDataTransaksi/auth/homeScreen")
	public ResponseEntity<Object> getDataTransaksiHomeScreen(@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate,
			@RequestParam(value = "isLimitValidated", required = true) Boolean isLimitValidated,
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken) {

		if (isLimitValidated) {
			String newDateString = new SimpleDateFormat("yyyyMMdd").format(new Date());
			Long getDiffDaysBet = DateValidation.getDifferentDays(startDate, endDate);
			Long getDiffDay90DaysEnd = DateValidation.getDifferentDays(endDate, newDateString);
			Long getDiffDay90DaysStart = DateValidation.getDifferentDays(startDate, newDateString);
			System.out.println("get diffDays : " + getDiffDaysBet);
			
			if(getDiffDaysBet<0) {
				throw new LengthValidationException("Difference Start date and End date is minus");
			}
			else if(getDiffDay90DaysEnd>90 || getDiffDay90DaysStart>90) {
				throw new NotAcceptanceDataException("Difference Start Date : " + getDiffDay90DaysStart
						+ " or End Date : " + getDiffDay90DaysEnd);
			}
			else if (getDiffDaysBet> 7) {
				throw new SizeDataOverCapacityException("Difference Start Date And End Date : " + getDiffDaysBet);
			}
		}

		AuthorizedResponseFirst(secretKey, secretId);
		AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken(secretToken, secretKey);
		String mid = allUserOobDtlRepo.getUserOobMidDtlTableDto().getMidNumber();
		RiwayatTransaksiHdrDto riwayatTransaksiHdrDto = getDataTransaksiOnlyHomeScreen(startDate, endDate, mid,
				isLimitValidated);

		if (startDate.equalsIgnoreCase(DateConverter.convertDateToString(new Date(), "yyyyMMdd"))
				&& endDate.equalsIgnoreCase(DateConverter.convertDateToString(new Date(), "yyyyMMdd"))) {
			for (RiwayatTransaksiDtlDto riwayatPencairan : riwayatTransaksiHdrDto.getDetail()) {
				// conditional untuk add new date() --start
				UserNotifDtlTable userNotifDtlTable = new UserNotifDtlTable();
				userNotifDtlTable.setUniqueTick(allUserOobDtlRepo.getUserOobTableSuccessDto().getId()+"-"+riwayatPencairan.getAuthSeqNo() + TICK_CODE_TRX);
				userNotifDtlTable.setNotification("Transaksi");
				userNotifDtlTable.setIdOob(allUserOobDtlRepo.getUserOobMidDtlTableDto().getIdOob());
				userNotifDtlTable.setCreateDate(DateConverter
						.convertStringToDate(riwayatPencairan.getDetail().getAuthDateTime(), "dd MMM yyyy HH:mm"));
				userNotifDtlTable.setHeader("Pembayaran dari " + riwayatPencairan.getIssuerPayment());
				userNotifDtlTable.setBody("Transaksi Sebesar " + riwayatPencairan.getAuthAmount()
						+ " telah berhasil dari " + riwayatPencairan.getIssuerPayment() + ", Kode Ref. "
						+ riwayatPencairan.getDetail().getReffNumber());
				userOobLogicSvc.createNotification(userNotifDtlTable);
			}
		}

		riwayatTransaksiHdrDto.setIdOob(allUserOobDtlRepo.getUserOobMidDtlTableDto().getIdOob());
		return new ResponseEntity<Object>(new SuccessValidation("Data Sukses Retrieve", riwayatTransaksiHdrDto),
				HttpStatus.OK);
	}

}

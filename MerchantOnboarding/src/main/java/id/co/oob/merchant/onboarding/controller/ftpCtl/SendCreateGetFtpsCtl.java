package id.co.oob.merchant.onboarding.controller.ftpCtl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.opencsv.CSVReader;

import id.co.oob.db.qris.merchant.onboarding.dao.user.UserOobMidDtlTableDao;
import id.co.oob.db.qris.merchant.onboarding.dao.user.UserOobTableDao;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobMidDtlTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableAuthDtl;
import id.co.oob.db.qris.merchant.onboarding.service.authentication.AllTheAuthenticationCircuitSvc;
import id.co.oob.db.qris.merchant.onboarding.service.user.UserOobLogicSvc;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableUploadFileDto;
import id.co.oob.lib.common.merchant.onboarding.email.EmailBody;
import id.co.oob.lib.common.merchant.onboarding.encryptor.MaskingNumber;
import id.co.oob.lib.common.merchant.onboarding.file.Base64FileDto;
import id.co.oob.lib.common.merchant.onboarding.file.LogStatusEmailDto;
import id.co.oob.lib.common.merchant.onboarding.file.SftpConnection;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.merchant.onboarding.controller.BaseCtl;
import id.co.oob.merchant.onboarding.controller.user.RegistrasiUserOobCtl;

@RestController
@RequestMapping("/sendCreateGetFtpsCtl")
public class SendCreateGetFtpsCtl extends BaseCtl {

	@Autowired
	private AllTheAuthenticationCircuitSvc allTheAuthenticationCircuitSvc;


	@Autowired
	private UserOobMidDtlTableDao userOobMidDtlTableDao;
	
	@Autowired
	private UserOobTableDao userOobTableDao;

	@Autowired
	private UserOobLogicSvc userOobLoginSvc;

	@Autowired
	private RegistrasiUserOobCtl registrasiUserOobCtl;

	@PostMapping("/getQrPictureByPathWs")
	public InputStream getQrPictureByPathWs(@RequestBody Map<String, String> mappo) {
		String pathName = mappo.get("pathName");
		String nmid = mappo.get("nmid");
		String newPath = pathName;
		ChannelSftp channelSftp = SftpConnection.setupJsch();
		try {
			channelSftp.cd(newPath);
		} catch (SftpException e) {
			e.printStackTrace();
			throw new InternalServerErrorException(newPath + " tidak dapat di cd");
		}

		try {
			Vector fileList = channelSftp.ls(newPath);
			for (int i = 0; i < fileList.size(); i++) {
				LsEntry entry = (LsEntry) fileList.get(i);
				String fileName = entry.getFilename();

				// untuk cek bahwa file name itu contains data yang diinginkan,
				// if(fileName.contains(pathname)) {
				if (fileName.startsWith(nmid)) {
					newPath = fileName;
				}
			}
		} catch (SftpException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		InputStream inputStream = null;
		System.err.println("path name yang terpilih " + newPath);
		try {
			inputStream = channelSftp.get(newPath);
			return inputStream;
		} catch (SftpException e) {
			e.printStackTrace();
			throw new InternalServerErrorException("path name : " + newPath);
		}

	}

	public InputStream getQrPictureByPath(String pathName, String nmid) {
		String newPath = pathName;
		ChannelSftp channelSftp = SftpConnection.setupJsch();
		try {
			System.out.println("sedang cd : " + newPath);
			channelSftp.cd(newPath);
		} catch (SftpException e) {
			e.printStackTrace();
			throw new InternalServerErrorException(newPath + " tidak dapat di cd");
		}

		try {
			Vector fileList = channelSftp.ls(newPath);
			for (int i = 0; i < fileList.size(); i++) {
				LsEntry entry = (LsEntry) fileList.get(i);
				String fileName = entry.getFilename();

				// untuk cek bahwa file name itu contains data yang diinginkan,
				// if(fileName.contains(pathname)) {
				if (fileName.startsWith(nmid)) {
					newPath = fileName;
				}
			}
		} catch (SftpException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		InputStream inputStream = null;
		System.err.println("path name yang terpilih " + newPath);
		try {
			inputStream = channelSftp.get(newPath);
			return inputStream;
		} catch (SftpException e) {
			e.printStackTrace();
			throw new InternalServerErrorException("path name : " + newPath);
		}

	}
	
	public InputStream getQrPictureByPathV2(String pathName, String nmid) {
		String newPath = pathName;
		ChannelSftp channelSftp = SftpConnection.setupJsch();
		try {
			System.out.println("sedang cd : " + newPath);
			channelSftp.cd(newPath);
		} catch (SftpException e) {
			return null;
		}

		try {
			Vector fileList = channelSftp.ls(newPath);
			for (int i = 0; i < fileList.size(); i++) {
				LsEntry entry = (LsEntry) fileList.get(i);
				String fileName = entry.getFilename();

				// untuk cek bahwa file name itu contains data yang diinginkan,
				// if(fileName.contains(pathname)) {
				if (fileName.startsWith(nmid)) {
					newPath = fileName;
				}
			}
		} catch (SftpException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		InputStream inputStream = null;
		System.err.println("path name yang terpilih " + newPath);
		try {
			inputStream = channelSftp.get(newPath);
			return inputStream;
		} catch (SftpException e) {
			e.printStackTrace();
			throw new InternalServerErrorException("path name : " + newPath);
		}

	}

	public ZipFile getFile(String pathName, String fileName) {
		File file = new File("oob-dummy");
		try {
			FileUtils.copyInputStreamToFile(getQrPictureByPath(pathName, fileName), file);
			return new ZipFile(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Error To Convert To Zip");
		}
	}
	
	public ZipFile getFileV2(String pathName, String fileName) {
		File file = new File("oob-dummy");
		try {
			
			InputStream is = getQrPictureByPathV2(pathName, fileName);
			if(is==null) {
				return null;
			}
			FileUtils.copyInputStreamToFile(is, file);
			return new ZipFile(file);
		} catch (IOException e) {
			return null;
		}
	}
	
	@GetMapping("/updateTo2FromSftp")
	@Scheduled(cron = "0 0 21 * * ?")
	public String updateTo2FromSftp() {
		DateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String currentDateTime = dateFormatter.format(new Date());
		String headerValue = "users_" + currentDateTime + ".csv";
		String pathDate = sftpPathFile + currentDateTime+"/oob";
		Map<Integer, List<String>> getRenderInMap = getFileFromFtpsOnlyQrStaticForCsv(pathDate, headerValue);
		List<Integer> datasId = new ArrayList<Integer>();
		for (Entry<Integer, List<String>> map : getRenderInMap.entrySet()) {
			System.out.println("datanya : " + map.getValue().get(0));
			
			Integer i = null;
			try {
					i = Integer.valueOf(map.getValue().get(0));
			}catch(NumberFormatException nex) {
				
			}
			if(i!=null)
			datasId.add(i);
		}
		
		for (Integer integer : datasId) {
			registrasiUserOobCtl.updateTo2(Integer.toUnsignedLong(integer), "YRuYy-dSiaK-LLdWA", "4Vbxc-uoGWA-LVYg5");
		}
		return new Gson().toJson(datasId);
	}
	
	@PostMapping("/updateTo2FromSftpPost")
	public String updateTo2FromSftp(@RequestBody String currentDateTime) {
		String headerValue = "users_" + currentDateTime + ".csv";
		String pathDate = sftpPathFile + currentDateTime+"/oob";
		Map<Integer, List<String>> getRenderInMap = getFileFromFtpsOnlyQrStaticForCsv(pathDate, headerValue);
		List<Integer> datasId = new ArrayList<Integer>();
		for (Entry<Integer, List<String>> map : getRenderInMap.entrySet()) {
			System.out.println("datanya : " + map.getValue().get(0));
			
			Integer i = null;
			try {
					i = Integer.valueOf(map.getValue().get(0));
			}catch(NumberFormatException nex) {
				
			}
			if(i!=null)
			datasId.add(i);
		}
		
		for (Integer integer : datasId) {
			registrasiUserOobCtl.updateTo2(Integer.toUnsignedLong(integer), "YRuYy-dSiaK-LLdWA", "4Vbxc-uoGWA-LVYg5");
		}
		return new Gson().toJson(datasId);
	}
	
	
	public InputStream getFileFromSomePathNew(String pathCd, String pathname) {
		System.out.println(
				"start masuk ke getFileFromFtpsOnlyQrStaticForCsv di cd " + pathCd + " dan path name " + pathname);
		ChannelSftp channelSftp = SftpConnection.setupJsch();

		try {
			channelSftp.cd(pathCd);
		} catch (SftpException e) {
			System.err.println("no such file for " + pathCd + "/" + pathname);
			return null;
		}

		try {
			Vector fileList = channelSftp.ls(pathCd);
			for (int i = 0; i < fileList.size(); i++) {
				LsEntry entry = (LsEntry) fileList.get(i);
				String fileName = entry.getFilename();

				// untuk cek bahwa file name itu contains data yang diinginkan,
				// if(fileName.contains(pathname)) {
				if (fileName.startsWith(pathname)) {
					pathname = fileName;
				}
			}
		} catch (SftpException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return null;
		}
		InputStream inputStream = null;
		System.err.println("path name yang terpilih " + pathname);
		try {
			inputStream = channelSftp.get(pathname);
		} catch (SftpException e) {
			System.out.println("pathname : " + pathname + " tidak tersedia.");
			//e.printStackTrace();
			return null;
		}
		return inputStream;
	}

	@Deprecated
	public InputStream getFileFromSomePath(String pathCd, String pathname) {
		System.out.println(
				"start masuk ke getFileFromFtpsOnlyQrStaticForCsv di cd " + pathCd + " dan path name " + pathname);
		ChannelSftp channelSftp = SftpConnection.setupJsch();

		try {
			channelSftp.cd(pathCd);
		} catch (SftpException e) {
			e.printStackTrace();
			throw new InternalServerErrorException(pathCd + " tidak dapat di cd");
		}

		try {
			Vector fileList = channelSftp.ls(pathCd);
			for (int i = 0; i < fileList.size(); i++) {
				LsEntry entry = (LsEntry) fileList.get(i);
				String fileName = entry.getFilename();

				// untuk cek bahwa file name itu contains data yang diinginkan,
				// if(fileName.contains(pathname)) {
				if (fileName.startsWith(pathname)) {
					pathname = fileName;
				}
			}
		} catch (SftpException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InputStream inputStream = null;
		System.err.println("path name yang terpilih " + pathname);
		try {
			inputStream = channelSftp.get(pathname);
		} catch (SftpException e) {
			e.printStackTrace();
			throw new InternalServerErrorException("path name : " + pathname + " tidak ada di " + pathCd);
		}
		return inputStream;
	}
	
	

	@SuppressWarnings("deprecation")
	public Map<Integer, List<String>> getFileFromFtps(String pathCd, String pathname) {

		Map<Integer, List<String>> data = new HashMap<>();
		try {
			Workbook workbook = new XSSFWorkbook(getFileFromSomePathNew(pathCd, pathname));
			Sheet sheet = workbook.getSheetAt(0);

			int i = 0;
			for (Row row : sheet) {
				data.put(i, new ArrayList<String>());
				for (Cell cell : row) {
					switch (cell.getCellTypeEnum()) {
					case STRING:
						data.get(new Integer(i)).add(cell.getRichStringCellValue().getString());
						break;
					case NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							data.get(i).add(cell.getDateCellValue() + "");
						} else {
							data.get(i).add(cell.getNumericCellValue() + "");
						}
						break;
					case BOOLEAN:
						data.get(i).add(cell.getBooleanCellValue() + "");
						break;
					case FORMULA:
						data.get(i).add(cell.getCellFormula() + "");
						break;
					default:
						data.get(new Integer(i)).add(" ");
					}
				}
				i++;
			}
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new InternalServerErrorException("tidak bisa format XSFFWorkbook ");
		}

		return data;
	}

	@SuppressWarnings("deprecation")
	public Map<Integer, List<String>> getFileFromFtpsOnlyQrStaticForCsv(String pathCd, String pathname) {
		InputStream is = getFileFromSomePathNew(pathCd, pathname);
		Map<Integer, List<String>> data = new HashMap<>();
		
		if(is==null) {
			return new HashMap<Integer, List<String>>();
		}
		
		CSVReader csvReader = new CSVReader(new InputStreamReader(is));
		System.out.println(
				"start masuk ke getFileFromFtpsOnlyQrStaticForCsv di cd " + pathCd + " dan path name " + pathname);
		String[] record;
		try {

			int i = 0;
			while ((record = csvReader.readNext()) != null) {
				data.put(i, new ArrayList<String>());
				if (record.length > 0) {
					for (String str : record) {
						String[] split = str.split("\\|");
						for (String sdd : split) {
							data.get(new Integer(i)).add(sdd);
						}

					}

				}
				i++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException(e.getLocalizedMessage());
		}
		System.out.println("return data sukses...");
		return data;
	}

	@SuppressWarnings("deprecation")
	public Map<Integer, List<String>> getFileFromFtpsOnlyQrStaticForExcel(String pathCd, String pathname) {

		Map<Integer, List<String>> data = new HashMap<>();
		try {
			Workbook workbook = new XSSFWorkbook(getFileFromSomePathNew(pathCd, pathname));
			Sheet sheet = workbook.getSheetAt(0);

			int i = 0;
			for (Row row : sheet) {
				data.put(i, new ArrayList<String>());
				for (Cell cell : row) {
					if (row.getCell(13) == null || !row.getCell(13).getStringCellValue().equals("S")) {
						data.remove(i);
						i--;
						break;
					}
					switch (cell.getCellTypeEnum()) {
					case STRING:
						data.get(new Integer(i)).add(cell.getRichStringCellValue().getString());
						break;
					case NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							data.get(i).add(cell.getDateCellValue() + "");
						} else {
							data.get(i).add(cell.getNumericCellValue() + "");
						}
						break;
					case BOOLEAN:
						data.get(i).add(cell.getBooleanCellValue() + "");
						break;
					case FORMULA:
						data.get(i).add(cell.getCellFormula() + "");
						break;
					default:
						data.get(new Integer(i)).add(" ");
					}
				}
				i++;
			}
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new InternalServerErrorException("tidak bisa format XSFFWorkbook ");
		}

		return data;
	}

	@GetMapping("/renderDataInMap")
	public Map<Integer, List<String>> getRenderInMap(@RequestParam("dateNowInString") String dateNowInString) {
		// String dateNowInString = DateConverter.convertDateToString(new Date(),
		// "yyyyMMdd");
		System.out.println("start masuk ke renderInMap");
		return getFileFromFtpsOnlyQrStaticForCsv(sftpPathFile+ dateNowInString + "/pten", "pten_");
	}
	

	
	@GetMapping("/getData0Kb")
	@Scheduled(cron = "0 0 1 * * ?")
	public ResponseEntity<Object> getData0Kb(){
		List<UserOobMidDtlTable> tables = userOobMidDtlTableDao.getAllMid();
		List<Map<String, Object>> maps = new ArrayList<Map<String,Object>>();
		for (UserOobMidDtlTable table : tables) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("urlPict", table.getPathQrNumber());
			map.put("idOob", table.getIdOob());
			System.out.println("cek idOob : " +table.getIdOob());
			System.out.println("cek table.getPathQrNumber() : " + table.getPathQrNumber());
			
			if(table.getPathQrNumber()==null) {
				continue;
			}
			InputStream isKtp = getInputStreamAliOss(table.getPathQrNumber());
			
			byte[] bytes = null;
			try {
				 bytes = IOUtils.toByteArray(isKtp);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(bytes==null) {
			map.put("sizeFile", bytes);
			}
			else {
				if(bytes.length!=0) {
					continue;
				}
				
				String nmid = table.getNmidNumber();
				Date createDate = table.getCreateDate();
			
				 String dateNowInString = DateConverter.convertDateToString(createDate, "yyyyMMdd");
					
					
					List<String> dateCaliberator = new ArrayList<String>();
					dateCaliberator.add(dateNowInString);
				    String whereTheZip = "";
					for(int i = 1 ; i<=8; i++) {
					    Calendar calendar = Calendar.getInstance();
					    calendar.setTime(createDate);
					    calendar.add(Calendar.DATE, (i));
					    Date yesterday = calendar.getTime();
					    String date1DayBeforeInString = DateConverter.convertDateToString(yesterday, "yyyyMMdd");
					    dateCaliberator.add(date1DayBeforeInString);
					}
			
					InputStream inputStream = null;
				
				for (String date : dateCaliberator) {
					ZipFile zipFile = getFileV2(sftpPathFile + date + "/pten", nmid + ".zip");
					if(zipFile==null) {
						continue;
					}
					Enumeration<? extends ZipEntry> entries = zipFile.entries();
					while (entries.hasMoreElements()) {
						ZipEntry entry = entries.nextElement();
						try {
							inputStream = zipFile.getInputStream(entry);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					if(inputStream!=null) {
						break;
					}
				}	
					
				if (inputStream != null) {
					@SuppressWarnings("unused")
					String getPath = getPathSaveNameAliOssQr(inputStream, nmid + "_A01",
							"oob-nasabah-qr-code/" + nmid);
				}

				map.put("sizeFile", bytes.length);
			}
			maps.add(map);
		}
		
		
		return new ResponseEntity<Object>(maps, HttpStatus.OK);
	}

	@GetMapping("/allUserThatApproved/v2")
	//@Scheduled(cron = "0 34 17 * * ?")
	//@Scheduled(fixedRate = 10*60*1000L)
	public List<String> getAllUserThatApproved() {
		System.out.println("======================= Trying to getAllUserThatApproved for approved all user : start On : " + new Date() + " ========================================= ");
		String dateNowInString = DateConverter.convertDateToString(new Date(), "yyyyMMdd");
	
		
		List<String> dateCaliberator = new ArrayList<String>();
		dateCaliberator.add(dateNowInString);
	
		for(int i = 1 ; i<=8; i++) {
		    Calendar calendar = Calendar.getInstance();
		    calendar.setTime(new Date());
		    calendar.add(Calendar.DATE, (0-i));
		    Date yesterday = calendar.getTime();
	   	String date1DayBeforeInString = DateConverter.convertDateToString(yesterday, "yyyyMMdd");
		dateCaliberator.add(date1DayBeforeInString);
		}
	
		

		List<String> userOobTablesAppr = new ArrayList<>();

		for (String dc : dateCaliberator) {

			Map<Integer, List<String>> map = getRenderInMap(dc);

			for (Entry<Integer, List<String>> result : map.entrySet()) {
				List<String> list = result.getValue();
				System.out.println("get value list csvnya : " + new Gson().toJson(list));
				Long id = null;
				try {
					id = Long.parseLong(list.get(0));
				} catch (NumberFormatException nfe) {
					System.err.println(" id " + id + " bukan tipe long ");
					//nfe.printStackTrace();
				}

				String statusId = list.get(6);
				System.err.println("lihat statusId nya id oob " + id + " adalah " + statusId);
				if (statusId != null && statusId.equals("0") && id != null) {
					System.err.println("check status approve untuk id oob " + id);
					UserOobTable userOobTable = userOobTableDao.getDetailUserByItsIdOobWhoseId2(id);
					if (userOobTable != null ) {
						String nmid = list.get(8);
						System.out.println("get zipfile untuk  " + sftpPathFile + dc + "/pten");
						ZipFile zipFile = getFile(sftpPathFile + dc + "/pten", nmid + ".zip");
						Enumeration<? extends ZipEntry> entries = zipFile.entries();
						InputStream inputStream = null;

						while (entries.hasMoreElements()) {
							ZipEntry entry = entries.nextElement();
							try {
								inputStream = zipFile.getInputStream(entry);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						if (inputStream != null) {
							String getPath = getPathSaveNameAliOssQr(inputStream, nmid + "_A01",
									"oob-nasabah-qr-code/" + nmid);
							userOobLoginSvc.createQrNumberUserWithCommon(userOobTable.getId(), nmid, list.get(1),
									getPath);
							
							System.err.println("id yg akan diupdate approved : " + userOobTable.getId());
							userOobLoginSvc.updateNasabahTo3New(userOobTable.getId(), "YRuYy-dSiaK-LLdWA");
							UserOobTableAuthDtl userOobTableAuthDtl = userOobLoginSvc
									.createPasswordUser(userOobTable.getId());
							LogStatusEmailDto logStatusEmailDto = EmailBody.bodyForSuccessPendaftaran(
									userOobTable.getNamaPemilikUsaha(), 
									MaskingNumber.maskingPhoneNumber(userOobTable.getNoHandphone())
									,
									userOobTableAuthDtl.getPassword(), userOobTable.getEmailPemilikUsaha(),
									OOB_DNS+"/login", OOB_DNS+"/makePassword");

							kirimEmail(logStatusEmailDto.getLogTujuan(), logStatusEmailDto.getLogUser(),
									logStatusEmailDto.getLogStatus(), logStatusEmailDto.getLogSubject());
							String smsWord = "Pendaftaran MORIS berhasil. "
									+ " Login di: "+OOB_DNS+"/cekStatusPendaftaran  "
									+ " dengan no.hp dan password sementara: " + userOobTableAuthDtl.getPassword()
									+ " dalam 2x24 jam." + " Hub HiYokke 14002 jika perlu bantuan.";
							kirimSms(userOobTable.getNoHandphone(), smsWord);
							userOobTablesAppr.add(userOobTable.getId() + " || " + userOobTable.getNamaPemilikUsaha());
						}

					}

				}
				else if(statusId != null && statusId.equals("1") && id != null) {
					System.err.println("check status approve untuk id oob " + id);
					UserOobTable userOobTable = userOobTableDao.getDetailUserByItsIdOobWhoseId2(id);
					if (userOobTable != null ) {
						userOobLoginSvc.updateNasabahToMinus1New(userOobTable.getId(), "YRuYy-dSiaK-LLdWA");
					}
				}
			}
		}
		
		System.out.println("all user that approved : " + new Gson().toJson(userOobTablesAppr));
		System.out.println("======================= Trying to getAllUserThatApproved for approved all user : end On : " + new Date() + " ========================================= ");
		
		return userOobTablesAppr;
	}

//	@Scheduled(cron = "0 0 17 * * ?")
//	@GetMapping("/deactivate/v2")
//	public List<UserOobTable> deactiveDataWhoOver8Days() {
//	List<UserOobTable> userOobTables = allTheAuthenticationCircuitSvc.getAllUserNeedDeactivate();
//		return userOobTables;
//	}


}

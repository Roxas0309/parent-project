package id.co.oob.merchant.onboarding.controller.encryption;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.token.AuthenticationSpiritMaker;
import id.co.oob.lib.common.merchant.onboarding.dto.token.MandiriTokenResponse;
import id.co.oob.lib.common.merchant.onboarding.encryptor.SHA256withRSAEncrypted;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;
import id.co.oob.merchant.onboarding.controller.BaseCtl;
import id.co.oob.merchant.onboarding.lib.encrypt.XSignatureHmacSha512Vers2;

@RestController
@RequestMapping("/encryptionEdition")
public class EncryptionEditionCtl extends BaseCtl{

	@GetMapping("/getClientAuthentication")
	public ResponseEntity<Object> getClientAuthentication(
			@RequestHeader(name = "httpMethod", required = true) String httpMethod,
			@RequestHeader(name = "path", required = true) String path,
			@RequestHeader(name = "reqPayload", required = true) String reqPayload){
		
		
		String url = MANDIRI_URL+"/openapi/auth/token";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("grant_type", "client_credentials");
	
		
		String timeStamp = DateConverter.convertDateToString(new Date(),
				"yyyy-MM-dd'T'HH:mm:ss.SSS'T'+0700");
		System.out.println("clientId " + clientId);
		System.out.println("keyPassword " + keyPassword);
		System.out.println("keyAlias " + keyAlias);
		System.out.println("keyStoreUrl " + keyStoreUrl);
		System.out.println("deployMentFrom " + deployMentFrom );
		System.out.println("timestamp " + timeStamp );
		String message = clientId+"|"+timeStamp;
		System.out.println("message " + message);
		Map<String, Object> mappo = null;
		try {
			 mappo = SHA256withRSAEncrypted.mySignature
					(message, keyPassword, keyAlias, keyStoreUrl, deployMentFrom);
		} catch (UnrecoverableKeyException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException
				| CertificateException | SignatureException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException(e.getMessage());
		}
		
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("X-Mandiri-Key", clientId);
		headerMap.put("X-SIGNATURE", (String) mappo.get(SHA256withRSAEncrypted.SIGNATURE));
		headerMap.put("X-TIMESTAMP", timeStamp);
		ResponseEntity<String> response = 
				wsBody(url, map, HttpMethod.POST, headerMap);
		MandiriTokenResponse mandiriTokenResponse = new MandiriTokenResponse();
		try {
			mandiriTokenResponse = mapperJsonToSingleDto(response.getBody(), MandiriTokenResponse.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Token Mandiri tidak dapat digenerate");
		}
		
		System.err.println("req payload " + reqPayload.replaceAll("\\s+",""));
		
		String value = httpMethod+":"+path+":"+mandiriTokenResponse.getAccessToken()+
				":"+reqPayload.replaceAll("\\s+","")+":"+timeStamp;
		System.out.println("value : " + value);
		AuthenticationSpiritMaker authenticationSpiritMaker = new AuthenticationSpiritMaker();
		authenticationSpiritMaker.setAuthorization(mandiriTokenResponse.getTokenType()+" " + mandiriTokenResponse.getAccessToken());
		authenticationSpiritMaker.setxTimestamp(timeStamp);
		authenticationSpiritMaker.setxSignature(XSignatureHmacSha512Vers2.buildHmacSignature
				(value, clientSecret));
		
		
		return new ResponseEntity<Object>(authenticationSpiritMaker,HttpStatus.OK);
	}
	
	@GetMapping("/myBearer/auth")
	public ResponseEntity<String> getMyBearerAuth(
			@RequestHeader(name = "clientId", required = true) String clientId,
			@RequestHeader(name = "keyPassword", required = true) String keyPassword,
			@RequestHeader(name = "keyAlias", required = true) String keyAlias,
			@RequestHeader(name = "keyStoreUrl", required = true) String keyStoreUrl,
			@RequestHeader(name = "deployMentFrom", required = true)String deployMentFrom){
		String url = MANDIRI_URL+"/openapi/auth/token";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("grant_type", "client_credentials");
	
		
		String timeStamp = DateConverter.convertDateToString(new Date(),
				"yyyy-MM-dd'T'HH:mm:ss.SSS'T'+0700");
		System.out.println("clientId " + clientId);
		System.out.println("keyPassword " + keyPassword);
		System.out.println("keyAlias " + keyAlias);
		System.out.println("keyStoreUrl " + keyStoreUrl);
		System.out.println("deployMentFrom " + deployMentFrom );
		System.out.println("timestamp " + timeStamp );
		String message = clientId+"|"+timeStamp;
		System.out.println("message " + message);
		Map<String, Object> mappo = null;
		try {
			 mappo = SHA256withRSAEncrypted.mySignature
					(message, keyPassword, keyAlias, keyStoreUrl, deployMentFrom);
		} catch (UnrecoverableKeyException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException
				| CertificateException | SignatureException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException(e.getMessage());
		}
		
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("X-Mandiri-Key", clientId);
		headerMap.put("X-SIGNATURE", (String) mappo.get(SHA256withRSAEncrypted.SIGNATURE));
		headerMap.put("X-TIMESTAMP", timeStamp);
		ResponseEntity<String> response = 
				wsBody(url, map, HttpMethod.POST, headerMap);
		return response;
	}
	
	@GetMapping("/restful/getSign")
	public ResponseEntity<Object> getMyRestfulSign(
			@RequestHeader(name = "clientId", required = true) String clientId,
			@RequestHeader(name = "keyPassword", required = true) String keyPassword,
			@RequestHeader(name = "keyAlias", required = true) String keyAlias,
			@RequestHeader(name = "keyStoreUrl", required = true) String keyStoreUrl,
			@RequestHeader(name = "deployMentFrom", required = true)String deployMentFrom){
		String timeStamp = DateConverter.convertDateToString(new Date(),
				"yyyy-MM-dd'T'HH:mm:ss.SSS'T'+0700");
		System.out.println("clientId " + clientId);
		System.out.println("keyPassword " + keyPassword);
		System.out.println("keyAlias " + keyAlias);
		System.out.println("keyStoreUrl " + keyStoreUrl);
		System.out.println("deployMentFrom " + deployMentFrom );
		System.out.println("timestamp " + timeStamp );
		String message = clientId+"|"+timeStamp;
		System.out.println("message " + message);
		Map<String, Object> mappo = null;
		try {
			 mappo = SHA256withRSAEncrypted.mySignature
					(message, keyPassword, keyAlias, keyStoreUrl, deployMentFrom);
		} catch (UnrecoverableKeyException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException
				| CertificateException | SignatureException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException(e.getMessage());
		}
		
		return new ResponseEntity<Object>(mappo, HttpStatus.OK);
	}
	
}

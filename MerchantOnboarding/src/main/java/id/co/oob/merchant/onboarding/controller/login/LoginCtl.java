package id.co.oob.merchant.onboarding.controller.login;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import id.co.oob.db.qris.merchant.onboarding.dao.session.SessionLoginHdrDao;
import id.co.oob.db.qris.merchant.onboarding.repository.user.AllUserOobDtlRepo;
import id.co.oob.db.qris.merchant.onboarding.repository.user.SessionLoginHdr;
import id.co.oob.db.qris.merchant.onboarding.repository.user.SessionLoginHdrDtl;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableAuthDtl;
import id.co.oob.db.qris.merchant.onboarding.service.authentication.AllTheAuthenticationCircuitSvc;
import id.co.oob.db.qris.merchant.onboarding.service.session.SessionServiceMaster;
import id.co.oob.lib.common.merchant.onboarding.convert.PasswordConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.alamat.ProvinsiTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.success.SuccessValidation;
import id.co.oob.lib.common.merchant.onboarding.dto.user.ChangePasswordUser;
import id.co.oob.lib.common.merchant.onboarding.dto.user.LinkChangePassDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserLogin;
import id.co.oob.lib.common.merchant.onboarding.encryptor.EncryptDataFiller;
import id.co.oob.lib.common.merchant.onboarding.throwable.AnauthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.ForbiddenAuthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.LockedDataException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;
import id.co.oob.lib.common.merchant.onboarding.validation.DateValidation;
import id.co.oob.merchant.onboarding.controller.BaseCtl;

@RestController
@RequestMapping("/loginCtl")
public class LoginCtl extends BaseCtl {

	@Autowired
	private AllTheAuthenticationCircuitSvc allTheAuthenticationCircuitSvc;

	@Autowired
	private SessionServiceMaster sessionServiceMaster;
	
	@Autowired
	private SessionLoginHdrDao sessionLoginHdrDao;
	
	
	@GetMapping("/hello-prod")
	public String helloProd() {
		return "Hello Prod";
	}
	
	@Scheduled(cron = "0 0 15 * * ?")
	@GetMapping("/update/expiredPassword")
	public List<UserOobTableAuthDtl> updateAllNasabahThatAlreadyExpiredPassword() {
		System.out.println("scheduler updateAllNasabahThatAlreadyExpiredPassword update start on " + SDF.format(new Date()));	
		List<UserOobTableAuthDtl> userOobTableAuthDtls = allTheAuthenticationCircuitSvc.updateAllUserThatAlreadyExpired();
		System.out.println("scheduler updateAllNasabahThatAlreadyExpiredPassword update start on " + SDF.format(new Date()));
		return userOobTableAuthDtls;
	}
	
	@GetMapping("/generateMySession")
	public String generateMySession(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SESSION_ITEM, required = true) String sessionItem) {
		
		if(Strings.isBlank(sessionItem)) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("date", new Date());
		map.put("Uuid", UUID.randomUUID().toString());
		String sessionId = EncryptDataFiller.encrypt(new Gson().toJson(map), "4Vbxc-uoGWA-LVYg5");
		sessionServiceMaster.saveSessionMe(sessionId);
		return sessionId;
		}else {
			SessionLoginHdr sessionLoginHdr = sessionLoginHdrDao.getSessionLoginHdr(sessionItem);
			if(sessionLoginHdr!=null) {
			sessionServiceMaster.refreshSessionMe(sessionItem);
			return sessionItem;
			}
			else {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("date", new Date());
				map.put("Uuid", UUID.randomUUID().toString());
				String sessionId = EncryptDataFiller.encrypt(new Gson().toJson(map), "4Vbxc-uoGWA-LVYg5");
				sessionServiceMaster.saveSessionMe(sessionId);
				return sessionId;
			}
		}
	}
	

	@PostMapping("/auth")
	public ResponseEntity<Object> auth(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey, 
			@RequestBody UserLogin userLogin) {
		saveYourLogsHere(userLogin, "/loginCtl/auth");
		if (Strings.isEmpty(userLogin.getSessionLocal())) {
			throw new ForbiddenAuthorizedException("Session Sudah Tidak Aktif");
		}
		
		if (sessionLoginHdrDao.getSessionLoginHdr(userLogin.getSessionLocal())==null) {
			throw new ForbiddenAuthorizedException("Session Tidak Valid");
		}
		
		SessionLoginHdrDtl sess = sessionServiceMaster.getSessionLoginDtl(userLogin.getSessionLocal(), "/loginCtl/auth");
		
		if(sess!=null) {
			throw new AnauthorizedException(
					"Mohon maaf, Anda sudah melakukan kesalahan login sebanyak 5 kali. Anda dapat melakukan login kembali pada jam " + new SimpleDateFormat("HH:mm").format(sess.getBlockedOpenDate()));
		}

		AuthorizedResponseFirst(secretKey, secretId);
		Map<String, Object> token = allTheAuthenticationCircuitSvc.getTokenLogin(userLogin.getNoPhone(),
				userLogin.getPassword(), "USER_OOB", secretKey, userLogin.getTokenLocal(), userLogin.getSessionLocal());

		System.err.println("tokennya adalah : " + new Gson().toJson(token));

		if (token.get("isInvalid") != null && (boolean) token.get("isInvalid")) {
			if (((String) token.get("condition")).equalsIgnoreCase("EXPIRED")) {
				throw new AnauthorizedException(
						"Username atau Password sementara sudah tidak berlaku. Pilih lupa password atau hubungi HiYokke di 14021.");
			}
			else if (((String) token.get("condition")).equalsIgnoreCase("INACTIVE")) {
				throw new LockedDataException(
						"Akun tidak digunakan selama 90 hari. Untuk keamanan Anda, silakan ulangi pendaftaran.");
			}
			
			//Permintaan untuk tidak spesifikasi login invalid
			else if (((String) token.get("condition")).equalsIgnoreCase("NO_HP_TERDAFTAR")) {
				throw new AnauthorizedException("Nomor handphone atau password tidak sesuai. Silakan ulangi login.");
			}
			else if(((String) token.get("condition")).equalsIgnoreCase("BELUM_TERDAFTAR")) {
				throw new AnauthorizedException("Nomor handphone atau password tidak sesuai. Silakan ulangi login.");
			}
			
			
//			else if (((String) token.get("condition")).equalsIgnoreCase("NO_HP_TERDAFTAR")) {
//				throw new AnauthorizedException(" Harap masukkan nomor handphone yang terdaftar. ");
//			}
//			else if(((String) token.get("condition")).equalsIgnoreCase("BELUM_TERDAFTAR")) {
//				throw new AnauthorizedException(" Anda belum terdaftar. Silakan melakukan pendaftaran "
//						+ "akun terlebih dahulu.");
//			}
			
			else if(((String) token.get("condition")).equalsIgnoreCase("PASSWORD_EMAIL_NOT_SAME")) {
			    throw new AnauthorizedException("Nomor handphone atau password tidak sesuai. Silakan ulangi login.");	
			}
			else if(((String) token.get("condition")).equalsIgnoreCase("PASSWORD_EMAIL_NOT_SAME")) {
			    throw new AnauthorizedException("Nomor handphone atau password tidak sesuai. Silakan ulangi login.");	
			}
			
			//Permintaan untuk tidak spesifikasi login invalid
//			else if(((String) token.get("condition")).equalsIgnoreCase("PASSWORD_EMAIL_NOT_SAME")) {
//			    throw new AnauthorizedException("Harap masukan password yang sudah dikirimkan melalui email dan SMS.");	
//			}
//			else if(((String) token.get("condition")).equalsIgnoreCase("PASSWORD_EMAIL_NOT_SAME")) {
//			    throw new AnauthorizedException("Harap masukan password yang sudah dikirimkan melalui email dan SMS.");	
//			}
			
			else if(((String) token.get("condition")).equalsIgnoreCase("PASSWORD_PHONE_NOT_SAME")) {
				 throw new AnauthorizedException("Nomor handphone atau password tidak sesuai. Silakan ulangi login.");
			}
			
			else if(((String) token.get("condition")).equalsIgnoreCase("ONLY_ONE_DEVICE")) {
				throw new AnauthorizedException("Pastikan Anda Hanya Login di satu perangkat "
						+ "untuk mengakses akun ini");
			}
		}

		return new ResponseEntity<Object>(new SuccessValidation("Login Berhasil", token), HttpStatus.OK);
	}

	@PostMapping("/out-auth")
	public ResponseEntity<Object> outAuth(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey, @RequestBody UserLogin userLogin) {
		AuthorizedResponseFirst(secretKey, secretId);
		allTheAuthenticationCircuitSvc.removeTokenLogin(userLogin.getTokenLocal(), secretKey);
		return new ResponseEntity<Object>(new SuccessValidation("Logout Berhasi", ""), HttpStatus.OK);
	}

	@PostMapping("/refresh")
	public ResponseEntity<Object> refresh(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken,
			@RequestHeader(value = SESSION_ITEM, required = true) String sessionItem) {

//		System.out.println("check refresh token : :1");
		if (Strings.isEmpty(sessionItem)) {
			throw new ForbiddenAuthorizedException("Session Sudah Tidak Aktif");
		}
		
//		System.out.println("check refresh token : :2");
		
		AuthorizedResponseFirst(secretKey, secretId);
		
//		System.out.println("check refresh token : :3");
		
		String token = allTheAuthenticationCircuitSvc.checkAndRefreshToken(secretToken, secretKey, sessionItem);
		
//		System.err.println("refresh berjalam dengan baikk.");
		
		return new ResponseEntity<Object>(new SuccessValidation("Login Berhasil", token), HttpStatus.OK);

	}


	@PostMapping("/changePassword")
	public ResponseEntity<Object> changePassword(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken,
			@RequestBody ChangePasswordUser map) {
		AuthorizedResponseFirst(secretKey, secretId);
		
		Boolean checkIfPasswordValid = PasswordConverter.checkPasswordConf(map.getPassword());
		
		if(!checkIfPasswordValid) {
			throw new  NotAcceptanceDataException("Pastikan password minimum 8 karakter yang " + 
					" terdiri dari kombinasi: huruf kapital, huruf kecil, dan angka.");
		}

		
		AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken(secretToken, secretKey);
		UserOobTableAuthDtl authDtl = allTheAuthenticationCircuitSvc.changePassword(secretId,
				allUserOobDtlRepo.getUserOobTableAuthDtlDto().getIdUserOob(), map.getPassword(),
				allUserOobDtlRepo.getUserOobTableSuccessDto().getNoHandphone());
		allTheAuthenticationCircuitSvc.deleteTokenByIdToken(allUserOobDtlRepo.getUserAuthTokenTableDto().getIdToken());
		Map<String, Object> mappo = new HashMap<String, Object>();
		mappo.put("authDtl", authDtl);
		mappo.put("valid", true);
		return new ResponseEntity<Object>(new SuccessValidation("Ganti Password Berhasil", mappo), HttpStatus.OK);
	}

	@PostMapping("/forgotPassword/v2")
	public ResponseEntity<Object> forgotPassword(
			@RequestHeader("secretKey") String secretKey,
			@RequestHeader("encValue") String encValue,
			@RequestBody ChangePasswordUser changePasswordUser) {
	
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("secretKey", secretKey);
		map.put("encValue", encValue);
		map.put("changePasswordUser", changePasswordUser);
		saveYourLogsHere(map, "/forgotPassword/v2");
		if (changePasswordUser == null || Strings.isBlank(changePasswordUser.getPassword())) {
			throw new NotAcceptanceDataException("mohon masukkan parameter untuk ubah password");
		}

        Boolean checkIfPasswordValid = PasswordConverter.checkPasswordConf(changePasswordUser.getPassword());
		
		if(!checkIfPasswordValid) {
			throw new  NotAcceptanceDataException("Pastikan password minimum 8 karakter yang " + 
					" terdiri dari kombinasi: huruf kapital, huruf kecil, dan angka.");
		}

		System.out.println("will denc : enc : " + encValue + ", secret key : " + secretKey  );
		String enc = EncryptDataFiller.decrypt(encValue, secretKey.toUpperCase());
		System.out.println("enc : " + enc);
		LinkChangePassDto linkChangePassDto = new LinkChangePassDto();
		try {
			linkChangePassDto = mapperJsonToSingleDto(enc, LinkChangePassDto.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("tidak dapat memetakan json ke LinkChangePassDto");
		}
		String format = "ddMMyyyyHHmmss";
		Date date = null;
		
		try {
			date = new SimpleDateFormat(format).parse(linkChangePassDto.getDate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(DateValidation.getDifferentDays(date)>= 2) {
			throw new ForbiddenAuthorizedException("Link Memasuki Masa Kadaluarsa");
		}
		
		UserOobTableAuthDtl oobTableAuthDtl = allTheAuthenticationCircuitSvc
				.changePasswordForgotPassword(linkChangePassDto.getId(), changePasswordUser.getPassword());
		
		return new ResponseEntity<Object>(new SuccessValidation("Ganti Password Berhasil", "Done"), HttpStatus.OK);
//		AuthorizedResponseFirst(secretKey, secretId);
//		AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken(secretToken, secretKey);
//		UserOobTableAuthDtl authDtl = allTheAuthenticationCircuitSvc.changePassword
//				(secretId, allUserOobDtlRepo.getUserOobTableAuthDtlDto().getIdUserOob(),
//						map.getPassword(), allUserOobDtlRepo.getUserOobTableSuccessDto().getNoHandphone()); 
//		//allTheAuthenticationCircuitSvc.deleteTokenByIdToken(allUserOobDtlRepo.getUserAuthTokenTableDto().getIdToken());
//		Map<String, Object> mappo = new HashMap<String, Object>();
//		mappo.put("authDtl", authDtl);
//		mappo.put("valid", true);
//        return new ResponseEntity<Object>(new SuccessValidation("Ganti Password Berhasil", mappo), HttpStatus.OK);
	
	}

	@DeleteMapping("/deleteAllAuthToken")
	public ResponseEntity<Object> refreshAllAuthToken(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken) {
		AuthorizedResponseFirst(secretKey, secretId);
		Integer count = allTheAuthenticationCircuitSvc.refreshAllToken();
		return new ResponseEntity<Object>(new SuccessValidation("Token Berhasil Dihapus", count), HttpStatus.OK);
	}

	@GetMapping("/detailToken")
	public ResponseEntity<Object> detailToken(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken) {
		AuthorizedResponseFirst(secretKey, secretId);
		AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken(secretToken, secretKey);
		return new ResponseEntity<Object>(new SuccessValidation("Get Detail Token Berhasil", allUserOobDtlRepo),
				HttpStatus.OK);
	}
	
	@GetMapping("/detailToken/Byid")
	public ResponseEntity<Object> detailTokenById(@RequestParam(value = "id") Long id) {
		AllUserOobDtlRepo repo = userOobLogicSvc.getDetailAllUserByItsId(id);
		
		return new ResponseEntity<Object>(new SuccessValidation("Get Detail Token Berhasil", repo),
				HttpStatus.OK);
	}

}

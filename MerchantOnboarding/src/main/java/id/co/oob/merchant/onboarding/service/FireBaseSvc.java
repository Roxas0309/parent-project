package id.co.oob.merchant.onboarding.service;

import java.io.IOException;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

@Service
public class FireBaseSvc {

	
    @Value("${app.firebase-configuration-file}")
    private String firebaseConfigPath;
	
    public FirebaseMessaging initialize() throws IOException {
    	 GoogleCredentials googleCredentials = GoogleCredentials
    	            .fromStream(new ClassPathResource(firebaseConfigPath).getInputStream());
    	    FirebaseOptions firebaseOptions = FirebaseOptions
    	            .builder()
    	            .setCredentials(googleCredentials)
    	            .build();
    	    FirebaseApp app = FirebaseApp.initializeApp(firebaseOptions);
    	    return FirebaseMessaging.getInstance(app);
    }

    public String sendNotification(String token) throws FirebaseMessagingException, IOException{
        Message message = Message
                .builder()
                .setToken("BGRT_kj1S-vr_SgYDkwUuB9Wy28lSMrmcZ00R4fttoUkHbXWPjLaEIoOQWPfcDK-SWRJb3rWVDeT7OGBVd7V0yc")
                .setNotification(new Notification("hai","body"))
                .putAllData(new HashMap<String, String>())
                .build();
        return initialize().send(message);
    }
	
}

package id.co.oob.merchant.onboarding.controller.vendor;

import javax.validation.Valid;

import id.co.oob.merchant.onboarding.controller.BaseCtl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import id.co.oob.db.qris.merchant.onboarding.repository.apivendor.VendorModel;
import id.co.oob.db.qris.merchant.onboarding.service.vendor.VendorService;

@RestController
@RequestMapping("/vendor")
public class VendorCtl extends BaseCtl {
	
	@Autowired
	private VendorService vendorService;
	
	@PostMapping
	public <T> ResponseEntity<T> save (
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody @Valid VendorModel vendorModel){
		AuthorizedResponseFirst(secretKey, secretId);
		return (ResponseEntity<T>) vendorService.save(vendorModel);
	}
}

package id.co.oob.merchant.onboarding.controller;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.oob.lib.common.merchant.onboarding.convert.CalendarDayDateDto;
import id.co.oob.lib.common.merchant.onboarding.convert.CalendarDto;
import id.co.oob.lib.common.merchant.onboarding.convert.MakingCalendar;

@RestController
@RequestMapping("/date-maker")
public class DateComponentMaker extends BaseCtl {

	@GetMapping("/get-date-now/{rentang}")
	public ResponseEntity<Object> getRentangTanggal(@PathVariable("rentang") Integer rentang) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int tanggalSekarang = calendar.get(Calendar.DATE);
		CalendarDto calendarDto = MakingCalendar.makingCalendar(calendar.get(Calendar.MONTH) + 1,
				calendar.get(Calendar.YEAR));
			
		CalendarDto calendarDtoRentang = mapperFacade.map(calendarDto, CalendarDto.class);
		List<CalendarDayDateDto> calendarDayDateDtos = new ArrayList<CalendarDayDateDto>();
		int i = 0;
		int seq = 6;
		for (CalendarDayDateDto stream : calendarDto.getCalendarDayDateDtos()) {
//			System.out.println("validasi : " + stream.getTanggal() + "  " + (tanggalSekarang + i));
			if (stream.getTanggal() > (tanggalSekarang - rentang) && stream.getTanggal() <=(tanggalSekarang)) {
				CalendarDayDateDto calendarDayDateDto = new CalendarDayDateDto();
				calendarDayDateDto.setHari(stream.getHari().toUpperCase());
				final Calendar cal = Calendar.getInstance();
			    cal.add(Calendar.DATE, (0-seq));
			    
				calendarDayDateDto.setFormatIn(new SimpleDateFormat("yyyyMMdd").format(cal.getTime()));
				calendarDayDateDto.setTanggal(stream.getTanggal());
				calendarDayDateDto.setSequence(seq);
				if(stream.getTanggal()==tanggalSekarang) {
					calendarDayDateDto.setIsActiveDay(true);
				}
				calendarDayDateDtos.add(calendarDayDateDto);
				seq--;
			}
		}
		calendarDtoRentang.setCalendarDayDateDtos(calendarDayDateDtos);
		return new ResponseEntity<Object>(calendarDtoRentang,HttpStatus.OK);
	}

}

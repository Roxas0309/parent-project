package id.co.oob.merchant.onboarding;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {"id.co.oob"})
@EnableAutoConfiguration
@EnableSwagger2
@EnableScheduling
public class MerchantOnboardingApplication {
	
	private static ConfigurableApplicationContext context;
	
	public static void main(String[] args) {
		System.out.println("deployment untuk update persiapan ");
		System.out.println("================   Built Time V1.2 " + new Date() + " ================  " );
		SpringApplication.run(MerchantOnboardingApplication.class, args);			
		System.out.println("================   Built Time End V1.2 " + new Date() + " ================  " );
	}
	
	public static void restart() {
        ApplicationArguments args = context.getBean(ApplicationArguments.class);

        Thread thread = new Thread(() -> {
            context.close();
            context = SpringApplication.run(MerchantOnboardingApplication.class, args.getSourceArgs());
        });

        thread.setDaemon(false);
        thread.start();
    }

}
